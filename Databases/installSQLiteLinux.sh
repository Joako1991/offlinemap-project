#!/bin/bash

SCRIPT_DIR=`dirname "$(readlink -f "$0")"`
{
    cd ${SCRIPT_DIR}
    tar xvfz ./sqlite-autoconf-3300100.tar.gz
    cd sqlite-autoconf-3300100
    ./configure --prefix=/usr/local
    make
    sudo make install
}
