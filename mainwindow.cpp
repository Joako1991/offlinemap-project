/// Qt includes
#include <QDockWidget>

/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include "mainwindow.h"
#include <MapWidget/OfflineMap.hpp>
#include "ui_mainwindow.h"

#define INSERT_ERASE_BUTTONS_SPACING 10

MainWindow::MainWindow(const User &loggedInUser, QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow),
    _mapWidget(nullptr),
    _loggedInUser(loggedInUser),
    _insertPlaceButton(nullptr),
    _erasePlaceButton(nullptr),
    _searchManager(nullptr),
    _commentSystem(nullptr),
    _schedWidget(nullptr),
    _searchScrollArea(nullptr),
    _centralLayout(nullptr)
{
    ui->setupUi(this);
    _centralLayout = new QVBoxLayout(this);
    initializeWidgets();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete _mapWidget;
    delete _insertPlaceButton;
    delete _searchManager;
    delete _erasePlaceButton;
    delete _searchScrollArea;
    delete _centralLayout;
    delete _commentSystem;
    delete _schedWidget;
}

void MainWindow::setWidgetsSizeConstrains()
{
    if(_mapWidget)
    {
        _mapWidget->setMinimumWidth(int(this->width() * MIN_WIDTH_FACTOR));
        _mapWidget->setMaximumWidth(int(this->width() * MAX_WIDTH_FACTOR));
        _mapWidget->setMaximumHeight(int(this->height() - INSERT_ERASE_BUTTONS_SPACING - 20));
    }
}

void MainWindow::createMapDockWidget()
{
    _mapWidget = new QDockWidget("Le Creusot Map", this);
    /// We disable the close button from the dock widget.
    /// Taken from https://stackoverflow.com/a/26998696
    _mapWidget->setFeatures(QDockWidget::DockWidgetFloatable |
        QDockWidget::DockWidgetMovable);
    _mapWidget->setWidget(new OfflineMap(_mapWidget));

    /// We set constrains where we allow the map to be
    _mapWidget->setAllowedAreas(Qt::RightDockWidgetArea);

    /// We start the map docked
    _mapWidget->setFloating(false);

    addDockWidget(Qt::RightDockWidgetArea, _mapWidget);

    connect(_mapWidget,
        &QDockWidget::topLevelChanged,
        this,
        &MainWindow::onStateChanged);
}

void MainWindow::createInsertAndErasePlaceWidgets()
{
    assert(_mapWidget);
    /// We create a little widget with the two buttons in
    /// an horizontal layout, and we add the final widget
    QHBoxLayout *buttonsLayout = new QHBoxLayout(this);
    QWidget *buttonsWidget = new QWidget(this);

    _insertPlaceButton = new InsertPlaceWidget(this);

    _erasePlaceButton = new ErasePlaceWidget(this);
    buttonsLayout->addWidget(_insertPlaceButton);
    buttonsLayout->addWidget(_erasePlaceButton);

    buttonsWidget->setLayout(buttonsLayout);

    _centralLayout->addWidget(buttonsWidget);
    /// We connect the map tap function with the insert QDialog widget
    OfflineMap *myMap = static_cast<OfflineMap*>(_mapWidget->widget());
    connect(myMap,
        &OfflineMap::mapTapped,
        _insertPlaceButton,
        &InsertPlaceWidget::mapInformationReceived);
}

void MainWindow::initializeWidgets()
{
    //// WARNING! INITIALIZATION ORDER MATTERS!
    /// We need to do certain connections between the widgets, so we need that they
    /// are initialized before doing the connection. Changing the order in which we
    /// create the widgets may produce unexpected behaviors and malfunctioning of
    /// the program

    /// Because our main layout is vertical, the order we list the createWidget functions
    /// is going to be the shape the main app is going to have
    /// Map widget
    createMapDockWidget();

    /// Search widget
    createSearchWidget();

    /// We add the button to insert new places and another one to erase
    /// (it should be initialized after the map has been created)
    createInsertAndErasePlaceWidgets();

    /// Create the comments system widget
    createCommentWidget();

    /// Create the schedule button
    createScheduleWidget();

    QWidget *mainWidget = new QWidget(this);
    mainWidget->setLayout(_centralLayout);
    setCentralWidget(mainWidget);

    setWidgetsSizeConstrains();
}

void MainWindow::createSearchWidget()
{
    OfflineMap* map = static_cast<OfflineMap*>(_mapWidget->widget());
    _searchManager = new SearchManager(this);

    /// We create an scrollable area for the search widget, so we can move up
    /// and down, even if the main window is small
    _searchScrollArea = new QScrollArea(this);
    _searchScrollArea->setWidgetResizable(true);
    _searchScrollArea->setWidget(_searchManager);
    _centralLayout->addWidget(_searchScrollArea);

    connect(_searchManager,
            &SearchManager::markPlaceRequest,
            map,
            &OfflineMap::markAPlace);

    connect(_searchManager,
            &SearchManager::stopIdChanged,
            map,
            &OfflineMap::setMarkId);

    connect(map,
        &OfflineMap::mapTapped,
        _searchManager,
        &SearchManager::onMarkAdded);

    connect(_searchManager,
        &SearchManager::clearRequested,
        map,
        &OfflineMap::clearMapInformation);

    connect(_searchManager,
        &SearchManager::removeMarkRequested,
        map,
        &OfflineMap::removeMarkedPlace);

    connect(_searchManager,
        &SearchManager::computePathRequested,
        map,
        &OfflineMap::onComputeRoute);
}

void MainWindow::createCommentWidget()
{
    assert(_searchManager);
    _commentSystem = new CommentsManager(_loggedInUser, this);
    /// When the search manager wants to mark a place, it is because
    /// the user selected a place from the list, so we update the
    /// place in the comment system too
    connect(_searchManager,
        &SearchManager::markPlaceRequest,
        _commentSystem,
        &CommentsManager::onNewPlace);

    connect(_searchManager,
        &SearchManager::clearRequested,
        _commentSystem,
        &CommentsManager::onReset);

    _centralLayout->addWidget(_commentSystem);
}

void MainWindow::createScheduleWidget()
{
    assert(_searchManager);
    _schedWidget = new ScheduleWidget(this);
    /// When the search manager wants to mark a place, it is because
    /// the user selected a place from the list, so we update the
    /// place in the comment system too
    connect(_searchManager,
        &SearchManager::markPlaceRequest,
        _schedWidget,
        &ScheduleWidget::setPlace);

    connect(_searchManager,
        &SearchManager::clearRequested,
        _schedWidget,
        &ScheduleWidget::onReset);

    _centralLayout->addWidget(_schedWidget);
}

void MainWindow::onStateChanged(bool isFloating)
{
    if(isFloating)
    {
        /// If the map is undocked, then we remove the size constrains
        _mapWidget->setMinimumWidth(0);
        _mapWidget->setMinimumHeight(0);
        _mapWidget->setMaximumWidth(QWIDGETSIZE_MAX);
        _mapWidget->setMaximumHeight(QWIDGETSIZE_MAX);
    }
    else
    {
        /// If the map is docked, then we set size constrains
        setWidgetsSizeConstrains();
    }
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    /// Inspired in the answer given here: https://stackoverflow.com/a/12829659
    QMainWindow::resizeEvent(event);
    setWidgetsSizeConstrains();
}
