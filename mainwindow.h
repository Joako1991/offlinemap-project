#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/// Qt includes
#include <QScrollArea>
#include <QMainWindow>
#include <QVBoxLayout>
#include <QWidget>

/// Custom includes
#include <CommentSystem/CommentsManager.hpp>
#include <ErasePlaceWidget/ErasePlaceWidget.hpp>
#include <InsertPlaceWidget/InsertPlaceWidget.hpp>
#include <MapWidget/OfflineMap.hpp>
#include <SearchEngine/SearchManager.hpp>
#include <ScheduleWidget/ScheduleWidget.hpp>
#include <User.hpp>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(const User &loggedInUser, QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    /**
     * @brief This slot will be called all the times the Map QDockWidget toggles
     * between floating position and docked
     *
     * @param isFloating: If true, it means the widget is floating and it is not
     * attached to the MainWindow. If false, it means the widget is docked
    */
    void onStateChanged(bool isFloating);

private:
    /**
     * @brief Initialize all the graphical objects
    */
    void initializeWidgets();

    /**
     * @brief Overloaded slot. This function is called whenever
     *   the MainWindow size changes
     *
     * @param event: Object with all the information related to the
     *   size change event
    */
    void resizeEvent(QResizeEvent* event) override;

    /**
     * @brief Create a QDockWidget and place the Map widget on it.
     * It also configures the QDockwidget sizes and allowed areas
    */
    void createMapDockWidget();

    /**
     * @brief Create a SearchWidget.
     * It also connects the signal mapTapped of OfflineMap
     * with the slot getLatLon of SearchWidget.
    */
    void createSearchWidget();

    /**
     * @brief Set the size contrains to the widgets.
     *   It will define maximum and minimum values that they can
     *   have depending on the MainWindow size, so then we can keep a nice geometry
     *   of all of them all the time
    */
    void setWidgetsSizeConstrains();

    /**
     * @brief Set up the GUI in order to add the new place Widget and the
     *  erase place widget. In the main window it will appear as a button,
     *  but when we push it, a new QDialog will appear that will allow us
     *  to enter the information regarding the new place we want to add
     *  for the insertion case, and for the erasing case, a new pop up window
     *  will appear that will show us the database entries. We just have to
     *  click the entry we want to erase, and if we confirm through the confirmation
     *  dialog, that place will be removed from the database.
    */
    void createInsertAndErasePlaceWidgets();

    /**
     * @brief Add to the main window the buttons that will provide access
     *   to the comments system. The buttons will provide the options of showing
     *   the comments for certain place, or add a comment to that place.
     *   In order to do that, a place object needs to be provided to the system.
     *   If this is not done, then a pop up window will inform you about the problem.
    */
    void createCommentWidget();

    /**
     * @brief Add to the main window the button to see the schedule of a place.
     *   This button is connected to a dialog window that will show the schedule
     *   of the last place selected from the SearchResult list.
     *   Random places that are not in the database don't have an schedule, so
     *   this window will show a pop up saying that we don't have an schedule for it
    */
    void createScheduleWidget();

    Ui::MainWindow *ui;

    QDockWidget* _mapWidget;

    const User _loggedInUser;
    InsertPlaceWidget *_insertPlaceButton;
    ErasePlaceWidget *_erasePlaceButton;
    SearchManager *_searchManager;
    CommentsManager *_commentSystem;
    ScheduleWidget *_schedWidget;
    QScrollArea *_searchScrollArea;
    QVBoxLayout *_centralLayout;
};
#endif // MAINWINDOW_H
