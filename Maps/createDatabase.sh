#!/bin/bash
# We need this library
sudo apt-get install -y \
    libgl-dev \
    libxml2-dev \
    zlib1g-dev \
    geoglue-2.0 \
    libgl-dev \
    libprotobuf-dev \
    protobuf-c-compiler \
    protobuf-compiler

# To build libosmscout:
## cd path/to/downloaded/repo
## mkdir build
## cd build
## cmake ..
## make

OST_PATH='../Externals/libosmscout/stylesheets/map.ost'
PROGRAM='../Externals/libosmscout/build/Import/Import'
MAP='./le_creusot_export.pbf'
OUTPUT='./map_database'
mkdir -p ${OUTPUT}

${PROGRAM} -d --destinationDirectory ${OUTPUT} --typefile ${OST_PATH} ${MAP}

# continue here: http://libosmscout.sourceforge.net/documentation/
