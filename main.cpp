// Qt includes
#include <QApplication>

// Custom widget includes
#include <DBEngine/DBEngine.hpp>
#include <LoginManager/LoginManager.hpp>
#include "mainwindow.h"
#include <User.hpp>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    DBEngine::instance()->loadDatabase(QString(PROJECT_PATH) + "Databases/mainDatabase.db");
    LoginManager lgnManager;

    User myUser;
    int ret_val = 0;
    if(lgnManager.startLoginManager(myUser))
    {
        if(myUser.isValid())
        {
            /// You should get in here with a valid user
            MainWindow w(myUser);
            w.show();
            ret_val = a.exec();
        }
    }
    return ret_val;
}
