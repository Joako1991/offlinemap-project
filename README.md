# README
This is the final cours project for Software Engineering. The goal is to make
an offline version of a mix between TripAdvisor (user opinions and places
reputations) and Google maps (have a map and a way to go to any place). The
constrain is to only do it for Le Creusot.

Here are the instructions in order to make it work in a
linux based system.

By: Ali Mahmoud - Lal-Trehan Uma - Pooja Kumari - Rodriguez Joaquin

# Main program window
![GitHub Logo](/program_screenshot.png)

# Install these programs on your linux:

```
sudo apt-get install -y \
    libgl-dev \
    libxml2-dev \
    zlib1g-dev \
    geoclue-2.0 \
    libgl-dev \
    libprotobuf-dev \
    protobuf-c-compiler \
    protobuf-compiler
```
# Install SQLite
Go into the folder Databases, and execute the script:
```
./installSQLiteLinux.sh
```
Installing this program like this is enough to have SQLite available in your system, and of course, to compile the program

# Copy this lines into your ~/.bashrc file
(We are supposing that you installed ***Qt 5.13.1*** at your ***home folder***)
```
export LD_LIBRARY_PATH=/usr/include/libxml2:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/mesa:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/$USER/Qt5.13.1/5.13.1/Src/qtwebengine/src/3rdparty/chromium/third_party/mesa_headers:$LD_LIBRARY_PATH
```
```
export PATH=/usr/include/libxml2:$PATH
export PATH=/home/$USER/Qt5.13.1/5.13.1/gcc_64/bin:$PATH
export PATH=/home/$USER/Qt5.13.1/Tools/QtCreator/bin:$PATH
export PATH=/usr/local/sbini:/usr/local/bin:$PATH
export PATH=/home/$USER/Qt5.13.1/5.13.1/Src/qtwebengine/src/3rdparty/chromium/third_party/mesa_headers:$PATH
```

# Source your bashrc script:
```
source ~/.bashrc
```

# Update your project repo
Go into your project repository, go into the branch master (git checkout master), and run this command:
```
git pull origin master
```

# Adding extra export lines:
Then, add these lines into your ```~/.bashrc``` file
```
export LD_LIBRARY_PATH=<YOUR_PATH_TO_REPO>/Externals/libosmscout/build/libosmscout-gpx:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH=<YOUR_PATH_TO_REPO>/Externals/libosmscout/build/libosmscout:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH=<YOUR_PATH_TO_REPO>/Externals/libosmscout/build/libosmscout-client-qt:$LD_LIBRARY_PATH
```
# Compile the library
Go into the folder <YOUR_PATH_TO_REPO>/Externals/libosmscout and execute this lines:
```
mkdir build
cd build
cmake ..
make
```

# Create local map database
For doing this you have 2 options:
* (Recommended) Use the compressed map of Le Creusot included in this repository, only execute the extractFiles.sh script located in the folder ***Maps*** and that's it.

* Download the map (OSM version) from the source https://export.hotosm.org/es/v3/exports/new/describe , uncompress the downloaded file into the folder <YOUR_PATH_TO_REPO>/Maps and execute the script createDatabase.sh

NOTE 1: For the second case, you have to have the library compiled before. The script required the Import module from OSMScout.



***NOTE 2: <YOUR_PATH_TO_REPO> is the path to your final project repo folder.***
***For instance, if you had put your repo at home, and it is called software-engineering-project, then <YOUR_PATH_TO_REPO> is /home/$USER/software-engineering-project/***

# Run the app
Open the project
```
./final_project.pro
```
in Qt creator, and just run it. It should work as it is

***NOTE: THE NEXT STEP IS DEPRECATED. IT WAS NEEDED WHEN WE COMPILED AND RUN THE EXAMPLE PROVIDED BY THE LIBRARY OSMSCOUT2.***
***We keep it just in case we want to run the example again in the future***

Go to
```
<YOUR_PATH_TO_REPO>/Externals/libosmscout/build-OSMScout-Desktop_Qt_5_13_1_GCC_64bit-Debug/debug
```
and run this command:
```
./OSMScout --icons ../../libosmscout/data/icons/svg/standard <YOUR_IMPORTED_MAP_DATA> ../../stylesheets/standard.oss
```

where:
***<YOUR_IMPORTED_MAP_DATA>*** is the path to the map that you imported. For instance, if you have used the script extractFiles.sh, ***<YOUR_IMPORTED_MAP_DATA>*** would be ***<YOUR_PATH_TO_REPO>/Maps/map_database***