/// Qt includes
#include <QDialogButtonBox>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>

/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include "InsertPlaceWidget/DialogNewPlace.hpp"

DialogNewPlace::DialogNewPlace(QDialog *parent) : QDialog(parent),
    _rowCounter(0),
    _editAddress(nullptr),
    _editPlaceName(nullptr),
    _editLatitude(nullptr),
    _editLongitude(nullptr),
    _typeOptionList(nullptr),
    _mondaySched(nullptr),
    _tuesdaySched(nullptr),
    _wednesdaySched(nullptr),
    _thrusdaySched(nullptr),
    _fridaySched(nullptr),
    _saturdaySched(nullptr),
    _sundaySched(nullptr)
{
    initializeDialog();
    setWindowTitle(tr("Insert a new place"));
    setModal(true);
}

DialogNewPlace::~DialogNewPlace()
{
    delete _editAddress;
    delete _editPlaceName;
    delete _editLatitude;
    delete _editLongitude;
    delete _typeOptionList;
    delete _mondaySched;
    delete _tuesdaySched;
    delete _wednesdaySched;
    delete _thrusdaySched;
    delete _fridaySched;
    delete _saturdaySched;
    delete _sundaySched;
}

QLineEdit *DialogNewPlace::createField(QGridLayout *layout, QString title, QString placeHolder, int row)
{
    QLineEdit *edit = new QLineEdit(this);
    edit->setPlaceholderText(placeHolder);
    edit->setStyleSheet("QCustomLineEdit{color: gray;}");
    QLabel *label = new QLabel(title, this);
    label->setBuddy(_editAddress);
    layout->addWidget(label, row, 0);
    layout->addWidget(edit, row, 1);
    return edit;
}

void DialogNewPlace::createTypeOption(QGridLayout *layout, QString title, int row)
{
    QLabel *label = new QLabel(title, this);
    _typeOptionList = new QComboBox(this);
    PlaceTypeMap placesTypes;
    /// We remove the key of all the places type
    QString key = placesTypes.mapToEnum.key(ALL_TYPES, "");
    if(!key.isEmpty())
    {
        placesTypes.mapToEnum.remove(key);
    }

    /// We create a list with all the keys of types we have
    for (auto &key : placesTypes.mapToEnum.keys())
    {
        _typeOptionList->addItem(key);
    }
    layout->addWidget(label, row, 0);
    layout->addWidget(_typeOptionList, row, 1);
}

DayScheduleGroup* DialogNewPlace::createDayScheduleField(QGridLayout *layout,
    QString title,
    DaysOfTheWeek day,
    int row)
{
    QLabel *label = new QLabel(title, this);
    layout->addWidget(label, row, 0);
    DayScheduleGroup* daySched = new DayScheduleGroup(day);
    layout->addWidget(daySched, row, 1);
    return daySched;
}

void DialogNewPlace::initializeDialog()
{
    // set up the layout
    QGridLayout *formGridLayout = new QGridLayout(this);

    /// User info fields
    _editAddress = createField(formGridLayout, tr("Address:"), tr("Ex: 2A Rue des Acacias"), _rowCounter++);
    _editPlaceName = createField(formGridLayout, tr("Place name:"), tr("Ex: Residence Des Acacias"), _rowCounter++);

    /// Lat and Long fields and its button to select them from the map
    _editLatitude = createField(formGridLayout, tr("Latitude:"), tr("Ex: 54.321"), _rowCounter++);
    _editLongitude = createField(formGridLayout, tr("Longitude:"), tr("Ex: 6.543"), _rowCounter++);
    QPushButton *latLongSelectButton = new QPushButton("Select Lat and Long from map", this);
    formGridLayout->addWidget(latLongSelectButton, _rowCounter++, 1, 1, 1);

    /// List of type of places
    createTypeOption(formGridLayout, tr("Place type"), _rowCounter++);

    /// Add day schedule options
    _mondaySched = createDayScheduleField(formGridLayout, tr("Monday"), MONDAY, _rowCounter++);
    _tuesdaySched = createDayScheduleField(formGridLayout, tr("Tuesday"), TUESDAY, _rowCounter++);
    _wednesdaySched = createDayScheduleField(formGridLayout, tr("Wednesday"), WEDNESDAY, _rowCounter++);
    _thrusdaySched = createDayScheduleField(formGridLayout, tr("Thrusday"), THRUSDAY, _rowCounter++);
    _fridaySched = createDayScheduleField(formGridLayout, tr("Friday"), FRIDAY, _rowCounter++);
    _saturdaySched = createDayScheduleField(formGridLayout, tr("Saturday"), SATURDAY, _rowCounter++);
    _sundaySched = createDayScheduleField(formGridLayout, tr("Sunday"), SUNDAY, _rowCounter++);

    // initialize buttons
    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    buttons->button(QDialogButtonBox::Ok)->setText(tr("Submit"));
    buttons->button(QDialogButtonBox::Cancel)->setText(tr("Cancel"));
    formGridLayout->addWidget(buttons, _rowCounter++, 0, 1, 2);

    // connects slots
    connect(buttons->button(QDialogButtonBox::Cancel),
            &QPushButton::clicked,
            this,
            &DialogNewPlace::onClickCancelButton);

    connect(buttons->button(QDialogButtonBox::Ok),
            &QPushButton::clicked,
            this,
            &DialogNewPlace::onClickSubmitButton);

    connect(latLongSelectButton,
            &QPushButton::clicked,
            this,
            &DialogNewPlace::onClickSelectFromMap);

    setLayout(formGridLayout);
}

void DialogNewPlace::onClickSubmitButton(bool checked)
{
    (void)checked;
    PlaceTypeMap placesTypes;
    bool lat_ok = false;
    bool lng_ok = false;
    /// We retrieve all the data from the user input
    QString name = _editPlaceName->text();
    QString addr = _editAddress->text();
    double lat = _editLatitude->text().toDouble(&lat_ok);
    double lng = _editLongitude->text().toDouble(&lng_ok);

    TypeOfPlace type = static_cast<TypeOfPlace>(placesTypes.mapToEnum.value(_typeOptionList->currentText(), UNKNOWN_TYPE));
    if(name.isEmpty())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. The place name cannot be empty");
    }
    else if(addr.isEmpty())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. The place address cannot be empty");
    }
    else if(!lat_ok)
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Latitude is not a number");
    }
    else if(!lng_ok)
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Longitude is not a number");
    }
    else if(type == UNKNOWN_TYPE || type == ALL_TYPES)
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Cannot recognize the entered type");
    }
    else if(!_mondaySched->isValid())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Monday schedule is not valid");
    }
    else if(!_tuesdaySched->isValid())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Tuesday schedule is not valid");
    }
    else if(!_wednesdaySched->isValid())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Wednesday schedule is not valid");
    }
    else if(!_thrusdaySched->isValid())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Thrusday schedule is not valid");
    }
    else if(!_fridaySched->isValid())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Friday schedule is not valid");
    }
    else if(!_saturdaySched->isValid())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Saturday schedule is not valid");
    }
    else if(!_sundaySched->isValid())
    {
        QMessageBox::warning(this, "Adding a new place", "Invalid. Sunday schedule is not valid");
    }
    else
    {
        PlaceObj myNewPlace(0, addr, name, lat, lng, type);
        myNewPlace.addDayToSchedule(_mondaySched->getSchedule());
        myNewPlace.addDayToSchedule(_tuesdaySched->getSchedule());
        myNewPlace.addDayToSchedule(_wednesdaySched->getSchedule());
        myNewPlace.addDayToSchedule(_thrusdaySched->getSchedule());
        myNewPlace.addDayToSchedule(_fridaySched->getSchedule());
        myNewPlace.addDayToSchedule(_saturdaySched->getSchedule());
        myNewPlace.addDayToSchedule(_sundaySched->getSchedule());
        if(DBEngine::instance()->insertNewPlace(myNewPlace))
        {
            QMessageBox::information(this, "Adding a new place", "Place successfully added");
            close();
        }
        else
        {
            QMessageBox::warning(this, "Adding a new place", "Invalid. This place already exists in the database");
        }
    }
}

void DialogNewPlace::onClickCancelButton(bool checked)
{
    (void)checked;
    /// If the user cancels, we just close the dialog
    close();
}

void DialogNewPlace::onClickSelectFromMap(bool checked)
{
    /// Inspired in the solution given here: https://stackoverflow.com/a/11507742
    (void)checked;
    hide();
    // This loop will wait for the window is destroyed
    connect(this,
            &QDialog::destroyed,
            &_loop,
            &QEventLoop::quit);

    _loop.exec();
    show();
}

void DialogNewPlace::onMapTapped(double lat, double lng)
{
    /// Whenever the user clicks on the map, we get that information,
    /// we set them into the right fields, and we stop the waiting loop
    _editLatitude->setText(QString::number(lat));
    _editLongitude->setText(QString::number(lng));
    _loop.quit();
}

void DialogNewPlace::showWindow(bool checked)
{
    (void)checked;
    /// We erase all the previous information entered before
    _editAddress->clear();
    _editPlaceName->clear();
    _editLatitude->clear();
    _editLongitude->clear();
    show();
    exec();
}
