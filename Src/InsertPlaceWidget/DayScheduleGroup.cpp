// Qt includes
#include <QHBoxLayout>
#include <QRadioButton>
#include <QTime>

/// Custom includes
#include "InsertPlaceWidget/DayScheduleGroup.hpp"

DayScheduleGroup::DayScheduleGroup(DaysOfTheWeek day, QWidget *parent) :
    QWidget(parent),
    _rowCounter(0),
    _morningStartTime(nullptr),
    _morningEndTime(nullptr),
    _afternoonStartTime(nullptr),
    _afternoonEndTime(nullptr),
    _layout(nullptr),
    _morningLabel(nullptr),
    _afternoonLabel(nullptr),
    _day(day)
{
    initializeGroup();
}

DayScheduleGroup::~DayScheduleGroup()
{
    delete _morningStartTime;
    delete _morningEndTime;
    delete _afternoonStartTime;
    delete _afternoonEndTime;
    delete _layout;
    delete _morningLabel;
    delete _afternoonLabel;
}

void DayScheduleGroup::addHoursLine(QGridLayout *layout,
    int row,
    QTimeEdit *&start,
    QTimeEdit *&end,
    QLabel *&label)
{
    /// All the new fields we set up has a default value of 8:00
    QTime defaultTime(8, 0);
    start = new QTimeEdit(defaultTime, this);
    start->setDisplayFormat("HH:mm");

    end = new QTimeEdit(defaultTime, this);
    end->setDisplayFormat("HH:mm");
    /// The label text is not set here, because it is going to
    /// change depending on the type of schedule
    label = new QLabel(this);
    layout->addWidget(label, row, 0);
    layout->addWidget(start, row, 1);
    layout->addWidget(end, row, 2);
}

void DayScheduleGroup::initializeGroup()
{
    /// Function based on the Qt example
    /// https://doc.qt.io/qt-5/qtwidgets-widgets-groupbox-example.html
    QRadioButton *closeAllDayRadio = new QRadioButton(tr("Close all day"));
    QRadioButton *closeMiddayRadio = new QRadioButton(tr("Closes at midday"));
    QRadioButton *oneSchedRadio = new QRadioButton(tr("Only one schedule"));

    /// We initialize the radio buttons for the type of schedule we want to set
    _layout = new QGridLayout;
    _layout->addWidget(closeAllDayRadio, _rowCounter, 0);
    _layout->addWidget(closeMiddayRadio, _rowCounter, 1);
    _layout->addWidget(oneSchedRadio, _rowCounter, 2);
    _rowCounter++;

    /// We initialize 2 schedule lines (morning and afternoon)
    addHoursLine(_layout, _rowCounter++, _morningStartTime, _morningEndTime, _morningLabel);
    addHoursLine(_layout, _rowCounter++, _afternoonStartTime, _afternoonEndTime, _afternoonLabel);

    /// Connections
    connect(closeAllDayRadio,
            &QRadioButton::toggled,
            this,
            &DayScheduleGroup::onHideEverything);

    connect(closeMiddayRadio,
            &QRadioButton::toggled,
            this,
            &DayScheduleGroup::onShowMorningAndAfternoonTime);

    connect(oneSchedRadio,
            &QRadioButton::toggled,
            this,
            &DayScheduleGroup::onShowMorningTime);

    /// We create a group and we place all the buttons there
    setLayout(_layout);

    /// By default, everything is closed all day, so we hide all
    /// the possible schedules
    closeAllDayRadio->setChecked(true);
    _morningLabel->hide();
    _afternoonLabel->hide();
    _morningStartTime->hide();
    _morningEndTime->hide();
    _afternoonStartTime->hide();
    _afternoonEndTime->hide();
    _schedType = CLOSED_ALL_DAY;
}

void DayScheduleGroup::onShowMorningTime(bool checked)
{
    /// We do something only if the radio button at which we
    /// are connected is checked
    if (checked)
    {
        _morningLabel->setText("Schedule:");
        _morningLabel->show();
        _afternoonLabel->hide();
        _morningStartTime->show();
        _morningEndTime->show();
        _afternoonStartTime->hide();
        _afternoonEndTime->hide();
        _schedType = ONLY_ONE_SCHEDULE;
    }
}

void DayScheduleGroup::onShowMorningAndAfternoonTime(bool checked)
{
    /// We do something only if the radio button at which we
    /// are connected is checked
    if (checked)
    {
        _morningLabel->setText("Morning:");
        _afternoonLabel->setText("Afternoon:");
        _morningLabel->show();
        _afternoonLabel->show();
        _morningStartTime->show();
        _morningEndTime->show();
        _afternoonStartTime->show();
        _afternoonEndTime->show();
        _schedType = CLOSES_AT_MIDDAY;
    }
}

void DayScheduleGroup::onHideEverything(bool checked)
{
    /// We do something only if the radio button at which we
    /// are connected is checked
    if (checked)
    {
        _morningLabel->hide();
        _afternoonLabel->hide();
        _morningStartTime->hide();
        _morningEndTime->hide();
        _afternoonStartTime->hide();
        _afternoonEndTime->hide();
        _schedType = CLOSED_ALL_DAY;
    }
}

bool DayScheduleGroup::isValid()
{
    if(_schedType == CLOSED_ALL_DAY)
    {
        /// If it is closed, that is already a valid state
        return true;
    }
    else if(_schedType == CLOSES_AT_MIDDAY)
    {
        /// We check if the morning schedule and afternoon schedules are consistent
        /// (closure > opening) and that the Afternoon starts after the morning

        return ((_morningEndTime->time() > _morningStartTime->time()) &&
            (_afternoonEndTime->time() > _afternoonStartTime->time()) &&
            (_afternoonStartTime->time() > _morningEndTime->time()));
    }
    else if(_schedType == ONLY_ONE_SCHEDULE)
    {
        /// We only need to check 2 hours: Opening and closure
        return (_morningEndTime->time() > _morningStartTime->time());
    }
    return false;
}

DaySchedule DayScheduleGroup::getSchedule()
{
    DaySchedule mySched(_day);
    switch (_schedType)
    {
        case CLOSED_ALL_DAY:
        {
            /// We do not need to do anything. It is already considered to be closed all day
            break;
        }
        case CLOSES_AT_MIDDAY:
        {
            Time24Hours morningStart(
                _morningStartTime->time().hour(),
                _morningStartTime->time().minute());

            Time24Hours morningEnd(
                _morningEndTime->time().hour(),
                _morningEndTime->time().minute());

            Time24Hours afternoonStart(
                _afternoonStartTime->time().hour(),
                _afternoonStartTime->time().minute());

            Time24Hours afternoonEnd(
                _afternoonEndTime->time().hour(),
                _afternoonEndTime->time().minute());

            mySched.setScheduleWithCloseAtMidday(morningStart,
                morningEnd,
                afternoonStart,
                afternoonEnd);
            break;
        }
        case ONLY_ONE_SCHEDULE:
        {
            Time24Hours start(
                _morningStartTime->time().hour(),
                _morningStartTime->time().minute());

            Time24Hours end(
                _morningEndTime->time().hour(),
                _morningEndTime->time().minute());

            mySched.setScheduleWithoutClosureTime(start, end);
            break;
        }
    }
    return mySched;
}
