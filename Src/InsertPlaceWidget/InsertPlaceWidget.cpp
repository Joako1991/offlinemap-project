/// Qt includes
#include <QPushButton>

/// Custom includes
#include "InsertPlaceWidget/InsertPlaceWidget.hpp"

InsertPlaceWidget::InsertPlaceWidget(QWidget *parent) :
    QWidget(parent),
    _layout(nullptr),
    _dialog(nullptr)
{
    initializeWidget();
}

InsertPlaceWidget::~InsertPlaceWidget()
{
    delete _layout;
    delete _dialog;
}

void InsertPlaceWidget::initializeWidget()
{
    _layout = new QHBoxLayout(this);
    _layout->setMargin(0);
    QPushButton *addNewPlace = new QPushButton("Insert new place", this);
    _layout->addWidget(addNewPlace);
    setLayout(_layout);

    _dialog = new DialogNewPlace();

    connect(addNewPlace,
        &QPushButton::clicked,
        _dialog,
        &DialogNewPlace::showWindow);
}

void InsertPlaceWidget::mapInformationReceived(double lat, double lng)
{
    _dialog->onMapTapped(lat, lng);
}