// STL includes
#include <iostream>

// Custom includes
#include "DBEngine/DBEngine.hpp"
#include <DBEngine/DefaultComments.hpp>
#include <DBEngine/DefaultPlaces.hpp>
#include <User.hpp>

DBEngine* DBEngine::_instance = nullptr;

static int databaseStatementResultCallback(
    void *param,
    int columns_in_res,
    char **columns_values,
    char **columns_names)
{
    static_cast<DBEngine*>(param)->resultsOfQuery(columns_in_res, columns_values, columns_names);
    return 0;
}

DBEngine *DBEngine::instance()
{
    if (!_instance)
    {
        _instance = new DBEngine;
    }
    return _instance;
}

DBEngine::DBEngine() :
    _filepath(""),
    _lastCountQueryValue(0),
    _db(nullptr),
    _isOpened(false),
    _currentQueryType(UNKNOWN)
{}

DBEngine::~DBEngine()
{
    sqlite3_close(_db);
}

QVector<PlaceObj> DBEngine::getPlacesBasedTypeAndName(TypeOfPlace type, std::string name)
{
    _lastPlacesQuery.clear();
    if(type != ALL_TYPES && type != UNKNOWN_TYPE && type != AMOUNT_OF_TYPES)
    {
        evaluateSQLStatement(FILTER_PLACES, SQLQueries::filterPlacesByTypeAndName(type, name));
        return _lastPlacesQuery;
    }
    else
    {
        // Name can be empty or it can have some letters. Because the query uses LIKE
        // SQL keyword to compare the input, if name is empty, the query
        // will return all the places
        evaluateSQLStatement(FILTER_PLACES, SQLQueries::filterPlacesByName(name));
        return _lastPlacesQuery;
    }
}

void DBEngine::loadDefaultDatabase()
{
    DefaultPlaces defaultPlaces;
    defaultPlaces.loadPlaces();
    QVector<PlaceObj> places = defaultPlaces.getPlaces();
    for(auto &place : places)
    {
        if(place.isValidPlace())
        {
            evaluateSQLStatement(INSERT, SQLQueries::insertPlace(place));
        }
    }
}

void DBEngine::loadUsersDefaultDatabase()
{
    User adminUser(QString("admin"), QString("nn@nn.com"), QDate(1991, 1, 1), QString("admin"));
    insertUserInfo(adminUser);
}

void DBEngine::loadDefaultComments()
{
   DefaultComments defaultComments;
   defaultComments.loadComments();
   QVector<CommentObj> comments = defaultComments.getComments();
   for(auto &comment : comments)
   {
       if(comment.isCommentValid())
       {
           evaluateSQLStatement(INSERT, SQLQueries::insertComment(comment));
       }
   }
}

void DBEngine::closeDatabase()
{
    sqlite3_close(_db);
    _isOpened = false;
}

bool DBEngine::loadDatabase(QString db_filepath)
{
    /// Implementation taken from https://www.tutorialspoint.com/sqlite/sqlite_c_cpp.htm
    closeDatabase();
    _filepath = db_filepath;
    int r = sqlite3_open(_filepath.toStdString().c_str(), &_db);
    if (r)
    {
        std::cerr << "Can't open database: " << sqlite3_errmsg(_db) << std::endl;
        return false;
    }
    else
    {
        std::cout << "Database opened successfully" << std::endl;
    }
    _isOpened = true;
    if(isPlacesDatabaseEmpty())
    {
        loadDefaultDatabase();
    }

    if(isUsersDatabaseEmpty())
    {
        loadUsersDefaultDatabase();
    }

    if(isCommentsDatabaseEmpty())
    {
        loadDefaultComments();
    }

    return true;
}

bool DBEngine::isPlacesDatabaseEmpty()
{
    /// If the table does exists, then this command does anything
    evaluateSQLStatement(CREATE_TABLE, SQLQueries::createTableOfPlaces());
    /// We check if the Database is empty or not
    evaluateSQLStatement(COUNT, SQLQueries::getAmountOfPlaces());
    return _lastCountQueryValue == 0;
}

bool DBEngine::isUsersDatabaseEmpty()
{
    /// If the table does exists, then this command does anything
    evaluateSQLStatement(CREATE_TABLE, SQLQueries::createTableOfUsers());
    /// We check if the Database is empty or not
    evaluateSQLStatement(COUNT, SQLQueries::getAmountOfUsers());
    return _lastCountQueryValue == 0;
}

bool DBEngine::isCommentsDatabaseEmpty()
{
    /// If the table does exists, then this command does anything
    evaluateSQLStatement(CREATE_TABLE, SQLQueries::createTableOfComments());
    /// We check if the Database is empty or not
    evaluateSQLStatement(COUNT, SQLQueries::getAmountOfComments());
    return _lastCountQueryValue == 0;
}

std::vector<User> DBEngine::getUserInfoValues(std::string username, std::string password)
{
    _lastUserQuery.clear();
    evaluateSQLStatement(FILTER_USERS, SQLQueries::getUserInfo(username, password));
    return _lastUserQuery;
}

std::vector<User> DBEngine::getUserInfoValuesBasedUserName(std::string username)
{
    _lastUserQuery.clear();
    evaluateSQLStatement(FILTER_USERS, SQLQueries::getUserInfoBasedUserName(username));
    return _lastUserQuery;
}

bool DBEngine::insertUserInfo(const User& newUser)
{
    return evaluateSQLStatement(INSERT, SQLQueries::insertUser(newUser));
}

bool DBEngine::updateUserPassword(const User& user)
{
    return evaluateSQLStatement(UPDATE, SQLQueries::updateUserPassword(user));
}

bool DBEngine::insertNewPlace(const PlaceObj& newPlace)
{
    return evaluateSQLStatement(INSERT, SQLQueries::insertPlace(newPlace));
}

bool DBEngine::eraseAPlaceFromDatabase(const PlaceObj& pl)
{
    return evaluateSQLStatement(ERASE, SQLQueries::eraseAPlace(pl));
}

QVector<PlaceObj> DBEngine::getListOfPlaces()
{
    _lastPlacesQuery.clear();
    evaluateSQLStatement(FILTER_PLACES, SQLQueries::getAllPlaces());
    return _lastPlacesQuery;
}

bool DBEngine::checkIfPlaceExists(std::string placeName)
{
    _lastPlacesQuery.clear();
    bool res = evaluateSQLStatement(FILTER_PLACES, SQLQueries::filterPlacesByName(placeName));
    if(res && _lastPlacesQuery.size() == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool DBEngine::insertNewComment(const CommentObj& newComment)
{
    if(newComment.isCommentValid() && checkIfPlaceExists(newComment.getPlaceName()))
    {
        return evaluateSQLStatement(INSERT, SQLQueries::insertComment(newComment));
    }
    else
    {
        return false;
    }
}

bool DBEngine::getPlaceAverageReputation(std::string placeName, double &reputation)
{
    /// We check if the place exists, before asking for comments
    if(checkIfPlaceExists(placeName))
    {
        _lastAverageValue = -1;
        bool res = evaluateSQLStatement(AVERAGE, SQLQueries::getGlobalPlaceReputation(placeName));
        reputation = _lastAverageValue;
        return res;
    }
    reputation = -1;
    return false;
}

bool DBEngine::getPlaceComments(std::string placeName, std::vector<CommentObj> &comments)
{
    _lastCommentsQuery.clear();
    bool res = evaluateSQLStatement(FILTER_COMMENTS, SQLQueries::getPlaceComments(placeName));
    comments = _lastCommentsQuery;
    return res;
}

bool DBEngine::evaluateSQLStatement(TypeOfQuery type, std::string query)
{
    bool res = true;
    if (_isOpened)
    {
        char *zErrMsg = nullptr;
        _currentQueryType = type;
        /// If query is an empty pointer or NULL, exec won't do anything
        int r = sqlite3_exec(
            _db,                             /* An open database */
            query.c_str(),                   /* SQL to be evaluated */
            databaseStatementResultCallback, /* Callback function */
            this,                            /* 1st argument to callback */
            &zErrMsg                         /* Error msg written here */
        );
        if (r != SQLITE_OK)
        {
            std::cerr << "SQL error: " << zErrMsg << std::endl;
            sqlite3_free(zErrMsg);
            res = false;
        }
    }
    else
    {
        std::cout << "Cannot execute SQL statement. Database has not been initialized"
                  << std::endl;
        res = false;
    }
    return res;
}

QVector<PlaceObj> DBEngine::getPlacesBasedType(TypeOfPlace type)
{
    return getPlacesBasedTypeAndName(type, "");
}

QVector<PlaceObj> DBEngine::getPlacesBasedName(std::string name)
{
    return getPlacesBasedTypeAndName(ALL_TYPES, name);
}

QVector<PlaceObj> DBEngine::getPlacesBasedAddress(std::string addr)
{
    _lastPlacesQuery.clear();
    evaluateSQLStatement(FILTER_PLACES, SQLQueries::filterPlacesByAddress(addr));
    return _lastPlacesQuery;
}

void DBEngine::resultsOfQuery(int amount_of_cols, char **argv, char **colName)
{
    /// Implementation based on https://www.sqlite.org/quickstart.html
    /// Variable declaration here to avoid compilation error due to "jump to case label"
    bool ok = false;
    QString s;
    switch(_currentQueryType)
    {
        case COUNT:
            /// Sanity check
            if(amount_of_cols != 1)
            {
                std::cout << "ERROR I was expecting the count and I got "
                          << amount_of_cols
                          << " columns"
                          << std::endl;
                assert(0);
            }

            _lastCountQueryValue = atoi(argv[0]);
            break;
        case AVERAGE:
            /// Sanity check
            if(amount_of_cols != 1)
            {
                std::cout << "ERROR I was expecting the count and I got "
                          << amount_of_cols
                          << " columns"
                          << std::endl;
                assert(0);
            }
            /// Inspired in the implementation given at https://stackoverflow.com/a/10300665
            s = QString(argv[0]);
            if(s.isEmpty())
            {
                _lastAverageValue = 0;
            }
            else
            {
                _lastAverageValue = s.toDouble(&ok);
                if(!ok)
                {
                    std::cout << "Error converting " << argv[0] << " into double!!!" << std::endl;
                    assert(0);
                }
            }
            break;
        case FILTER_PLACES:
            _lastPlacesQuery.push_back(SQLQueries::convertQueryResultToPlace(amount_of_cols, colName, argv));
            break;
        case FILTER_USERS:
            _lastUserQuery.push_back(SQLQueries::convertQueryResultToUser(amount_of_cols, colName, argv));
            break;
        case FILTER_COMMENTS:
            _lastCommentsQuery.push_back(SQLQueries::convertQueryResultToComment(amount_of_cols, colName, argv));
            break;
        default:
            // This case should never happen
            std::cout << "No recognized type of query. Got " << _currentQueryType << std::endl;
            assert(0);
    }
}
