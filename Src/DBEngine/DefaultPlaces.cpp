#include "DBEngine/DefaultPlaces.hpp"

static PlaceObj residenceDesAcacias(int id)
{
    PlaceObj residence(
        id,
        "2A Rue des acacias",
        "Residence Les Acacias",
        46.798433,
        4.425779,
        RESIDENCE_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(0,0), Time24Hours(23,59));
    residence.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    residence.addDayToSchedule(sched);

    return residence;
}

static PlaceObj hotelLaPetiteVerrerie(int id)
{
    PlaceObj hotel(
        id,
        "4 Rue Jules Guesde",
        "La Petite Verrerie",
        46.806335,
        4.424762,
        HOTEL_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(0,0), Time24Hours(23,59));
    hotel.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    hotel.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    hotel.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    hotel.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    hotel.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    hotel.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    hotel.addDayToSchedule(sched);

    return hotel;
}

static PlaceObj residenceJeanMonnet(int id)
{
    PlaceObj residence(
        id,
        "350 Avenue Jean Monnet",
        "Residence Jean Monnet",
        46.80707072596702,
        4.422049784663223,
        RESIDENCE_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(0,0), Time24Hours(23,59));
    residence.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    residence.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    residence.addDayToSchedule(sched);

    return residence;
}

static PlaceObj restaurantLeFutMets(int id)
{
    PlaceObj restaurant(
        id,
        "2 bis Rue de la Couronne",
        "Le Fût-Mets",
        46.802245,
        4.417324,
        RESTAURANT_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(11,0), Time24Hours(15,0));
    restaurant.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.setScheduleWithCloseAtMidday(
        Time24Hours(11,0),
        Time24Hours(15,0),
        Time24Hours(17,30),
        Time24Hours(0,0));
    restaurant.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(17,30), Time24Hours(0,0));
    restaurant.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    restaurant.addDayToSchedule(sched);

    return restaurant;
}

static PlaceObj universityIUT(int id)
{
    PlaceObj university(
        id,
        "12 Rue de la Fonderie",
        "Institut Universitaire de Technologie (IUT)",
        46.806300,
        4.414220,
        UNIVERSITY_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(8,0), Time24Hours(18,0));
    university.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    university.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    university.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    university.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    university.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.placeClosed();
    university.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    university.addDayToSchedule(sched);

    return university;
}

static PlaceObj universityCondorce(int id)
{
    PlaceObj university(
        id,
        "720 Avenue de Europe",
        "Centre Universitaire Condorcet",
        46.808430,
        4.421770,
        UNIVERSITY_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(8,0), Time24Hours(18,0));
    university.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    university.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    university.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    university.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    university.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.placeClosed();
    university.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    university.addDayToSchedule(sched);

    return university;
}

static PlaceObj supermarketCarrefour(int id)
{
    PlaceObj supermarket(
        id,
        "Avenue François Mitterrand Cc Arche",
        "Carrefour Market",
        46.807006,
        4.429974,
        SUPERMARKET_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(8,30), Time24Hours(20,0));
    supermarket.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(9,0), Time24Hours(12,30));
    supermarket.addDayToSchedule(sched);

    return supermarket;
}

static PlaceObj supermarketAldi(int id)
{
    PlaceObj supermarket(
        id,
        "21 Rue Arcole",
        "ALDI",
        46.796260,
        4.428110,
        SUPERMARKET_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(8,30), Time24Hours(20,0));
    supermarket.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    supermarket.addDayToSchedule(sched);

    return supermarket;
}

static PlaceObj supermarketAlMercatino(int id)
{
    PlaceObj supermarket(
        id,
        "55 Rue Marechal Foch",
        "AL MERCATINO",
        46.805100,
        4.437920,
        SUPERMARKET_TYPE);

    DaySchedule sched(MONDAY);
    sched.placeClosed();
    supermarket.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(9,30), Time24Hours(12,0), Time24Hours(14,30), Time24Hours(19,30));
    supermarket.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    supermarket.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(9,0), Time24Hours(13,0));
    supermarket.addDayToSchedule(sched);

    return supermarket;
}

static PlaceObj restaurantNonnaRosa(int id)
{
    PlaceObj restaurant(
        id,
        "4 Rue de la Verrerie",
        "Nonna Rosa",
        46.805556,
        4.420291,
        RESTAURANT_TYPE);

    DaySchedule sched(MONDAY);
    sched.placeClosed();
    restaurant.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(12,0), Time24Hours(14,0), Time24Hours(19,0), Time24Hours(22,30));
    restaurant.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    restaurant.addDayToSchedule(sched);

    return restaurant;
}

static PlaceObj restaurantLaKashmir(int id)
{
    PlaceObj restaurant(
        id,
        "11 Rue de la Verrerie",
        "La Kashmir",
        46.805570,
        4.419750,
        RESTAURANT_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(11,0), Time24Hours(14,0), Time24Hours(18,30), Time24Hours(23,0));
    restaurant.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    restaurant.addDayToSchedule(sched);

    return restaurant;
}

static PlaceObj restaurantLeCreusotin(int id)
{
    PlaceObj restaurant(
        id,
        "36 Rue Jean Jaurès",
        "Le Creusotin",
        46.806110,
        4.417920,
        RESTAURANT_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(11,55), Time24Hours(13,15), Time24Hours(18,55), Time24Hours(20,35));
    restaurant.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.placeClosed();
    restaurant.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    restaurant.addDayToSchedule(sched);

    return restaurant;
}

static PlaceObj restaurantShao(int id)
{
    PlaceObj restaurant(
        id,
        "Avenue François Mitterrand",
        "Shao",
        46.806679,
        4.429920,
        RESTAURANT_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(12,0), Time24Hours(14,30), Time24Hours(19,0), Time24Hours(22,30));
    restaurant.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    restaurant.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    restaurant.addDayToSchedule(sched);

    return restaurant;
}

static PlaceObj fastfoodMcDonalds(int id)
{
    PlaceObj fastfood(
        id,
        "Avenue de Europe",
        "McDonals",
        46.808871,
        4.428509,
        FASTFOOD_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(10,0), Time24Hours(23,30));
    fastfood.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    fastfood.addDayToSchedule(sched);

    return fastfood;
}

static PlaceObj fastfoodSubway(int id)
{
    PlaceObj fastfood(
        id,
        "17 Rue de Pologne",
        "Subway",
        46.791640,
        4.446860,
        FASTFOOD_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(10,0), Time24Hours(22,30));
    fastfood.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    fastfood.addDayToSchedule(sched);

    return fastfood;
}

static PlaceObj fastfoodRoyalTacos(int id)
{
    PlaceObj fastfood(
        id,
        "62 Rue Jean Jaurès",
        "Royal Tacos",
        46.805830,
        4.416200,
        FASTFOOD_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(11,30), Time24Hours(13,30), Time24Hours(18,0), Time24Hours(22,30));
    fastfood.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(18,0), Time24Hours(23,0));
    fastfood.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    fastfood.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    fastfood.addDayToSchedule(sched);

    return fastfood;
}

static PlaceObj bakeryDurqueEric(int id)
{
    PlaceObj bakery(
        id,
        "17 Rue Jean Jaurès",
        "Durque Eric",
        46.806100,
        4.419490,
        BAKERY_TYPE);

    DaySchedule sched(MONDAY);
    sched.placeClosed();
    bakery.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(6,30), Time24Hours(19,30));
    bakery.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    bakery.addDayToSchedule(sched);

    return bakery;
}

static PlaceObj bakeryPetitjeanLudovic(int id)
{
    PlaceObj bakery(
        id,
        "105 Rue Eidth Cavell",
        "Petitjean Ludovic",
        46.795730,
        4.424730,
        BAKERY_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(7,0), Time24Hours(19,30));
    bakery.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    bakery.addDayToSchedule(sched);

    return bakery;
}

static PlaceObj bakeryFleurentinDavid(int id)
{
    PlaceObj bakery(
        id,
        "12 Rue de la Couronne",
        "Fleurentin David",
        46.802000,
        4.418120,
        BAKERY_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(6,30), Time24Hours(13,0), Time24Hours(15,30), Time24Hours(19,0));
    bakery.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    bakery.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(6,30), Time24Hours(13,0), Time24Hours(15,30), Time24Hours(19,0));
    bakery.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    bakery.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(6,30), Time24Hours(13,0));
    bakery.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    bakery.addDayToSchedule(sched);

    return bakery;
}

static PlaceObj parkParcDeLaVerrerie(int id)
{
    PlaceObj park(
        id,
        "71200 Le Creusot",
        "Parc de la Verrerie",
        46.803023,
        4.423068,
        PARK_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(0,0), Time24Hours(23,59));
    park.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    park.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    park.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    park.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    park.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    park.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    park.addDayToSchedule(sched);

    return park;
}

static PlaceObj culturalChateauDeLaVerrerie(int id)
{
    PlaceObj cultural(
        id,
        "71 Place Schneider",
        "Château de la Verrerie",
        46.805770,
        4.420980,
        CULTURAL_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(10,0), Time24Hours(12,0), Time24Hours(14,0), Time24Hours(17,30));
    cultural.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.placeClosed();
    cultural.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(10,0), Time24Hours(12,0), Time24Hours(14,0), Time24Hours(17,30));
    cultural.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    cultural.addDayToSchedule(sched);

    return cultural;
}

static PlaceObj culturalArcSceneNationaleEuropeenne(int id)
{
    PlaceObj cultural(
        id,
        "Espace François Mitterrand",
        "Arc Scene Nationale Européenne",
        46.806350,
        4.429330,
        CULTURAL_TYPE);

    DaySchedule sched(MONDAY);
    sched.placeClosed();
    cultural.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(13,30), Time24Hours(18,30));
    cultural.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(15,0), Time24Hours(18,0));
    cultural.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    cultural.addDayToSchedule(sched);

    return cultural;
}

static PlaceObj culturalCinemaLeMorvan(int id)
{
    PlaceObj cultural(
        id,
        "8 Rue de la Verrerie",
        "Cinéma Le Morvan",
        46.805600,
        4.420040,
        CULTURAL_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(14,0), Time24Hours(23,59));
    cultural.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    cultural.addDayToSchedule(sched);

    return cultural;
}

static PlaceObj culturalAmpli(int id)
{
    PlaceObj cultural(
        id,
        "2 Avenue François Mitterrand",
        "Ampli",
        46.806400,
        4.430150,
        CULTURAL_TYPE);

    DaySchedule sched(MONDAY);
    sched.placeClosed();
    cultural.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(14,0), Time24Hours(23,59));
    cultural.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(14,0), Time24Hours(18,0));
    cultural.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    cultural.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    cultural.addDayToSchedule(sched);

    return cultural;
}

static PlaceObj flowerLeGardenia(int id)
{
    PlaceObj flower(
        id,
        "34 Rue Edith Cavell",
        "Le Gardénia",
        46.801206,
        4.420612,
        FLOWER_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(9,0), Time24Hours(12,15), Time24Hours(14,15), Time24Hours(19,30));
    flower.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    flower.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(9,0), Time24Hours(13,0));
    flower.addDayToSchedule(sched);

    return flower;
}

static PlaceObj flowerSarlRivieraFleurs(int id)
{
    PlaceObj flower(
        id,
        "19 Rue du Maréchal Leclerc",
        "Sarl Rivièra Fleurs",
        46.806950,
        4.426330,
        FLOWER_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(8,0), Time24Hours(12,0), Time24Hours(14,0), Time24Hours(19,0));
    flower.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(8,0), Time24Hours(19,30));
    flower.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    flower.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(8,0), Time24Hours(13,0));
    flower.addDayToSchedule(sched);

    return flower;
}

static PlaceObj flowerAFleurDePot(int id)
{
    PlaceObj flower(
        id,
        "93 Rue Président Wilson",
        "A fleur de pot",
        46.795848,
        4.427662,
        FLOWER_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(9,0), Time24Hours(12,30), Time24Hours(14,0), Time24Hours(19,0));
    flower.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    flower.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(9,0), Time24Hours(14,30));
    flower.addDayToSchedule(sched);

    return flower;
}

static PlaceObj flowerChlorophyl(int id)
{
    PlaceObj flower(
        id,
        "196 Rue Maréchal Foch",
        "Chlorophyl",
        46.803342,
        4.451984,
        FLOWER_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(8,0), Time24Hours(12,30), Time24Hours(14,0), Time24Hours(19,0));
    flower.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(8,0), Time24Hours(19,30));
    flower.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    flower.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    flower.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(8,0), Time24Hours(13,0));
    flower.addDayToSchedule(sched);

    return flower;
}

static PlaceObj hairdresserFranzCoiffure(int id)
{
    PlaceObj hairdresser(
        id,
        "14 Rue de la Couronne",
        "Franz Coiffure",
        46.802343,
        4.418377,
        HAIRDRESSER_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(8,0), Time24Hours(12,0), Time24Hours(14,0), Time24Hours(19,0));
    hairdresser.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(8,0), Time24Hours(12,0), Time24Hours(13,0), Time24Hours(17,0));
    hairdresser.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    return hairdresser;
}

static PlaceObj hairdresserInstantCoiffure(int id)
{
    PlaceObj hairdresser(
        id,
        "Avenue François Mitterrand",
        "Instant Coiffure",
        46.807404,
        4.429805,
        HAIRDRESSER_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(9,0), Time24Hours(19,30));
    hairdresser.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    return hairdresser;
}

static PlaceObj hairdresserKalistaCoiffure(int id)
{
    PlaceObj hairdresser(
        id,
        "24 Rue du Maréchal Leclerc",
        "Kalista Coiffure",
        46.806715,
        4.427004,
        HAIRDRESSER_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(14,0), Time24Hours(19,0));
    hairdresser.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(9,0), Time24Hours(19,0));
    hairdresser.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(9,0), Time24Hours(18,0));
    hairdresser.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    return hairdresser;
}

static PlaceObj hairdresserSalonBruno(int id)
{
    PlaceObj hairdresser(
        id,
        "38 Rue Jean Jaurès",
        "Salon Bruno",
        46.806264,
        4.419229,
        HAIRDRESSER_TYPE);

    DaySchedule sched(MONDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(8,0), Time24Hours(12,0), Time24Hours(13,30), Time24Hours(19,0));
    hairdresser.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    hairdresser.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(7,30), Time24Hours(15,0));
    hairdresser.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    hairdresser.addDayToSchedule(sched);

    return hairdresser;
}

static PlaceObj clothesPantashop(int id)
{
    PlaceObj clothes(
        id,
        "25 Rue du Maréchal Leclerc",
        "Pantashop",
        46.807150,
        4.427036,
        CLOTHES_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(10,0), Time24Hours(12,0), Time24Hours(14,0), Time24Hours(19,0));
    clothes.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    clothes.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    clothes.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    clothes.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    clothes.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    clothes.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    clothes.addDayToSchedule(sched);

    return clothes;
}

static PlaceObj postLaPoste(int id)
{
    PlaceObj post(
        id,
        "29 Rue Jules Guesde",
        "La Poste",
        46.806321,
        4.428848,
        POST_TYPE);

    DaySchedule sched(MONDAY);
    sched.setScheduleWithCloseAtMidday(Time24Hours(9,0), Time24Hours(12,30), Time24Hours(13,30), Time24Hours(18,0));
    post.addDayToSchedule(sched);

    sched.setDay(TUESDAY);
    post.addDayToSchedule(sched);

    sched.setDay(WEDNESDAY);
    sched.placeClosed();
    post.addDayToSchedule(sched);

    sched.setDay(THRUSDAY);
    post.addDayToSchedule(sched);

    sched.setDay(FRIDAY);
    post.addDayToSchedule(sched);

    sched.setDay(SATURDAY);
    sched.setScheduleWithoutClosureTime(Time24Hours(9,0), Time24Hours(12,0));
    post.addDayToSchedule(sched);

    sched.setDay(SUNDAY);
    sched.placeClosed();
    post.addDayToSchedule(sched);

    return post;
}

void DefaultPlaces::loadPlaces()
{
    int idCounter = 0;
    /// We clear the places before starting to avoid dupplications
    _places.clear();
    _places.push_back(residenceDesAcacias(idCounter++));
    _places.push_back(hotelLaPetiteVerrerie(idCounter++));
    _places.push_back(residenceJeanMonnet(idCounter++));
    _places.push_back(restaurantLeFutMets(idCounter++));
    _places.push_back(universityIUT(idCounter++));
    _places.push_back(universityCondorce(idCounter++));
    _places.push_back(supermarketCarrefour(idCounter++));
    _places.push_back(supermarketAldi(idCounter++));
    _places.push_back(supermarketAlMercatino(idCounter++));
    _places.push_back(restaurantNonnaRosa(idCounter++));
    _places.push_back(restaurantLaKashmir(idCounter++));
    _places.push_back(restaurantLeCreusotin(idCounter++));
    _places.push_back(restaurantShao(idCounter++));
    _places.push_back(fastfoodMcDonalds(idCounter++));
    _places.push_back(fastfoodSubway(idCounter++));
    _places.push_back(fastfoodRoyalTacos(idCounter++));
    _places.push_back(bakeryDurqueEric(idCounter++));
    _places.push_back(bakeryPetitjeanLudovic(idCounter++));
    _places.push_back(bakeryFleurentinDavid(idCounter++));
    _places.push_back(parkParcDeLaVerrerie(idCounter++));
    _places.push_back(culturalChateauDeLaVerrerie(idCounter++));
    _places.push_back(culturalArcSceneNationaleEuropeenne(idCounter++));
    _places.push_back(culturalCinemaLeMorvan(idCounter++));
    _places.push_back(culturalAmpli(idCounter++));
    _places.push_back(flowerLeGardenia(idCounter++));
    _places.push_back(flowerSarlRivieraFleurs(idCounter++));
    _places.push_back(flowerAFleurDePot(idCounter++));
    _places.push_back(flowerChlorophyl(idCounter++));
    _places.push_back(hairdresserFranzCoiffure(idCounter++));
    _places.push_back(hairdresserInstantCoiffure(idCounter++));
    _places.push_back(hairdresserKalistaCoiffure(idCounter++));
    _places.push_back(hairdresserSalonBruno(idCounter++));
    _places.push_back(clothesPantashop(idCounter++));
    _places.push_back(postLaPoste(idCounter++));
}