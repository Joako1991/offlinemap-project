// Qt includes
#include <QString>

// STL includes
#include <iostream>
#include <sstream>

// Custom includes
#include "DBEngine/DBQueries.hpp"

#define PLACES_TABLE_NAME       "PLACES"
#define PLACES_ID_FIELD         "ID"
#define PLACES_NAME_FIELD       "NAME"
#define PLACES_ADDR_FIELD       "ADDR"
#define PLACES_LAT_FIELD        "LAT"
#define PLACES_LNG_FIELD        "LNG"
#define PLACES_TYPE_FIELD       "TYPE_OF_PLACE"
#define PLACES_SCHEDULE_FIELD   "SCHEDULE"

#define USERS_TABLE_NAME         "USERS"
#define USERS_ID_FIELD           "ID"
#define USERS_USERNAME_FIELD     "USERNAME"
#define USERS_PASSWORD_FIELD     "PASSWORD"
#define USERS_EMAIL_FIELD        "EMAIL"
#define USERS_BIRTH_FIELD        "BIRTHDAY"

#define COMMENTS_TABLE_NAME      "COMMENTS_TABLE"
#define COMMENT_ID_FIELD         "ID"
#define COMMENT_PLACENAME_FIELD  "PLACE_NAME"
#define COMMENT_USERNAME_FIELD   "USERNAME"
#define COMMENT_REPUTATION_FIELD "REPUTATION"
#define COMMENT_TEXT_FIELD       "COMMENT"
#define COMMENT_DATE_FIELD       "COMMENT_DATE"

/**
 * @brief Function to fix SQL problems with the appostrof character.
 *  When we handle strings in SQL, we have to enclose them between single quotes.
 *  Nonetheless, this character is also used within the text we want to query about.
 *  For instance, we can have a query this this one:
 *      SELECT * FROM PLACES WHERE ADDR='720 Ave de l'Europe'
 *  In this case we can clearly see that Ave de l'Europe has an appostrof between the
 *  the single quotes of the query itself. This will break the query statement, and a
 *  SQL error will be thrown showing that there is a problem close to the E.
 *  In contrast with how C++ handles the single quotes characters, SQL accepts contiguous
 *  double single quotes, as a single quote. For the example previously mentioned, the
 *  right query will be:
 *      SELECT * FROM PLACES WHERE ADDR='720 Ave de l''Europe'
 *  This function solves the problem. It looks for all the contiguous appostrofs, and
 *  it replaces all of them (the input can be "l'Europe" or "l'''''''''Europe") by
 *  2 contiguous single quotes, so then the SQL statement can be executed properly.
 *
 * @param query: Input string to remove appostrofs. It should be the argument of the
 *   SQL query, without the side quotes.
 *
 * @returns: String with the replaced appostrofs.
*/
static std::string correctQueryApostrof(std::string query)
{
    if (query.find('\'') != std::string::npos)
    {
        std::string out;
        bool apostrof_founded = false;

        for(unsigned i = 0; i < query.size(); i++)
        {
            if(query[i] == '\'')
            {
                /// If we find an appostrof, then we just set the flag, and we
                /// don't do anything else
                apostrof_founded = true;
            }
            else
            {
                if(!apostrof_founded)
                {
                    /// If we did not have an appostrof before, we just copy the
                    /// current character
                    out.push_back(query[i]);
                }
                else
                {
                    /// If we had appostrofs before, we just append two appostrofs
                    /// and the current character
                    out.push_back('\'');
                    out.push_back('\'');
                    out.push_back(query[i]);
                }
                apostrof_founded = false;
            }
        }
        return out;
    }
    else
    {
        return query;
    }
}

std::string SQLQueries::getAllElements(std::string orderingKey, std::string tableName)
{
    std::stringstream stream;
    stream << "SELECT * FROM "
           << tableName
           << " ORDER BY " << orderingKey << " ASC;";
    return stream.str();
}

std::string SQLQueries::getAmountOfElements(std::string id_key, std::string tableName)
{
    std::stringstream stream;
    stream << "SELECT count(" << id_key << ") FROM "
           << tableName;
    return stream.str();
}

std::string SQLQueries::eraseTable(std::string tableName)
{
    std::stringstream stream;
    stream << "DROP TABLE IF EXISTS "
           << tableName
           << ";";
    return stream.str();
}

std::string SQLQueries::createTableOfPlaces()
{
    std::stringstream stream;
    stream << "CREATE TABLE IF NOT EXISTS " << PLACES_TABLE_NAME << "("
           << PLACES_ID_FIELD       << " INTEGER PRIMARY KEY NOT NULL, "
           << PLACES_NAME_FIELD     << " TEXT                NOT NULL, "
           << PLACES_ADDR_FIELD     << " TEXT                NOT NULL, "
           << PLACES_LAT_FIELD      << " REAL                NOT NULL, "
           << PLACES_LNG_FIELD      << " REAL                NOT NULL, "
           << PLACES_TYPE_FIELD     << " INTEGER             NOT NULL, "
           << PLACES_SCHEDULE_FIELD << " TEXT                NOT NULL );";
    return stream.str();
}

std::string SQLQueries::createTableOfUsers()
{
    std::stringstream stream;
    stream << "CREATE TABLE IF NOT EXISTS " << USERS_TABLE_NAME << "("
           << USERS_USERNAME_FIELD  << " TEXT PRIMARY KEY NOT NULL, "
           << USERS_PASSWORD_FIELD  << " TEXT             NOT NULL, "
           << USERS_EMAIL_FIELD     << " TEXT             NOT NULL UNIQUE, "
           << USERS_BIRTH_FIELD     << " DATE             NOT NULL);";
    return stream.str();
}

std::string SQLQueries::insertPlace(PlaceObj place)
{
    std::stringstream stream;

    std::string placeName = correctQueryApostrof(place.getPlaceName().toStdString());
    std::string placeAddr = correctQueryApostrof(place.getAddress().toStdString());

    stream << "INSERT INTO " << PLACES_TABLE_NAME
           << "("
           << PLACES_NAME_FIELD  << ", "
           << PLACES_ADDR_FIELD  << ", "
           << PLACES_LAT_FIELD   << ", "
           << PLACES_LNG_FIELD   << ", "
           << PLACES_TYPE_FIELD  << ", "
           << PLACES_SCHEDULE_FIELD
           <<") VALUES ("
           << "\'" << placeName << "\', "
           << "\'" << placeAddr << "\', "
           << place.getLat() << ", "
           << place.getLong() << ", "
           << place.getPlaceType() << ", ";

    stream << "\'";
    for(int i = 0; i < AMOUNT_OF_DAYS; i++)
    {
        stream << place.getPlaceHours()[i].getDaySchedule() << "\n";
    }
    stream << "\');";
    return stream.str();
}

std::string SQLQueries::eraseAPlace(const PlaceObj& pl)
{
    std::stringstream stream;
    stream << "DELETE FROM " << PLACES_TABLE_NAME
           << " WHERE "
           << PLACES_ID_FIELD << "=" << pl.getPlaceId() << ";";
    return stream.str();
}

std::string SQLQueries::insertUser(User user)
{
    std::stringstream stream;

    std::string username = correctQueryApostrof(user.getUserName().toStdString());
    std::string password = correctQueryApostrof(user.getPassword().toStdString());
    std::string email = correctQueryApostrof(user.getEmail().toStdString());

    stream << "INSERT INTO " << USERS_TABLE_NAME
           << "("
           << USERS_USERNAME_FIELD  << ", "
           << USERS_PASSWORD_FIELD   << ", "
           << USERS_EMAIL_FIELD  << ", "
           << USERS_BIRTH_FIELD
           <<") VALUES ("
           << "\'" << username << "\', "
           << "\'" << password << "\', "
           << "\'" << email << "\', "
           << "\'" << user.getBirthDay().toString("dd.MM.yyyy").toStdString() << "\');";

    return stream.str();
}

std::string SQLQueries::updateAPlace(PlaceObj place)
{
    std::stringstream stream;
    std::string placeName = correctQueryApostrof(place.getPlaceName().toStdString());
    std::string placeAddr = correctQueryApostrof(place.getAddress().toStdString());
    stream << "UPDATE " << PLACES_TABLE_NAME << " set "
           << PLACES_NAME_FIELD     << " = " << "\'" << placeName << "\', "
           << PLACES_ADDR_FIELD     << " = " << "\'" << placeAddr << "\', "
           << PLACES_LAT_FIELD      << " = " << place.getLat() << ", "
           << PLACES_LNG_FIELD      << " = " << place.getLong() << ", "
           << PLACES_TYPE_FIELD     << " = " << place.getPlaceType() << ", "
           << PLACES_SCHEDULE_FIELD << " = ";
    stream << "\'";
    for(int i = 0; i < AMOUNT_OF_DAYS; i++)
    {
        stream << place.getPlaceHours()[i].getDaySchedule() << "\n";
    }
    stream << "\' ";
    stream << "WHERE " << PLACES_ID_FIELD << "=" << place.getPlaceId() << ";";
    return stream.str();
}

std::string SQLQueries::updateUserPassword(User user)
{
    std::stringstream stream;

    std::string username = correctQueryApostrof(user.getUserName().toStdString());
    std::string password = correctQueryApostrof(user.getPassword().toStdString());

    stream << "UPDATE " << USERS_TABLE_NAME << " set "
           << USERS_PASSWORD_FIELD   << "=" << "\'" << password << "\' "
           << "WHERE " << USERS_USERNAME_FIELD << " = " << "\'" << username << "\';";
    return stream.str();
}

std::string SQLQueries::filterPlacesByType(TypeOfPlace type)
{
    std::stringstream stream;
    stream <<  "SELECT * FROM "
           << PLACES_TABLE_NAME
           << " WHERE " << PLACES_TYPE_FIELD << " = " << type
           << " ORDER BY " << PLACES_NAME_FIELD << " ASC;";
    return stream.str();
}

std::string SQLQueries::filterPlacesByName(std::string name)
{
    std::stringstream stream;
    std::string placeName = correctQueryApostrof(name);
    stream <<  "SELECT * FROM "
           << PLACES_TABLE_NAME
           << " WHERE " << PLACES_NAME_FIELD << " LIKE " << "\'%" << placeName << "%\'"
           << " COLLATE NOCASE "
           << " ORDER BY " << PLACES_NAME_FIELD << " ASC;";
    return stream.str();
}

std::string SQLQueries::filterPlacesByAddress(std::string addr)
{
    std::stringstream stream;
    std::string placeAddr = correctQueryApostrof(addr);

    stream <<  "SELECT * FROM "
           << PLACES_TABLE_NAME
           << " WHERE " << PLACES_ADDR_FIELD << " LIKE " << "\'%" << placeAddr << "%\'"
           << " COLLATE NOCASE "
           << " ORDER BY " << PLACES_NAME_FIELD << " ASC;";
    return stream.str();
}

std::string SQLQueries::getAllPlaces()
{
    return getAllElements(PLACES_NAME_FIELD, PLACES_TABLE_NAME);
}

std::string SQLQueries::filterPlacesByTypeAndName(TypeOfPlace type, std::string name)
{
    std::stringstream stream;

    std::string placeName = correctQueryApostrof(name);

    stream << "SELECT * FROM "
           << PLACES_TABLE_NAME
           << " WHERE " << PLACES_TYPE_FIELD << "=\'" << type
           << "\' and " << PLACES_NAME_FIELD << " LIKE " << "\'%" << placeName << "%\';";
    return stream.str();
}

std::string SQLQueries::getAllUsers()
{
    return getAllElements(USERS_ID_FIELD, USERS_TABLE_NAME);
}

std::string SQLQueries::getUserInfo(std::string username, std::string password)
{
    std::stringstream stream;

    std::string usr = correctQueryApostrof(username);
    std::string psswd = correctQueryApostrof(password);

    stream << "SELECT * FROM "
           << USERS_TABLE_NAME
           << " WHERE " << USERS_USERNAME_FIELD << "=\'" << usr
           << "\' and " << USERS_PASSWORD_FIELD << "=\'" << psswd << "\';";
    return stream.str();
}

std::string SQLQueries::getUserInfoBasedUserName(std::string username)
{
    std::stringstream stream;

    std::string usr = correctQueryApostrof(username);

    stream << "SELECT * FROM "
           << USERS_TABLE_NAME
           << " WHERE " << USERS_USERNAME_FIELD << "=\'" << usr << "\';";
    return stream.str();
}

std::string SQLQueries::erasePlacesTable()
{
    return eraseTable(PLACES_TABLE_NAME);
}

std::string SQLQueries::eraseUsersTable()
{
    return eraseTable(USERS_TABLE_NAME);
}

std::string SQLQueries::getAmountOfUsers()
{
    return getAmountOfElements(USERS_USERNAME_FIELD, USERS_TABLE_NAME);
}

std::string SQLQueries::getAmountOfPlaces()
{
    return getAmountOfElements(PLACES_ID_FIELD, PLACES_TABLE_NAME);
}

std::string SQLQueries::getAmountOfComments()
{
    return getAmountOfElements(COMMENT_ID_FIELD, COMMENTS_TABLE_NAME);
}

std::string SQLQueries::createTableOfComments()
{
    std::stringstream stream;
    stream << "CREATE TABLE IF NOT EXISTS " << COMMENTS_TABLE_NAME << "("
           << COMMENT_ID_FIELD              << " INTEGER PRIMARY KEY NOT NULL, "
           << COMMENT_PLACENAME_FIELD       << " TEXT                NOT NULL, "
           << COMMENT_USERNAME_FIELD        << " TEXT                NOT NULL, "
           << COMMENT_REPUTATION_FIELD      << " REAL                NOT NULL, "
           << COMMENT_DATE_FIELD            << " TEXT                NOT NULL, "
           << COMMENT_TEXT_FIELD            << " TEXT                NOT NULL);";

    return stream.str();
}

std::string SQLQueries::insertComment(CommentObj comment)
{
    std::stringstream stream;

    std::string placeName = correctQueryApostrof(comment.getPlaceName());
    std::string userName = correctQueryApostrof(comment.getUsername());
    std::string commentText = correctQueryApostrof(comment.getComment());

    stream << "INSERT INTO " << COMMENTS_TABLE_NAME
           << "("
           << COMMENT_PLACENAME_FIELD    << ", "
           << COMMENT_USERNAME_FIELD    << ", "
           << COMMENT_REPUTATION_FIELD  << ", "
           << COMMENT_DATE_FIELD  << ", "
           << COMMENT_TEXT_FIELD
           <<") VALUES ("
           << "\'" << placeName << "\', "
           << "\'" << userName << "\', "
           << comment.getReputation() << ", "
           << "\'" << comment.getCommentDate().toString("dd.MM.yyyy").toStdString() << "\', "
           << "\'" << commentText << "\');";
    return stream.str();
}

std::string SQLQueries::getGlobalPlaceReputation(std::string placeName)
{
    std::stringstream stream;

    std::string plName = correctQueryApostrof(placeName);

    stream << "SELECT avg(" << COMMENT_REPUTATION_FIELD << ")"
           << " FROM " << COMMENTS_TABLE_NAME
           << " WHERE " << COMMENT_PLACENAME_FIELD << "=\'" << plName << "\';";
    return stream.str();
}

std::string SQLQueries::getPlaceComments(std::string placeName)
{
    std::stringstream stream;

    std::string plName = correctQueryApostrof(placeName);

    stream << "SELECT * FROM "
           << COMMENTS_TABLE_NAME
           << " WHERE " << COMMENT_PLACENAME_FIELD << "=\'" << plName << "\';";
    return stream.str();
}

CommentObj SQLQueries::convertQueryResultToComment(int amount_of_cols, char** colName, char** argv)
{
    std::string comment = "";
    std::string place = "";
    std::string username = "";
    float reputation = -1;
    QDate commentDay;

    for(int i = 0; i < amount_of_cols; i++)
    {
        std::string field =  QString(colName[i]).toUpper().toStdString();
        if(!field.compare(COMMENT_PLACENAME_FIELD))
        {
            place = argv[i];
        }
        else if(!field.compare(COMMENT_USERNAME_FIELD))
        {
            username = argv[i];
        }
        else if(!field.compare(COMMENT_REPUTATION_FIELD))
        {
            /// Inspired in the implementation given at https://stackoverflow.com/a/10300665
            QString s(argv[i]);
            bool ok = false;
            reputation = s.toDouble(&ok);
            if(!ok)
            {
                std::cout << "Error converting " << argv[i] << " into double!!!" << std::endl;
                assert(0);
            }
        }
        else if(!field.compare(COMMENT_DATE_FIELD))
        {
            commentDay = QDate::fromString(argv[i], "dd.MM.yyyy");
        }
        else if(!field.compare(COMMENT_TEXT_FIELD))
        {
            comment = argv[i];
        }
    }
    CommentObj retrievedComment(
        comment,
        place,
        username,
        reputation,
        commentDay);

    return retrievedComment;
}

PlaceObj SQLQueries::convertQueryResultToPlace(int amount_of_cols, char** colName, char** argv)
{
    int id = -1;
    std::string name = "";
    std::string addr = "";
    double lat = 0;
    double lng = 0;
    TypeOfPlace type = UNKNOWN_TYPE;
    std::string sched = "";
    for(int i = 0; i < amount_of_cols; i++)
    {
        std::string field =  QString(colName[i]).toUpper().toStdString();
        if(!field.compare(PLACES_ID_FIELD))
        {
            id = atoi(argv[i]);
        }
        else if(!field.compare(PLACES_NAME_FIELD))
        {
            name = argv[i];
        }
        else if(!field.compare(PLACES_ADDR_FIELD))
        {
            addr = argv[i];
        }
        else if(!field.compare(PLACES_LAT_FIELD))
        {
            std::string tmp = argv[i];
            lat = std::stod (tmp, nullptr);
        }
        else if(!field.compare(PLACES_LNG_FIELD))
        {
            std::string tmp = argv[i];
            lng = std::stod (tmp, nullptr);
        }
        else if(!field.compare(PLACES_TYPE_FIELD))
        {
            type = static_cast<TypeOfPlace>(atoi(argv[i]));
        }
        else if(!field.compare(PLACES_SCHEDULE_FIELD))
        {
            sched = argv[i];
        }
    }
    PlaceObj retrievedPlace(id,
        QString::fromStdString(addr),
        QString::fromStdString(name),
        lat,
        lng,
        type);

    std::stringstream stream(sched);
    std::string tmp;
    while(std::getline(stream, tmp))
    {
        DaySchedule daySched(tmp);
        retrievedPlace.addDayToSchedule(daySched);
    }
    return retrievedPlace;
}

User SQLQueries::convertQueryResultToUser(int amount_of_cols, char** colName, char** argv)
{
    std::string username = "";
    std::string email = "";
    QDate myDate;
    std::string password = "";
    for(int i = 0; i < amount_of_cols; i++)
    {
        std::string field =  QString(colName[i]).toUpper().toStdString();
        if(!field.compare(USERS_USERNAME_FIELD))
        {
            username = argv[i];
        }
        else if(!field.compare(USERS_PASSWORD_FIELD))
        {
            password = argv[i];
        }
        else if(!field.compare(USERS_EMAIL_FIELD))
        {
            email = argv[i];
        }
        else if(!field.compare(USERS_BIRTH_FIELD))
        {
             myDate = QDate::fromString(argv[i], "dd.MM.yyyy");
        }
        else
        {
            std::cout << "User table column " << field << " not handled. Value: " << argv[i] << std::endl;
        }
    }

    User retrievedUser(
                QString::fromStdString(username),
                QString::fromStdString(email),
                myDate,
                QString::fromStdString(password));

    return retrievedUser;
}