/// Custom includes
#include "DBEngine/DefaultComments.hpp"

static CommentObj residenceDesAcacias_1()
{
    std::string comment = std::string("This is a very cheap place where you can find a room where to ") +
        "stay if you are student. I have a really good time with friends. " +
        "The only drawback is that the kitchen might be small and there is " +
        "no oven nor washing machine";
    std::string placeName = "Residence Les Acacias";
    std::string user = "admin";
    float reputation = 3.5;
    QDate when(2019, 11, 28);
    return CommentObj(comment, placeName, user, reputation, when);
}

static CommentObj residenceDesAcacias_2()
{
    std::string comment = std::string("The place is dirty, the things do not work, and when ") +
        "you tell them to do something they dont fix it. The internet also is really bad";
    std::string placeName = "Residence Les Acacias";
    std::string user = "admin";
    float reputation = 1.0;
    QDate when(2019, 11, 28);
    return CommentObj(comment, placeName, user, reputation, when);
}

static CommentObj hotelLaPetiteVerrerie()
{
    std::string comment = std::string("Nice hotel to spend a nice weekend in Le Creusot. ") +
        "The place is cozy, it has a little piano and the breakfast is really good. " +
        "It is a little pricy.";
    std::string placeName = "La Petite Verrerie";
    std::string user = "admin";
    float reputation = 4.0;
    QDate when(2019, 11, 28);
    return CommentObj(comment, placeName, user, reputation, when);
}

static CommentObj residenceJeanMonnet()
{
    std::string comment = std::string("This is a nice place where you can find a room where to ") +
        "stay if you are student. I have a really good time with friends there. " +
        "It is more expensive than other residences but the rooms are more spacious.";
    std::string placeName = "Residence Jean Monnet";
    std::string user = "admin";
    float reputation = 3.5;
    QDate when(2019, 11, 28);
    return CommentObj(comment, placeName, user, reputation, when);
}

static CommentObj restaurantLeFutMets()
{
    std::string comment = std::string("This is the best restaurant in Le Creusot, served by ") +
        "the beloved Yohan, who is also the chef.";
    std::string placeName = "Le Fût-Mets";
    std::string user = "admin";
    float reputation = 5.0;
    QDate when(2019, 11, 28);
    return CommentObj(comment, placeName, user, reputation, when);
}

void DefaultComments::loadComments()
{
    /// We clear the comments before starting to avoid dupplications
    _comments.clear();
    _comments.push_back(residenceDesAcacias_1());
    _comments.push_back(residenceDesAcacias_2());
    _comments.push_back(hotelLaPetiteVerrerie());
    _comments.push_back(residenceJeanMonnet());
    _comments.push_back(restaurantLeFutMets());
}
