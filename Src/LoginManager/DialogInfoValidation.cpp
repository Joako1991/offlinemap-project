/// Qt includes
#include <QDate>
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>

/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include "LoginManager/DialogInfoValidation.hpp"
#include <User.hpp>

DialogInfoValidation::DialogInfoValidation(LoginStates back_state,
                                           LoginStates ok_state,
                                           QWidget *parent) :
    QDialog(parent),
    _editUsername(nullptr),
    _editEmail(nullptr),
    _editbirthday(nullptr),
    _buttons(nullptr),
    _back_state(back_state),
    _ok_state(ok_state)
{
    setUpGUI();
    setWindowTitle(tr("User Validation"));
    setModal(true);
}

DialogInfoValidation::~DialogInfoValidation()
{
    delete _editUsername;
    delete _editEmail;
    delete _editbirthday;
    delete _buttons;
}

void DialogInfoValidation::setUpGUI()
{
    // set up the layout
    QGridLayout *formGridLayout = new QGridLayout(this);

    // initialize the username
    _editUsername = new QLineEdit(this);
    QLabel *labelUsername = new QLabel(this);
    labelUsername->setText(tr("Username"));
    labelUsername->setBuddy(_editUsername);

    // initialize the Email
    _editEmail = new QLineEdit(this);
    QLabel *labelEmail = new QLabel(this);
    labelEmail->setText(tr("Email"));
    labelEmail->setBuddy(_editEmail);

    // initialize the Birthday
    _editbirthday = new QDateEdit(this);
    QLabel *labelbirthday = new QLabel(this);
    labelbirthday->setText(tr("Birthday"));
    labelbirthday->setBuddy(_editbirthday);

    // initialize buttons
    _buttons = new QDialogButtonBox(this);
    _buttons->addButton(QDialogButtonBox::Ok);
    _buttons->addButton(QDialogButtonBox::Retry);
    _buttons->button(QDialogButtonBox::Ok)->setText(tr("Submit"));
    _buttons->button(QDialogButtonBox::Retry)->setText(tr("Back"));

    // connects slots
    connect(_buttons->button(QDialogButtonBox::Retry),
            &QPushButton::clicked,
            this,
            &DialogInfoValidation::onClickBackButton);

    connect(_buttons->button(QDialogButtonBox::Ok),
            &QPushButton::clicked,
            this,
            &DialogInfoValidation::onClickSubmitButton);

    // place components into the dialog
    formGridLayout->addWidget(labelUsername, 0, 0);
    formGridLayout->addWidget(_editUsername, 0, 1);
    formGridLayout->addWidget(labelEmail, 1, 0);
    formGridLayout->addWidget(_editEmail, 1, 1);
    formGridLayout->addWidget(labelbirthday, 2, 0);
    formGridLayout->addWidget(_editbirthday, 2, 1);
    formGridLayout->addWidget(_buttons, 3, 0, 1, 2);

    setLayout(formGridLayout);
}

LoginStates DialogInfoValidation::showWindow(User &output)
{
    /// Inspired in the solution given here: https://stackoverflow.com/a/11507742
    show();

    /// By default, if we destroy the QDialog, then we have to exit
    _ret_state = _back_state;
    _entered_user.clear();

    // This loop will wait for the window is destroyed
    connect(this,
        &QDialog::destroyed,
        &_loop,
        &QEventLoop::quit);

    /// We wait until the loop exits
    _loop.exec();

    hide();
    output = _entered_user;
    return _ret_state;
}

void DialogInfoValidation::onClickBackButton()
{
    _ret_state = _back_state;
    _loop.quit();
}

void DialogInfoValidation::reject()
{
    onClickBackButton();
}

void DialogInfoValidation::onClickSubmitButton()
{
    QString username = _editUsername->text();
    QString email = _editEmail->text();
    QDate birthday = _editbirthday->date();
    if (!username.isEmpty() && !email.isEmpty() && birthday.isValid() && !birthday.isNull())
    {
        std::vector<User> user = DBEngine::instance()->getUserInfoValuesBasedUserName(username.toStdString());
        User checkUser(username, email, birthday, "");
        if (user.size() == 1)
        {
            if (checkUser == user[0])
            {
                _ret_state = _ok_state;
                _entered_user = user[0];
                _loop.quit();
            }
            else
            {
                QMessageBox::warning(this, "Valid", "Invalid. Cannot find an user with that information");
            }
        }
        else
        {
            QMessageBox::warning(this, "Valid", "Invalid. This username does not exists");
        }
    }
    else
    {
        if (username.isEmpty())
        {
            QMessageBox::warning(this, "Valid", "Username field is empty");
        }
        else if(email.isEmpty())
        {
            QMessageBox::warning(this, "Valid", "Email field is empty");
        }
        else if(!birthday.isValid() || birthday.isNull())
        {
            QMessageBox::warning(this, "Valid", "Birthday is not valid");
        }
    }
}
