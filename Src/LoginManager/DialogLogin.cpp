/// Qt includes
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>

/// STL includes
#include <iostream>
#include <vector>

/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include "LoginManager/DialogLogin.hpp"

DialogLogin::DialogLogin(LoginStates back_state,
                         LoginStates login_success,
                         LoginStates forget_pass,
                         QWidget *parent) :
    QDialog(parent),
    _editUsername(nullptr),
    _editPassword(nullptr),
    _buttons(nullptr),
    _back_state(back_state),
    _login_success(login_success),
    _forget_pass(forget_pass)
{
    setUpGUI();
    setWindowTitle(tr("User Login"));
    setModal(true);
}

DialogLogin::~DialogLogin()
{
    delete _editUsername;
    delete _editPassword;
    delete _buttons;
}

void DialogLogin::setUpGUI()
{
    QGridLayout *formGridLayout = new QGridLayout(this);

    // Initialize the username field
    _editUsername = new QLineEdit(this);
    QLabel *labelUsername = new QLabel(this);
    labelUsername->setText(tr("Username"));
    labelUsername->setBuddy(_editUsername);

    // Initialize the password field
    _editPassword = new QLineEdit(this);
    _editPassword->setEchoMode(QLineEdit::Password);
    QLabel *labelPassword = new QLabel(this);
    labelPassword->setText(tr("Password"));
    labelPassword->setBuddy(_editPassword);

    // Initialize buttons
    _buttons = new QDialogButtonBox(this);
    _buttons->addButton(QDialogButtonBox::Ok);
    _buttons->addButton(QDialogButtonBox::Retry);
    _buttons->addButton(QDialogButtonBox::Reset);

    _buttons->button(QDialogButtonBox::Ok)->setText(tr("Login"));
    _buttons->button(QDialogButtonBox::Retry)->setText(tr("Back"));
    _buttons->button(QDialogButtonBox::Reset)->setText(tr("Forget Password"));

    // connects slots
    connect(_buttons->button(QDialogButtonBox::Retry),
            &QPushButton::clicked,
            this,
            &DialogLogin::onClickedBack);

    connect(_buttons->button(QDialogButtonBox::Reset),
            &QPushButton::clicked,
            this,
            &DialogLogin::onClickedForgetPassword);

    connect(_buttons->button(QDialogButtonBox::Ok),
            &QPushButton::clicked,
            this,
            &DialogLogin::onClickedLogin);

    // place components into the dialog
    formGridLayout->addWidget(labelUsername, 0, 0);
    formGridLayout->addWidget(_editUsername, 0, 1);
    formGridLayout->addWidget(labelPassword, 1, 0);
    formGridLayout->addWidget(_editPassword, 1, 1);
    formGridLayout->addWidget(_buttons, 2, 0, 1, 3);

    setLayout(formGridLayout);
}

LoginStates DialogLogin::showWindow(User &output)
{
    /// Inspired in the solution given here: https://stackoverflow.com/a/11507742
    show();

    /// By default, if we destroy the QDialog, then we have to exit
    _ret_state = _back_state;

    // This loop will wait for the window is destroyed
    connect(this,
        &QDialog::destroyed,
        &_loop,
        &QEventLoop::quit);

    /// We wait until the loop exits
    _loop.exec();

    hide();
    output = _retrievedUser;
    return _ret_state;
}

void DialogLogin::onClickedLogin()
{
    QString username = _editUsername->text();
    QString password = _editPassword->text();
    if (!username.isEmpty() && !password.isEmpty())
    {
        std::vector<User> user = DBEngine::instance()->getUserInfoValues(username.toStdString(), password.toStdString());
        /// Sanity check
        if (user.size() < 2)
        {
            if (user.size() == 1)
            {
                QMessageBox::information(this, "login", "Login succeeded");
                _retrievedUser = user[0];
                _ret_state = _login_success;
                _loop.quit();
            }
            else
            {
                QMessageBox::information(this, "login", "ERROR: Username / Password incorrect");
            }
        }
        else
        {
            std::cout << "CRITICAL! You have two entries in the database with the same username!" << std::endl;
            assert(0);
        }
    }
    else
    {
        if (username.isEmpty())
        {
            QMessageBox::warning(this, "login", "Invalid: Username field is empty");
        }
        else
        {
            QMessageBox::warning(this, "login", "Invalid: Password field is empty");
        }
    }
}

void DialogLogin::onClickedBack()
{
    _ret_state = _back_state;
    _loop.quit();
}

void DialogLogin::onClickedForgetPassword()
{
    _ret_state = _forget_pass;
    _loop.quit();
}

void DialogLogin::reject()
{
    onClickedBack();
}
