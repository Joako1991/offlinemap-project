/// Qt includes
#include <QGridLayout>
#include <QPushButton>

/// Custom includes
#include "LoginManager/DialogStartLogin.hpp"

DialogStartLogin::DialogStartLogin(LoginStates exit_state, LoginStates login_state, LoginStates signup_state, QWidget *parent) :
    QDialog(parent),
    _buttons(nullptr),
    _exit_state(exit_state),
    _login_state(login_state),
    _signup_state(signup_state)
{
    setUpGUI();
    setWindowTitle(tr("Welcome"));
    setModal(true);
}

DialogStartLogin::~DialogStartLogin()
{
    delete _buttons;
}

void DialogStartLogin::setUpGUI()
{
    // set up the layout
    QGridLayout *formGridLayout = new QGridLayout(this);

    // initialize buttons
    _buttons = new QDialogButtonBox(this);
    _buttons->addButton(QDialogButtonBox::Cancel);
    _buttons->addButton(QDialogButtonBox::Ok);
    _buttons->addButton(QDialogButtonBox::Yes);

    _buttons->button(QDialogButtonBox::Ok)->setText(tr("Login"));
    _buttons->button(QDialogButtonBox::Yes)->setText(tr("Sign up"));
    _buttons->button(QDialogButtonBox::Cancel)->setText(tr("Exit"));

    // connects slots
    connect(_buttons->button(QDialogButtonBox::Ok),
            &QPushButton::clicked,
            this,
            &DialogStartLogin::onLoginClicked);

    connect(_buttons->button(QDialogButtonBox::Yes),
            &QPushButton::clicked,
            this,
            &DialogStartLogin::onSignUpClicked);

    connect(_buttons->button(QDialogButtonBox::Cancel),
            &QPushButton::clicked,
            this,
            &DialogStartLogin::onExitClicked);

    // place components into the dialog
    formGridLayout->addWidget(_buttons, 0, 0, 1, 3);

    setLayout(formGridLayout);
}

LoginStates DialogStartLogin::showWindow()
{
    /// Inspired in the solution given here: https://stackoverflow.com/a/11507742
    show();

    /// By default, if we destroy the QDialog, then we have to exit
    _ret_state = _exit_state;

    // This loop will wait for the window is destroyed
    connect(this,
        &QDialog::destroyed,
        &_loop,
        &QEventLoop::quit);

    /// We wait until the loop exits
    _loop.exec();

    hide();
    return _ret_state;
}

void DialogStartLogin::onExitClicked()
{
    _ret_state = _exit_state;
    _loop.quit();
}

void DialogStartLogin::onLoginClicked()
{
    _ret_state = _login_state;
    _loop.quit();
}

void DialogStartLogin::onSignUpClicked()
{
    _ret_state = _signup_state;
    _loop.quit();
}

void DialogStartLogin::reject()
{
    onExitClicked();
}
