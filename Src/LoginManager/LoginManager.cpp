/// Custom includes
#include <User.hpp>
#include "LoginManager/DialogInfoValidation.hpp"
#include "LoginManager/LoginManager.hpp"
#include "LoginManager/DialogLogin.hpp"
#include "LoginManager/DialogResetPass.hpp"
#include "LoginManager/DialogSignUp.hpp"
#include "LoginManager/DialogStartLogin.hpp"

bool LoginManager::startLoginManager(User &user)
{
    user.clear();
    _state = MAIN_WINDOW;
    bool login_success = false;
    bool running = true;

    /// Login Windows instantiation
    DialogStartLogin startWin(EXIT, LOGIN_WINDOW, SIGNUP_WINDOW);
    DialogLogin lngWindow(MAIN_WINDOW, LOGGING_SUCCESS, FORGET_PASSWORD_WINDOW);
    DialogInfoValidation validation(LOGIN_WINDOW, RESET_PASSWORD);
    DialogResetPass resetWin(FORGET_PASSWORD_WINDOW, LOGIN_WINDOW);
    DialogSignUp signUp(MAIN_WINDOW, LOGIN_WINDOW);

    while (running)
    {
        switch (_state)
        {
        case MAIN_WINDOW:
            _state = startWin.showWindow();
            break;
        case LOGIN_WINDOW:
            user.clear();
            _state = lngWindow.showWindow(user);
            break;
        case FORGET_PASSWORD_WINDOW:
            user.clear();
            _state = validation.showWindow(user);
            break;
        case RESET_PASSWORD:
            _state = resetWin.showWindow(user);
            user.clear();
            break;
        case SIGNUP_WINDOW:
            _state = signUp.showWindow();
            break;
        case LOGGING_SUCCESS:
            running = false;
            login_success = true;
            break;
        case EXIT:
            login_success = false;
            running = false;
            /// Just in case, we clear all the spurious information
            /// in the user object
            user.clear();
            break;
        default:
            running = false;
            break;
        }
    }
    return login_success;
}