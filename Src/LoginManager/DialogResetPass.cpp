/// Qt includes
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>

/// STL includes
#include <iostream>

/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include "LoginManager/DialogResetPass.hpp"

DialogResetPass::DialogResetPass(LoginStates back_state, LoginStates submit_state, QWidget *parent) :
    QDialog(parent),
    _editNewPassword(nullptr),
    _editConfNewPassword(nullptr),
    _buttons(nullptr),
    _back_state(back_state),
    _submit_state(submit_state)
{
    setUpGUI();
    setWindowTitle(tr("Reset Password"));
    setModal(true);
}

DialogResetPass::~DialogResetPass()
{
    delete _editNewPassword;
    delete _editConfNewPassword;
    delete _buttons;
}

void DialogResetPass::setUpGUI()
{
    // set up the layout
    QGridLayout *formGridLayout = new QGridLayout(this);

    /// New password line
    _editNewPassword = new QLineEdit(this);
    _editNewPassword->setEchoMode(QLineEdit::Password);
    QLabel *labelNewPassword = new QLabel(this);
    labelNewPassword->setText(tr("New Password"));
    labelNewPassword->setBuddy(_editNewPassword);

    /// Confirm password line
    _editConfNewPassword = new QLineEdit(this);
    _editConfNewPassword->setEchoMode(QLineEdit::Password);
    QLabel *labelConfNewPassword = new QLabel(this);
    labelConfNewPassword->setText(tr("confirm New Password"));
    labelConfNewPassword->setBuddy(_editConfNewPassword);

    // initialize buttons
    _buttons = new QDialogButtonBox(this);

    _buttons->addButton(QDialogButtonBox::Ok);
    _buttons->button(QDialogButtonBox::Ok)->setText(tr("Submit"));

    _buttons->addButton(QDialogButtonBox::Retry);
    _buttons->button(QDialogButtonBox::Retry)->setText(tr("Back"));

    // connects slots
    connect(_buttons->button(QDialogButtonBox::Retry),
            &QPushButton::clicked,
            this,
            &DialogResetPass::onClickBackButton);

    connect(_buttons->button(QDialogButtonBox::Ok),
            &QPushButton::clicked,
            this,
            &DialogResetPass::onClickSubmitButton);

    // place components into the dialog
    formGridLayout->addWidget(labelNewPassword, 1, 0);
    formGridLayout->addWidget(_editNewPassword, 1, 1);
    formGridLayout->addWidget(labelConfNewPassword, 2, 0);
    formGridLayout->addWidget(_editConfNewPassword, 2, 1);
    formGridLayout->addWidget(_buttons, 3, 0, 1, 2);

    setLayout(formGridLayout);
}

LoginStates DialogResetPass::showWindow(const User user_to_update)
{
    /// Inspired in the solution given here: https://stackoverflow.com/a/11507742
    show();

    /// By default, if we destroy the QDialog, then we have to exit
    _ret_state = _back_state;
    _user_data = user_to_update;

    // This loop will wait for the window is destroyed
    connect(this,
        &QDialog::destroyed,
        &_loop,
        &QEventLoop::quit);

    /// We wait until the loop exits
    _loop.exec();

    hide();
    return _ret_state;
}

void DialogResetPass::onClickBackButton()
{
    _ret_state = _back_state;
    _loop.quit();
}

void DialogResetPass::onClickSubmitButton()
{
    if (!_user_data.isValid())
    {
        std::cout << "ERROR! When you are resetting the password, your saved user is not valid" << std::endl;
        assert(0);
    }
    QString password = _editNewPassword->text();
    QString confpassword = _editConfNewPassword->text();
    if (!password.isEmpty() && password == confpassword)
    {
        _user_data.setPassword(password);
        DBEngine::instance()->updateUserPassword(_user_data);
        QMessageBox::information(this, "Reset", "The password is resetted");
        _ret_state = _submit_state;
        _loop.quit();
    }
    else if(password.isEmpty())
    {
        QMessageBox::warning(this, "Reset", "Invalid. The password cannot be empty");
    }
    else
    {
        QMessageBox::warning(this, "Reset", "Invalid. The two provided passwords do not match");
    }
}
