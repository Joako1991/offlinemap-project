/// Qt includes
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>

/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include "LoginManager/DialogSignUp.hpp"

DialogSignUp::DialogSignUp(LoginStates back_state, LoginStates submit_state, QWidget *parent) :
    QDialog(parent),
    _editUsername(nullptr),
    _editEmail(nullptr),
    _editBirthday(nullptr),
    _editPassword(nullptr),
    _editConfPassword(nullptr),
    _buttons(nullptr),
    _back_state(back_state),
    _submit_state(submit_state)
{
    setUpGUI();
    setWindowTitle(tr("User SignUP "));
    setModal(true);
}

DialogSignUp::~DialogSignUp()
{
    delete _editUsername;
    delete _editEmail;
    delete _editBirthday;
    delete _editPassword;
    delete _editConfPassword;
    delete _buttons;
}

void DialogSignUp::setUpGUI()
{
    // set up the layout
    QGridLayout *formGridLayout = new QGridLayout(this);

    // initialize the fields
    _editUsername = new QLineEdit(this);
    QLabel *labelUsername = new QLabel(this);
    labelUsername->setText(tr("Username"));
    labelUsername->setBuddy(_editUsername);

    _editEmail = new QLineEdit(this);
    QLabel *labelEmail = new QLabel(this);
    labelEmail->setText(tr("Email"));
    labelEmail->setBuddy(_editEmail);

    _editBirthday = new QDateEdit(this);
    QLabel *labelbirthday = new QLabel(this);
    labelbirthday->setText(tr("Birthday"));
    labelbirthday->setBuddy(_editBirthday);

    _editPassword = new QLineEdit(this);
    _editPassword->setEchoMode(QLineEdit::Password);
    QLabel *labelPassword = new QLabel(this);
    labelPassword->setText(tr("Password"));
    labelPassword->setBuddy(_editPassword);

    _editConfPassword = new QLineEdit(this);
    _editConfPassword->setEchoMode(QLineEdit::Password);
    QLabel *labelConfPassword = new QLabel(this);
    labelConfPassword->setText(tr("confirm Password"));
    labelConfPassword->setBuddy(_editConfPassword);

    // initialize buttons
    _buttons = new QDialogButtonBox(this);
    _buttons->addButton(QDialogButtonBox::Ok);
    _buttons->addButton(QDialogButtonBox::Retry);
    _buttons->button(QDialogButtonBox::Ok)->setText(tr("Sign up"));
    _buttons->button(QDialogButtonBox::Retry)->setText(tr("Back"));

    // connects slots
    connect(_buttons->button(QDialogButtonBox::Retry),
            &QPushButton::clicked,
            this,
            &DialogSignUp::onClickBackButton);

    connect(_buttons->button(QDialogButtonBox::Ok),
            &QPushButton::clicked,
            this,
            &DialogSignUp::onClickSignUpButton);

    // place components into the dialog
    formGridLayout->addWidget(labelUsername, 0, 0);
    formGridLayout->addWidget(_editUsername, 0, 1);
    formGridLayout->addWidget(labelEmail, 1, 0);
    formGridLayout->addWidget(_editEmail, 1, 1);
    formGridLayout->addWidget(labelbirthday, 2, 0);
    formGridLayout->addWidget(_editBirthday, 2, 1);
    formGridLayout->addWidget(labelPassword, 3, 0);
    formGridLayout->addWidget(_editPassword, 3, 1);
    formGridLayout->addWidget(labelConfPassword, 4, 0);
    formGridLayout->addWidget(_editConfPassword, 4, 1);
    formGridLayout->addWidget(_buttons, 5, 0, 1, 2);

    setLayout(formGridLayout);
}

LoginStates DialogSignUp::showWindow()
{
    /// Inspired in the solution given here: https://stackoverflow.com/a/11507742
    show();

    /// By default, if we destroy the QDialog, then we have to exit
    _ret_state = _back_state;
    // This loop will wait for the window is destroyed
    connect(this,
        &QDialog::destroyed,
        &_loop,
        &QEventLoop::quit);

    /// We wait until the loop exits
    _loop.exec();

    hide();
    return _ret_state;
}

void DialogSignUp::onClickBackButton()
{
    _ret_state = _back_state;
    _loop.quit();
}

void DialogSignUp::reject()
{
    onClickBackButton();
}

void DialogSignUp::onClickSignUpButton()
{
    QString username = _editUsername->text();
    QString email = _editEmail->text();
    QDate birthdate = _editBirthday->date();
    QDate currentDate = QDate::currentDate();
    QString password = _editPassword->text();
    QString Confpassword = _editConfPassword->text();

    if (!username.isEmpty() &&
        !password.isEmpty() && password == Confpassword &&
        !email.isEmpty() &&
        birthdate.isValid() && !birthdate.isNull() && birthdate < currentDate)
    {
        User newUser(username, email, birthdate, password);

        if (!DBEngine::instance()->insertUserInfo(newUser))
        {
            QMessageBox::information(this, "Sign Up", "User information already exists");
        }
        else
        {
            QMessageBox::information(this, "Sign Up", "User created successfully");
            _ret_state = _submit_state;
            _loop.quit();
        }
    }
    else
    {
        if (username.isEmpty())
        {
            QMessageBox::warning(this, "Sign Up", "Invalid. Username cannot be empty");
        }
        else if(password.isEmpty())
        {
            QMessageBox::warning(this, "Sign Up", "Invalid. Password cannot be empty");
        }
        else if(password != Confpassword)
        {
            QMessageBox::warning(this, "Sign Up", "Invalid. Passwords do not match");
        }
        else if(email.isEmpty())
        {
            QMessageBox::warning(this, "Sign Up", "Invalid. Email is empty");
        }
        else if(!birthdate.isValid() || birthdate.isNull() || birthdate >= currentDate)
        {
            QMessageBox::warning(this, "Sign Up", "Invalid. Birthday is not valid");
        }
    }
}
