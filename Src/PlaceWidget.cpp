/// Qt includes
#include <QFormLayout>

/// Custom includes
#include <CommonTypes.hpp>
#include <DBEngine/DBEngine.hpp>
#include "PlaceWidget.hpp"

PlaceWidget::PlaceWidget(PlaceObj plc, QWidget *parent) : QWidget(parent),
    _placeName(nullptr),
    _placeadd(nullptr),
    _placeType(nullptr),
    _placelat(nullptr),
    _placelon(nullptr),
    _placeReputation(nullptr)
{
    initPlaceWidget();
    setPlace(plc);
}

PlaceWidget::~PlaceWidget()
{
    delete _placeName;
    delete _placeadd;
    delete _placeType;
    delete _placelat;
    delete _placelon;
    delete _placeReputation;
}

void PlaceWidget::setPlace(PlaceObj place)
{
    _place = place;
    PlaceTypeMap placeTypes;
    TypeOfPlace type = place.getPlaceType();
    if (type == UNKNOWN_TYPE || type == ALL_TYPES)
    {
        type = UNKNOWN_TYPE;
    }
    _placeName->setText(("name : ") + place.getPlaceName());
    _placeType->setText(("type : ") + placeTypes.mapToEnum.key(place.getPlaceType(), "Unknown type"));  
    _placeadd->setText(("address : ") + place.getAddress());
    _placelat->setText(("Lat : ") + QString::number(place.getLat()));
    _placelon->setText(("Lon : ") + QString::number(place.getLong()));

    double reputation = -1;
    /// If we find the reputation of the place, then we set it. If not, we put a reputation
    /// of zero
    if(DBEngine::instance()->getPlaceAverageReputation(
        place.getPlaceName().toStdString(),
        reputation))
    {
        _placeReputation->setText(
            "Reputation: " +
            QString::number(reputation, 'f', 1) +
            " of 5.0"
            );
    }
    else
    {
        _placeReputation->setText("Reputation: 0 of 5.0");
    }
}

void PlaceWidget::initPlaceWidget()
{
    QGridLayout *formGridLayout = new QGridLayout(this);
    int rowCounter = 0;
    _placeName = new QLabel(tr(""), this);
    _placeReputation = new QLabel(tr(""), this);
    _placeType = new QLabel(tr(""), this);
    _placeadd = new QLabel(tr(""), this);
    _placelat = new QLabel(tr(""), this);
    _placelon = new QLabel(tr(""), this);
    formGridLayout->addWidget(_placeName, rowCounter++, 0);
    formGridLayout->addWidget(_placeReputation, rowCounter++, 0);
    formGridLayout->addWidget(_placeType, rowCounter++, 0);
    formGridLayout->addWidget(_placeadd, rowCounter++, 0);
    formGridLayout->addWidget(_placelat, rowCounter++, 0);
    formGridLayout->addWidget(_placelon, rowCounter++, 0);
    setLayout(formGridLayout);
}

void PlaceWidget::mousePressEvent(QMouseEvent *event)
{
    event->accept();
    emit widgetTapped(_place);
}
