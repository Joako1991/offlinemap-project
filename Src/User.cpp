#include <User.hpp>

void User::clear()
{
    _userName.clear();
    _email.clear();
    _birthday = QDate();
    _password.clear();
}
