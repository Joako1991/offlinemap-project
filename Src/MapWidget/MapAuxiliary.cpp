// STL includes
#include <cmath>
#include <stdint.h>
#include <iostream>
// Custom includes
#include "MapWidget/MapAuxiliary.hpp"

double MapAuxiliary::degreesToRadians(double degrees)
{
    return degrees * M_PI / 180.0;
}

double MapAuxiliary::getDistanceBetweenPointsInMeters(double lat1, double lon1, double lat2, double lon2)
{
    /// Inspired in the implementation given at:
    /// https://ourcodeworld.com/articles/read/1021/how-to-calculate-the-distance-between-2-markers-coordinates-in-google-maps-with-javascript
    // The radius of the planet earth in meters
    uint32_t earth_radious = 6378137;
    double dLat = degreesToRadians(lat2 - lat1);
    double dLong = degreesToRadians(lon2 - lon1);

    double arc = sin(dLat / 2)
            *
            sin(dLat / 2)
            +
            cos(degreesToRadians(lat1))
            *
            cos(degreesToRadians(lat1))
            *
            sin(dLong / 2.0)
            *
            sin(dLong / 2.0);

    std::cout << "Arc: " << arc << std::endl;

    double a = 2.0 * atan2(sqrt(arc), sqrt(1 - arc));
    return earth_radious * a;
}

double MapAuxiliary::getDistanceBetweenPointsInKm(double lat1, double lon1, double lat2, double lon2)
{
    return getDistanceBetweenPointsInMeters(lat1, lon1, lat2, lon2) * 0.001;
}