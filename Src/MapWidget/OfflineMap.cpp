/*
  OSMScout - a Qt backend for libosmscout and libosmscout-map
  Copyright (C) 2010  Tim Teulings

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Qt includes
#include <QDir>
#include <QFileInfo>
#include <QQmlContext> // We needed to handle the return type of rootContext()
#include <QQuickItem>
#include <QString>

// STL library
#include <iostream> // For std::cerr

// OSM Scout library
#include <osmscout/OSMScoutQt.h>
#include <osmscout/Settings.h>
#include <osmscout/util/Logger.h>

// Custom includes
#include <CommonTypes.hpp>
#include <MapWidget/AppSettings.h>
#include "MapWidget/OfflineMap.hpp"
#include <MapWidget/Theme.h>

// Logging levels
enum {
    ERROR = 1,
    WARNING,
    INFO,
    DEBUG,
};

using namespace osmscout;

OfflineMap::OfflineMap(QWidget *parent) :
    QWidget(parent),
    _initialized(false),
    _window(nullptr),
    _widget(nullptr),
    _layout(nullptr),
    _currentMarkId(-1),
    _currentRouteId(-1),
    _height(0),
    _width(0),
    _total_distance_km(0),
    _total_time_sec(0),
    _timeLengthLabel(nullptr)
{
    _width = this->size().width();
    _height = this->size().height();
    loadMapArguments();
    initializeMap();
}

OfflineMap::~OfflineMap()
{
    delete _window;
    delete _layout;
    delete _timeLengthLabel;
    OSMScoutQt::FreeInstance();
}

void OfflineMap::clearMapInformation()
{
    resetMarks();
    resetRoutes();
    MeansOfTransportMap transportMap;
    _choosenMean = transportMap.mapToEnum.value(ON_FOOT);
    if(_timeLengthLabel)
    {
        _timeLengthLabel->setText("");
    }
}

void OfflineMap::makeSignalsConnections()
{
    QObject* qmlMap = _window->rootObject()->findChild<QObject*>("MapWindow");
    connect(qmlMap,
        SIGNAL(mapClicked(double, double)),
        this,
        SLOT(onMapClicked(double, double)));

    connect(qmlMap,
        SIGNAL(routeComputationFinished(double, double)),
        this,
        SLOT(onRouteComputationFinished(double, double)));
}

bool OfflineMap::initOSMScoutEnginer()
{
    clearMapInformation();
    // register OSMScout library QML types
    OSMScoutQt::RegisterQmlTypes();

    // register application QML types
    qmlRegisterType<AppSettings>("net.sf.libosmscout.map", 1, 0, "AppSettings");
    qmlRegisterSingletonType<Theme>("net.sf.libosmscout.map", 1, 0, "Theme", OfflineMap::ThemeProvider);

    /// Init the logger
    int logEnv = LogEnv("DEBUG");
    osmscout::log.Debug(logEnv >= DEBUG);
    osmscout::log.Info(logEnv >= INFO);
    osmscout::log.Warn(logEnv >= WARNING);
    osmscout::log.Error(logEnv >= ERROR);

    OSMScoutQtBuilder builder = OSMScoutQt::NewInstance();

    QStringList mapLookupDirectories;
    mapLookupDirectories << _args.databaseDirectory;
    QDir dir(_args.databaseDirectory);

    QFileInfo stylesheetFile(_args.style);

    /// Here we initialize the library OSMScout with the provided arguments
    builder
        .WithStyleSheetDirectory(stylesheetFile.dir().path())
        .WithStyleSheetFile(stylesheetFile.fileName())
        .WithIconDirectory(_args.iconDirectory)
        .WithMapLookupDirectories(mapLookupDirectories);

    if (!builder.Init())
    {
        std::cerr << "Cannot initialize OSMScout library" << std::endl;
        _initialized = false;
        return false;
    }
    return true;
}

void OfflineMap::resetMarks()
{
    emit removeAllMarkIds();
    _marksIds.clear();
    _currentMarkId = -1;
    setMarkId(0);
}

bool OfflineMap::isAnOldId(int id, std::vector<int> seenIds)
{
    std::vector<int>::iterator it = std::find(seenIds.begin(), seenIds.end(), id);
    return (it != seenIds.end());
}

void OfflineMap::setMarkId(int newVal)
{
    if(newVal != _currentMarkId)
    {
        if(!isAnOldId(newVal, _marksIds))
        {
            _marksIds.push_back(newVal);
        }
        _currentMarkId = newVal;
        emit currentMarkIdChanged();
    }
}

void OfflineMap::setUpWidgetFormat()
{
    /// We convert the QML window into a widget, and we set up some properties
    _widget = QWidget::createWindowContainer(_window, this);

    /// We define a layout management for the widget
    _layout = new QVBoxLayout(this);
    _timeLengthLabel = new QLabel(tr(""), this);
    _timeLengthLabel->setAlignment(Qt::AlignCenter);
    _timeLengthLabel->setMaximumHeight(40);
    _layout->addWidget(_timeLengthLabel);
    _layout->addWidget(_widget);
    setLayout(_layout);
}

void OfflineMap::initializeMap()
{
    if(!initOSMScoutEnginer())
    {
        return;
    }

    /// Load the QML window
    _window = new QQuickView();
    _window->rootContext()->setContextProperty("offlineMap", this);
    _window->setSource(QUrl("qrc:/qml/main.qml"));

    setUpWidgetFormat();

    makeSignalsConnections();

    _initialized = true;
}

int OfflineMap::LogEnv(const QString &env)
{
    if (env.toUpper() == "DEBUG")
    {
        return DEBUG;
    }
    else if (env.toUpper() == "INFO")
    {
        return INFO;
    }
    else if (env.toUpper() == "WARNING")
    {
        return INFO;
    }
    else if (env.toUpper() == "ERROR")
    {
        return ERROR;
    }

    return WARNING;
}

QObject* OfflineMap::ThemeProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static Theme *theme = new Theme();

    return theme;
}

void OfflineMap::loadMapArguments()
{
    _args.help = false;
    /// PROJECT_PATH is a macro defined during compilation time
    QDir stylePath(QString(PROJECT_PATH) + "Externals/libosmscout/stylesheets/standard.oss");
    QDir databaseDir(QString(PROJECT_PATH) + "Maps/map_database");
    QDir icons(QString(PROJECT_PATH) + "Externals/libosmscout/libosmscout/data/icons/svg/standard");

    _args.style = stylePath.absoluteFilePath("");
    _args.databaseDirectory = databaseDir.absoluteFilePath("");
    _args.iconDirectory = icons.absoluteFilePath("");
}

void OfflineMap::onMapClicked(double lat, double lon)
{
    emit mapTapped(lat, lon);
}

void OfflineMap::markAPlace(const PlaceObj& pl)
{
    emit newPlace(pl.getLat(), pl.getLong(), true);
}

void OfflineMap::removeMarkedPlace(int id)
{
    emit removeId(id);
}

void OfflineMap::resetRoutes()
{
    emit removeAllRouteIds();
    _routeIds.clear();
    _vectorOfRoutes.clear();
    _currentRouteId = -1;
    setRouteId(0);
}

void OfflineMap::setRouteId(int nVal)
{
    if(nVal != _currentRouteId)
    {
        if(!isAnOldId(nVal, _routeIds))
        {
            _routeIds.push_back(nVal);
        }
        _currentRouteId = nVal;
        emit currentRouteIdChanged();
    }
}

void OfflineMap::onComputeRoute(std::vector<PlaceObj> &list, QString meanOfTransport)
{
    assert(list.size() > 1);
    resetRoutes();
    resetMarks();
    _choosenMean = meanOfTransport;

    PlaceObj startPlace = list[0];
    emit newPlace(startPlace.getLat(), startPlace.getLong(), false);

    for(unsigned i = 1; i < list.size(); i++)
    {
        PlaceObj endPlace = list[i];
        Route rte(
            startPlace.getLat(),
            startPlace.getLong(),
            endPlace.getLat(),
            endPlace.getLong());
        _vectorOfRoutes.push_back(rte);

        setMarkId(_currentMarkId+1);
        emit newPlace(endPlace.getLat(), endPlace.getLong(), false);

        startPlace = endPlace;
    }

    _total_distance_km = 0;
    _total_time_sec = 0;
    emit mapRecenter(list[0].getLat(), list[0].getLong());
    onRouteComputationFinished(0, 0);
}

void OfflineMap::onRouteComputationFinished(double time_sec, double length_km)
{
    _total_distance_km += length_km;
    _total_time_sec += time_sec;
    QTime time(0,0,0);
    time = time.addSecs(_total_time_sec);
    _timeLengthLabel->setText(
        QString("Length of the route: ") + QString::number(_total_distance_km, 'f', 2) + " km\n" +
        QString("Time required: ") + time.toString("HH:mm:ss"));

    if(_vectorOfRoutes.size()!=0)
    {
        setRouteId(_currentRouteId+1);
        emit computePath(_vectorOfRoutes[0].start(), _vectorOfRoutes[0].end(), _choosenMean);
        _vectorOfRoutes.erase(_vectorOfRoutes.begin());
    }
}

void OfflineMap::resizeEvent(QResizeEvent *event)
{
    _height = event->size().height();
    _width = event->size().width();
    emit sizeChanged();
}