/*
  OSMScout - a Qt backend for libosmscout and libosmscout-map
  Copyright (C) 2014  Tim Teulings

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "MapWidget/Theme.h"

#include <QApplication>
#include <QDebug>
#include <QFont>
#include <QFontMetrics>
#include <QScreen>

Theme::Theme()
    : textFontSize(0),
      averageCharWidth(0),
      numberCharWidth(0)
{
    // no code
}

Theme::~Theme()
{
    // no code
}

qreal Theme::mmToPixel(qreal mm) const
{
    return mm * GetDPI() / 25.4;
}

qreal Theme::pointToPixel(qreal point) const
{
    return point * 96.0 /*GetDPI()*/ / 72.0;
}

qreal Theme::GetDPI() const
{
    QScreen *srn = QApplication::screens().at(0);
    qreal dotsPerInch = (qreal)srn->physicalDotsPerInch();

    return dotsPerInch;
}

int Theme::GetTextFontSize() const
{
    if (textFontSize == 0)
    {
        QFont font;

        textFontSize = font.pixelSize();

        if (textFontSize == -1)
        {
            textFontSize = (int)pointToPixel(font.pointSize());
        }

        qDebug() << "TextFontSize:" << textFontSize << "px";
    }

    return textFontSize;
}

int Theme::GetAverageCharWidth() const
{
    if (averageCharWidth == 0)
    {
        QFont font;

        font.setPixelSize(GetTextFontSize());

        QFontMetrics metrics(font);

        averageCharWidth = metrics.averageCharWidth();

        qDebug() << "Average char width:" << averageCharWidth << "px";
    }

    return averageCharWidth;
}

int Theme::GetNumberCharWidth() const
{
    if (numberCharWidth == 0)
    {
        QFont font;

        font.setPixelSize(GetTextFontSize());

        QFontMetrics metrics(font);

        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('-'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance(','));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('.'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('0'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('1'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('2'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('3'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('4'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('5'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('6'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('7'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('8'));
        numberCharWidth = std::max(numberCharWidth, metrics.horizontalAdvance('9'));

        qDebug() << "Number char width: " << numberCharWidth << "px";
    }

    return numberCharWidth;
}
