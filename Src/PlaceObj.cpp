// STL includes
#include <mutex>

// Custom includes
#include "PlaceObj.hpp"

PlaceObj::PlaceObj() :
    _placeId(0),
    _address(""),
    _placeName(""),
    _latitude(0),
    _longitude(0),
    _placeType(UNKNOWN_TYPE)
{
    /// By default, all the days the place is closed
    for(int i = 0; i < AMOUNT_OF_DAYS; i++)
    {
        DaySchedule tmpDay(static_cast<DaysOfTheWeek>(i));
        _hoursOpened.push_back(tmpDay);
    }
}

PlaceObj::PlaceObj(int id, QString addr, QString name, double lat, double lgn, TypeOfPlace type) :
    _placeId(id),
    _address(addr),
    _placeName(name),
    _latitude(lat),
    _longitude(lgn),
    _placeType(type)
{
    /// By default, all the days the place is closed
    for(int i = 0; i < AMOUNT_OF_DAYS; i++)
    {
        DaySchedule tmpDay(static_cast<DaysOfTheWeek>(i));
        _hoursOpened.push_back(tmpDay);
    }
}

void PlaceObj::clear()
{
    _placeId = -1;
    _address.clear();
    _placeName.clear();
    _latitude = 0;
    _longitude = 0;
    _placeType = UNKNOWN_TYPE;
    _hoursOpened.clear();
}