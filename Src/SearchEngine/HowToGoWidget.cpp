/// Custom includes
#include "SearchEngine/HowToGoWidget.hpp"

/// Qt includes
#include <QGroupBox>
#include <QHBoxLayout>
#include <QScrollArea>

HowToGoWidget::HowToGoWidget(QWidget *parent) :
    QWidget(parent),
    _layout(nullptr),
    _listOfStops(nullptr),
    _showNextStepButton(nullptr),
    _footButton(nullptr)
{
    initWidget();
}

HowToGoWidget::~HowToGoWidget()
{
    delete _layout;
    delete _listOfStops;
    delete _showNextStepButton;
    delete _footButton;
}

void HowToGoWidget::initWidget()
{
    _layout = new QVBoxLayout(this);
    /// How to go scrollable list
    _listOfStops = new ListOfStopsWidget();
    QScrollArea *scroll = new QScrollArea(this);
    scroll->setWidget(_listOfStops);
    scroll->setWidgetResizable(true);
    _layout->addWidget(scroll);

    /// Add stop button
    QPushButton *addStop = new QPushButton(tr("Add stop"), this);
    connect(addStop,
        &QPushButton::clicked,
        _listOfStops,
        &ListOfStopsWidget::onAddNewStop);
    _layout->addWidget(addStop);

    /// Reset button
    QPushButton *reset = new QPushButton(tr("Reset"), this);
    connect(reset,
        &QPushButton::clicked,
        this,
        &HowToGoWidget::onReset);
    _layout->addWidget(reset);

    /// Buttons for the option of how to go: on foot, by bike or by car
    _footButton = new QRadioButton("On foot", this);
    QRadioButton *bikeButton = new QRadioButton("By bike", this);
    QRadioButton *carButton = new QRadioButton("By car", this);
    connect(_footButton,
        &QRadioButton::toggled,
        this,
        &HowToGoWidget::onChangeToFoot);
    connect(bikeButton,
        &QRadioButton::toggled,
        this,
        &HowToGoWidget::onChangeToBike);
    connect(carButton,
        &QRadioButton::toggled,
        this,
        &HowToGoWidget::onChangeToCar);

    QHBoxLayout *options = new QHBoxLayout(this);
    options->addWidget(_footButton);
    options->addWidget(bikeButton);
    options->addWidget(carButton);

    QGroupBox *optionsGroupBox = new QGroupBox(tr("Select the mean of transport: "));
    optionsGroupBox->setLayout(options);
    _layout->addWidget(optionsGroupBox);

    /// How to go button
    QPushButton *howToGoButton = new QPushButton(tr("How to go"), this);
    connect(howToGoButton,
        &QPushButton::clicked,
        _listOfStops,
        &ListOfStopsWidget::generateRouteVector);
    _layout->addWidget(howToGoButton);

    /// Button to show one step of the route
    _showNextStepButton = new QPushButton(tr("Show one route step"), this);
    connect(_showNextStepButton,
        &QPushButton::clicked,
        _listOfStops,
        &ListOfStopsWidget::generateNextRouteStepVector);
    _layout->addWidget(_showNextStepButton);

    /// We add an stretch in order to avoid expansion of the list unnecesarily
    _layout->addStretch();
    setLayout(_layout);

    /// We explicitely set the default mean by foot
    _footButton->setChecked(true);
    _listOfStops->onUpdateMeanOfTransport(ON_FOOT);

}

void HowToGoWidget::onChangeToFoot(bool checked)
{
    if(_listOfStops && checked)
    {
        _listOfStops->onUpdateMeanOfTransport(ON_FOOT);
    }
}

void HowToGoWidget::onChangeToBike(bool checked)
{
    if(_listOfStops && checked)
    {
        _listOfStops->onUpdateMeanOfTransport(BY_BIKE);
    }
}

void HowToGoWidget::onChangeToCar(bool checked)
{
    if(_listOfStops && checked)
    {
        _listOfStops->onUpdateMeanOfTransport(BY_CAR);
    }
}

void HowToGoWidget::onReset()
{
    if(_listOfStops)
    {
        _listOfStops->onReset();
        _footButton->setChecked(true);
    }
    emit reset();
}