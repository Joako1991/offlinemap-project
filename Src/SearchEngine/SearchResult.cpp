/// Qt includes
#include <QListWidgetItem>

/// Custom includes
#include "SearchEngine/SearchResult.hpp"

SearchResult::SearchResult(QWidget *parent):
    QWidget(parent),
    _resultListWidget(nullptr),
    _layout(nullptr),
    _emptyResultText(nullptr)
{
    _resultListWidget = new QListWidget(this);
    setMinimumHeight(400);
    _layout = new QVBoxLayout(this);
    _emptyResultText = new QLabel(this);
    _layout->addWidget(_emptyResultText);

    _layout->addWidget(_resultListWidget);
    setLayout(_layout);
}

SearchResult::~SearchResult()
{
    delete _resultListWidget;
    delete _emptyResultText;
}

void SearchResult::onSearchFinalResult(std::vector<PlaceObj> places, bool areWeClearing)
{
    /// We clear the list of results and we fill it out again with the
    /// more updated information
    _resultListWidget->clear();
    _searchResult.clear();
    if(places.size() || areWeClearing)
    {
        _emptyResultText->setText("");
    }
    else
    {
        _emptyResultText->setText("No places found");
    }
    for(auto &place : places)
    {
        PlaceWidget *placeResult = new PlaceWidget(place, this);
        _searchResult.push_back(placeResult);
        QListWidgetItem* item = new QListWidgetItem(_resultListWidget);
        item->setSizeHint(placeResult->sizeHint());
        // Add widget to QListWidget funList
        _resultListWidget->addItem(item);
        _resultListWidget->setItemWidget(item, placeResult);

        connect(placeResult,
            &PlaceWidget::widgetTapped,
            this,
            &SearchResult::onPlaceResultClicked);
    }
}

void SearchResult::onPlaceResultClicked(PlaceObj placeToSelect)
{
    /// We emit the information of the selected place to higher
    /// up layers
    emit placeClicked(placeToSelect);
}

void SearchResult::clearList()
{
    _resultListWidget->clear();
    _searchResult.clear();
    _emptyResultText->setText("");
}