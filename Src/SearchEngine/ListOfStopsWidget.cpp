// Custom includes
#include "SearchEngine/ListOfStopsWidget.hpp"

/// STL includes
#include <iostream>

/// Qt includes
#include <QMessageBox>

ListOfStopsWidget::ListOfStopsWidget(QWidget *parent) :
    QWidget(parent),
    _layout(nullptr),
    _to(nullptr),
    _from(nullptr),
    _stopUnderModification(nullptr),
    _maximumCurrentId(0),
    _currentStep(0)
{
    initWidget();
}

ListOfStopsWidget::~ListOfStopsWidget()
{
    delete _layout;
    delete _to;
    delete _from;
}

void ListOfStopsWidget::initWidget()
{
    _layout = new QVBoxLayout(this);

    _from = createStop(tr("FROM"), false);
    _to = createStop(tr("TO"), false);

    setLayout(_layout);
    _layout->setSizeConstraint(QLayout::SetMinimumSize);
    updateGridOfStops();
}

void ListOfStopsWidget::onModifyStop(StopWidget *stopToModify)
{
    _stopUnderModification = stopToModify;
    /// We disable this widget and its childs
    setEnabled(false);
    int stop_id = _idsMap.value(stopToModify, -1);
    if(stop_id == -1)
    {
        _idsMap[stopToModify] = _maximumCurrentId;
        stop_id = _maximumCurrentId;
        _maximumCurrentId++;
    }
    emit modificationRequested(stop_id);
}

void ListOfStopsWidget::onRemoveStop(StopWidget *stopWidget)
{
    int idx = _stops.indexOf(stopWidget);
    if (idx != -1)
    {
        int stop_id = _idsMap.value(stopWidget, -1);
        if(stop_id != -1)
        {
            emit removeStopMark(stop_id);
            _idsMap.remove(_stops[idx]);
        }
        delete _stops[idx];
        _stops.remove(idx);
        updateGridOfStops();
    }
    else
    {
        std::cout << "Cannot find the desired stop to remove" << std::endl;
    }
}

void ListOfStopsWidget::onAddNewStop()
{
    StopWidget *newStop = createStop(tr("Stop"), true);
    _stops.push_back(newStop);
    updateGridOfStops();
}

StopWidget *ListOfStopsWidget::createStop(QString title, bool includeRemove)
{
    if (includeRemove)
    {
        title += QString(" ") + QString::number(_stops.size() + 1);
    }

    StopWidget *newStop = new StopWidget(title, includeRemove, this);

    connect(newStop,
            &StopWidget::removePlace,
            this,
            &ListOfStopsWidget::onRemoveStop);

    connect(newStop,
            &StopWidget::modifyPlace,
            this,
            &ListOfStopsWidget::onModifyStop);

    return newStop;
}

void ListOfStopsWidget::clearLayout(QLayout *layout)
{
    /// Inspired in https://stackoverflow.com/a/5924726
    assert(layout);
    QLayoutItem *item;
    while((item = layout->takeAt(0)) != 0)
    {
        delete item;
    }
}

void ListOfStopsWidget::updateGridOfStops()
{
    clearLayout(_layout);
    _layout->addWidget(_from);
    int idx = 1;
    for(auto &st : _stops)
    {
        st->setTitle(tr("Stop ") + QString::number(idx));
        _layout->addWidget(st);
        idx++;
    }
    _layout->addWidget(_to);
    _currentStep = 0;
}

void ListOfStopsWidget::onReset()
{
    /// Inspired in https://stackoverflow.com/a/5924726
    assert(_layout);
    QLayoutItem *item;
    /// This loop will delete all the stops and the _from and _to instances
    while((item = _layout->takeAt(0)) != 0)
    {
        delete item->widget();
        delete item;
    }
    _stops.clear();

    _from = createStop(tr("FROM"), false);
    _to = createStop(tr("TO"), false);
    setLayout(_layout);
    _layout->setSizeConstraint(QLayout::SetMinimumSize);
    updateGridOfStops();
    /// We enable the widget and its childs
    setEnabled(true);
    cancelModificationRequest();
    _maximumCurrentId = 0;
    _idsMap.clear();
}

void ListOfStopsWidget::updateStopUnderModification(const PlaceObj& pl)
{
    /// We enable the widget and its childs and we update the place information
    setEnabled(true);
    if(_stopUnderModification)
    {
        _stopUnderModification->onPlaceInfoReceived(pl);
    }
    _stopUnderModification = nullptr;
}

void ListOfStopsWidget::updateTargetPoint(const PlaceObj& pl)
{
    assert(_to);
    /// We enable the widget and its childs and we update the place information
    setEnabled(true);
    if(pl.isValidPlace())
    {
        _to->onPlaceInfoReceived(pl);
    }
}

bool ListOfStopsWidget::checkUserEntries()
{
    assert(_to);
    assert(_from);
    if(!_from->getPlaceInfo().isValidPlace())
    {
        QMessageBox::warning(this, "ERROR", "The starting place point has not a valid position");
        return false;
    }
    else if (!_to->getPlaceInfo().isValidPlace())
    {
        QMessageBox::warning(this, "ERROR", "The target place point has not a valid position");
        return false;
    }

    int stopIdx = 1;
    for(auto &st : _stops)
    {
        if(!st->getPlaceInfo().isValidPlace())
        {
            QMessageBox::warning(this, "ERROR", tr("The stop ") + QString::number(stopIdx) + " point has not a valid position");
            return false;
        }
        stopIdx++;
    }
    return true;
}

std::vector<PlaceObj> ListOfStopsWidget::getListOfPlaces()
{
    /// We generate a vector with all the information entered by the user up to now
    std::vector<PlaceObj> myList;
    myList.push_back(_from->getPlaceInfo());
    for(auto &st : _stops)
    {
        myList.push_back(st->getPlaceInfo());
    }
    myList.push_back(_to->getPlaceInfo());
    return myList;
}

void ListOfStopsWidget::onUpdateMeanOfTransport(MeansOfTransport transport)
{
    MeansOfTransportMap transportMap;
    _meanOfTransport = transportMap.mapToEnum.value(transport);
}

void ListOfStopsWidget::generateRouteVector()
{
    /// We check user entries. If there is anything missing, checkUserEntries will
    /// show a pop up and return false
    if(checkUserEntries())
    {
        std::vector<PlaceObj> myList = getListOfPlaces();
        emit computePathRequest(myList, _meanOfTransport);
        _currentStep = 0;
    }
}

void ListOfStopsWidget::generateNextRouteStepVector()
{
    if(checkUserEntries())
    {
        /// We get the list of all the places
        std::vector<PlaceObj> myList = getListOfPlaces();
        const int vectorSize = myList.size();
        if(_currentStep < (vectorSize - 1))
        {
            /// We take the current step element, and the next one in
            /// order to compute the paths
            _currentStep = _currentStep % (vectorSize - 1);
            std::vector<PlaceObj> pieceOfRoute;
            pieceOfRoute.push_back(myList[_currentStep]);
            pieceOfRoute.push_back(myList[_currentStep+1]);
            /// We emit the signal only with the two points we want to see
            emit computePathRequest(pieceOfRoute, _meanOfTransport);
            _currentStep++;
        }
        else
        {
            emit computePathRequest(myList, _meanOfTransport);
            _currentStep = 0;
        }
    }
}
