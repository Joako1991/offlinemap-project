/// Custom includes
#include "SearchEngine/HowToGoWidget.hpp"
#include "SearchEngine/SearchManager.hpp"

SearchManager::SearchManager(QWidget *parent) :
    QWidget(parent),
    _layout(nullptr),
    _searchEng(nullptr),
    _searchres(nullptr),
    _searchWidget(nullptr),
    _howToGo(nullptr),
    _tabWidget(nullptr),
    _searchTabIndex(-1),
    _howToGoTabIndex(-1),
    _modificationPending(false)
{
    initSearchManagerWidget();
}

SearchManager::~SearchManager()
{
    delete _layout;
    delete _searchEng;
    delete _searchres;
    delete _searchWidget;
    delete _howToGo;
    delete _tabWidget;
}

void SearchManager::initSearchManagerWidget()
{
    _tabWidget = new QTabWidget(this);
    _layout = new QVBoxLayout(this);

    _searchWidget = new SearchWidget(this);
    _searchres = new SearchResult(this);

    QVBoxLayout *vLayout = new QVBoxLayout(this);
    QWidget *searchWidget = new QWidget(this);
    vLayout->addWidget(_searchWidget);
    vLayout->addWidget(_searchres);
    searchWidget->setLayout(vLayout);

    _tabWidget->addTab(searchWidget, tr("Search"));
    _searchTabIndex = 0;

    _howToGo = new HowToGoWidget(this);
    _tabWidget->addTab(_howToGo, tr("How to go"));
    _howToGoTabIndex = 1;

    _layout->addWidget(_tabWidget);

    /// The search engine is just a helper class, it is not a widget we can show
    _searchEng = new SearchEngine();

    /// Connections
    /// Search Widget and Search Engine
    connect(_searchWidget,
            &SearchWidget::searchResult,
            _searchEng,
            &SearchEngine::onSearchRequest);

    /// Search engine and Search Results
    connect(_searchEng,
            &SearchEngine::newSearchResult,
            _searchres,
            &SearchResult::onSearchFinalResult);

    /// Search Result and Search Manager
    connect(_searchres,
            &SearchResult::placeClicked,
            this,
            &SearchManager::onPlaceClicked);

    /// Search Widget and and Search Manager
    connect(_searchWidget,
        &SearchWidget::clearPressed,
        this,
        &SearchManager::onClear);

    connect(_searchWidget,
        &SearchWidget::howToGoRequested,
        this,
        &SearchManager::onClickedHowToGo);

    /// how to go widget and Search Widget
    connect(_howToGo,
        &HowToGoWidget::reset,
        _searchWidget,
        &SearchWidget::clear);

    /// How to go and SearchResult
    connect(_howToGo,
        &HowToGoWidget::reset,
        _searchres,
        &SearchResult::clearList);

    /// How to go widget and Search Manager
    connect(_howToGo,
        &HowToGoWidget::reset,
        this,
        &SearchManager::onClear);

    connect(_howToGo->getListOfStops(),
        &ListOfStopsWidget::removeStopMark,
        this,
        &SearchManager::onRemoveMark);

    connect(_howToGo->getListOfStops(),
        &ListOfStopsWidget::computePathRequest,
        this,
        &SearchManager::onComputePath);

    connect(_howToGo->getListOfStops(),
        &ListOfStopsWidget::cancelModificationRequest,
        this,
        &SearchManager::onCancelRequest);

    connect(_howToGo->getListOfStops(),
        &ListOfStopsWidget::modificationRequested,
        this,
        &SearchManager::onPlaceModificationRequested);

    setLayout(_layout);
}

void SearchManager::onPlaceModificationRequested(int id)
{
    _tabWidget->setCurrentIndex(_searchTabIndex);
    _modificationPending = true;
    emit stopIdChanged(id);
}

void SearchManager::onSearchFinished(const PlaceObj &pl)
{
    if(_modificationPending)
    {
        _howToGo->getListOfStops()->updateStopUnderModification(pl);
        _tabWidget->setCurrentIndex(_howToGoTabIndex);
        _modificationPending = false;
        _searchWidget->clear();
        _searchres->clearList();
        _lastPlaceClicked.clear();
    }
}

void SearchManager::onCancelRequest()
{
    _modificationPending = false;
}

void SearchManager::onPlaceClicked(const PlaceObj& pl)
{
    _lastPlaceClicked = pl;
    /// We inform the How to go Search that a place has been selected
    /// and we emit a signal to inform higher level layers about this event
    onSearchFinished(pl);
    emit markPlaceRequest(pl);
}

void SearchManager::onClear()
{
    _lastPlaceClicked.clear();
    emit clearRequested();
}

void SearchManager::onRemoveMark(int id)
{
    emit removeMarkRequested(id);
}

void SearchManager::onMarkAdded(double lat, double lng)
{
    if(_searchWidget)
    {
        _searchWidget->onClickMap(lat, lng);
    }
}

void SearchManager::onComputePath(std::vector<PlaceObj> &list, QString meanTransport)
{
    emit computePathRequested(list, meanTransport);
}

void SearchManager::onClickedHowToGo()
{
    assert(_howToGo);
    _howToGo->getListOfStops()->updateTargetPoint(_lastPlaceClicked);
    _tabWidget->setCurrentIndex(_howToGoTabIndex);
    _modificationPending = false;
    _searchWidget->clear();
}