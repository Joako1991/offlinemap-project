/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include <DBEngine/DBQueries.hpp>
#include <MapWidget/MapAuxiliary.hpp>
#include "SearchEngine/SearchEngine.hpp"

#define EXACT_PLACE_RADIOUS_M 50

QVector<PlaceObj> SearchEngine::getPlacesBasedOnDistance(double lat_nearby, double lon_nearby, int nearbyRadius, QVector<PlaceObj> placesIn)
{
    QVector<PlaceObj> placesOutBasedDistance;
    for(auto& place : placesIn)
    {
        double distance = MapAuxiliary::getDistanceBetweenPointsInMeters(lat_nearby, lon_nearby, place.getLat(), place.getLong());
        if (distance <= nearbyRadius)
        {
            placesOutBasedDistance.append(place);
        }
    }
    return placesOutBasedDistance;
}

QVector<PlaceObj> SearchEngine::getPlacesBasedFiltering(PossibleSearchCase searchType,SearchParams params)
{
    QVector<PlaceObj> placesFilterOut;
    /// First we filter based on the information given by the user: Type and Text entry
    placesFilterOut = DBEngine::instance()->getPlacesBasedTypeAndName(params.placeType, params.enteredText);
    if(params.isMapClicked && searchType == SEARCH_EXACT_PLACE)
    {
        /// If the user also added lat and long information, then we look around the place
        /// in a radious of EXACT_PLACE_RADIOUS_M meters
        QVector<PlaceObj> placesFilteredAround = getPlacesBasedOnDistance(
            params.latitude,
            params.longitude,
            EXACT_PLACE_RADIOUS_M,
            placesFilterOut);
        if(placesFilteredAround.isEmpty())
        {
            QString name = QString::number(params.latitude)+ " , " + QString::number(params.longitude);
            placesFilterOut.clear();
            placesFilterOut.push_back(PlaceObj(0, " ", name, params.latitude, params.longitude, UNKNOWN_TYPE));
        }
        else
        {
            placesFilterOut = placesFilteredAround;
        }
    }
    return placesFilterOut;
}

void SearchEngine::onSearchRequest(PossibleSearchCase searchType, SearchParams params)
{
    QVector<PlaceObj> placesOut;
    bool isClearing = false;
    switch (searchType)
    {
    case CLEAR_RESULTS:
        isClearing = true;
        placesOut.clear();
        break;
    case SHOW_ALL_PLACES:
        placesOut = DBEngine::instance()->getListOfPlaces();
        break;
    case SEARCH_EXACT_PLACE:
        placesOut = getPlacesBasedFiltering(searchType, params);
        break;
    case SEARCH_NEARBY:
        QVector<PlaceObj> placesIn = getPlacesBasedFiltering(searchType, params);
        placesOut = getPlacesBasedOnDistance(params.latitude, params.longitude, params.radiousDistance, placesIn);
        break;
    }
    emit newSearchResult(placesOut.toStdVector(), isClearing);
}
