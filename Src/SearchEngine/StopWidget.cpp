// QT includes
#include <QGridLayout>

// Custom includes
#include "SearchEngine/StopWidget.hpp"

StopWidget::StopWidget(QString title, bool includeRemove, QWidget *parent) :
    QWidget(parent),
    _titleLabel(nullptr),
    _placeLabel(nullptr),
    _latlonLabel(nullptr),
    _addressLabel(nullptr),
    _modifyButton(nullptr),
    _removeButton(nullptr),
    _title(title),
    _includeRemove(includeRemove),
    _gridCounter(0)
{
    initWidget();
}

StopWidget::~StopWidget()
{
    delete _titleLabel;
    delete _placeLabel;
    delete _latlonLabel;
    delete _addressLabel;
    delete _modifyButton;
    delete _removeButton;
}

void StopWidget::initWidget()
{
    // Layout
    QGridLayout *_layoutStops = new QGridLayout(this);

    // Title label
    _titleLabel = new QLabel(_title, this);
    _layoutStops->addWidget(_titleLabel, _gridCounter++, 0);

    // Place label
    _placeLabel = new QLabel(tr("Undefined place"), this);
    _layoutStops->addWidget(_placeLabel, _gridCounter++, 0);

    // Address label
    _addressLabel = new QLabel(tr("Undefined address"), this);
    _layoutStops->addWidget(_addressLabel, _gridCounter++, 0);

    // Lat Long label
    _latlonLabel = new QLabel(tr("Undefined lat/lon"), this);
    _layoutStops->addWidget(_latlonLabel, _gridCounter++, 0);

    // Modify button
    _modifyButton = new QPushButton(tr("Modify"), this);
    _layoutStops->addWidget(_modifyButton, _gridCounter++, 0);
    connect(_modifyButton,
        &QPushButton::clicked,
        this,
        &StopWidget::onClickModifyButton);

    // Remove button
    if (_includeRemove)
    {
        _removeButton = new QPushButton(tr("Remove"), this);
        _layoutStops->addWidget(_removeButton, _gridCounter++, 0);
        connect(_removeButton,
            &QPushButton::clicked,
            this,
            &StopWidget::onClickRemoveButton);
    }
    _layoutStops->setSizeConstraint(QLayout::SetMinimumSize);
}

void StopWidget::setTitle(QString newTitle)
{
    _title = newTitle;
    _titleLabel->setText(_title);
}

void StopWidget::onPlaceInfoReceived(PlaceObj newPlace)
{
    _storedPlace = newPlace;
    _placeLabel->setText(_storedPlace.getPlaceName());
    _addressLabel->setText(_storedPlace.getAddress());
    _latlonLabel->setText(QString::number(_storedPlace.getLat()) + ", " + QString::number(_storedPlace.getLong()));
}

void StopWidget::onClickModifyButton()
{
    emit modifyPlace(this);
}

void StopWidget::onClickRemoveButton(bool checked)
{
    (void)checked;
    emit removePlace(this);
}

void StopWidget::reset()
{
    _storedPlace.clear();
    _placeLabel->setText(tr("Undefined place"));
    _addressLabel->setText(tr("Undefined address"));
    _latlonLabel->setText(tr("Undefined lat/lon"));
}