// Custom includes
#include "SearchEngine/SearchWidget.hpp"

// STL includes
#include <iostream>

// QT include
#include <QFormLayout>
#include <QMessageBox>
#include <QPushButton>

SearchWidget::SearchWidget(QWidget *parent) :
    QWidget(parent),
    _isMapClicked(false),
    _lat(0),
    _lon(0),
    _latlonLabel(nullptr),
    _searchGroupBox(nullptr),
    _typeComboBox(nullptr),
    _inputText(nullptr),
    _exactButton(nullptr),
    _nearbyButton(nullptr),
    _distanceSpinBox(nullptr)
{
    initWidget();
}

SearchWidget::~SearchWidget()
{
    delete _distanceSpinBox;
    delete _nearbyButton;
    delete _exactButton;
    delete _inputText;
    delete _typeComboBox;
    delete _searchGroupBox;
    /// TODO: Check why we get SegFault when this line is enabled
//    delete _latlonLabel;
}

void SearchWidget::initSearchGroupBox()
{
    // Groupbox Search
    _searchGroupBox = new QGroupBox(tr("Search"),this);

    // Layout for GroupBox
    QFormLayout *layoutSearch = new QFormLayout(this);

    // Exact Place Button
    _exactButton = new QRadioButton("Exact place", this);
    connect(_exactButton,
        &QRadioButton::toggled,
        this,
        &SearchWidget::onToggledExactButton);
    layoutSearch->addRow(_exactButton);

    // Nearby Button
    _nearbyButton = new QRadioButton("Nearby (Select the place nearby on map)", this);
    connect(_nearbyButton,
        &QRadioButton::toggled,
        this,
        &SearchWidget::onToggledNearbyButton);
    layoutSearch->addRow(_nearbyButton);

    // Nearby Radius Spinbox
    _distanceSpinBox =  new QSpinBox(this);
    _distanceSpinBox->setRange(1, 100000);
    _distanceSpinBox->setSingleStep(1);
    _distanceSpinBox->setSuffix(" meters");
    layoutSearch->addRow(_distanceSpinBox);

    // Place Types List
    QLabel *typeLabel = new QLabel(tr("Type of place to search:"));
    _typeComboBox = new QComboBox(this);
    for(auto &type : _placesTypes.mapToEnum.toStdMap())
    {
        _typeComboBox->addItem(type.first);
    }
    layoutSearch->addRow(typeLabel, _typeComboBox);

    // Text Linedit
    _inputText = new QLineEdit(this);
    _inputText->setPlaceholderText("Write the name of the place");
    layoutSearch->addRow(_inputText);

    // Longitude Latitude Label
    _latlonLabel = new QLabel(this);
    layoutSearch->addRow(_latlonLabel);

    layoutSearch->setSizeConstraint(QLayout::SetMinimumSize);
    _searchGroupBox->setLayout(layoutSearch);
}

void SearchWidget::initWidget()
{
    // Layout
    QFormLayout *layout = new QFormLayout(this);

    initSearchGroupBox();

    layout->addRow(_searchGroupBox);

    // Search Button
    QPushButton *searchPushButton = new QPushButton(tr("Search"), this);
    connect(searchPushButton,
        &QPushButton::clicked,
        this,
        &SearchWidget::onClickSearchButton);
    layout->addRow(searchPushButton);

    // Show all places Button
    QPushButton *showAllPlacesButton = new QPushButton(tr("Show all places"), this);
    connect(showAllPlacesButton,
        &QPushButton::clicked,
        this,
        &SearchWidget::onClickShowAllPlaces);
    layout->addRow(showAllPlacesButton);

    QPushButton *clearSearchButton = new QPushButton(tr("Clear"), this);
    connect(clearSearchButton,
        &QPushButton::clicked,
        this,
        &SearchWidget::onClickClearButton);
    layout->addRow(clearSearchButton);

    QPushButton *howToGoButton = new QPushButton(tr("How to go"), this);
    connect(howToGoButton,
        &QPushButton::clicked,
        this,
        &SearchWidget::onClickHowToGo);
    layout->addRow(howToGoButton);

    // Search QWidget
    layout->setSizeConstraint(QLayout::SetMinimumSize);
    setLayout(layout);

    onClickShowAllPlaces(true);
}

void SearchWidget::onClickMap(double lat, double lon)
{
    _isMapClicked = true;
    _lat = lat;
    _lon = lon;
    _latlonLabel->setText("Lat: " + QString::number(_lat) + ", Lon: " + QString::number(_lon));
}

void SearchWidget::onClickShowAllPlaces(bool checked)
{
    (void)checked;
    SearchParams mySearchResult;
    emit searchResult(SHOW_ALL_PLACES, mySearchResult);

    // Reset search parameters
    _isMapClicked = false;
    _exactButton->setChecked(true);
    _distanceSpinBox->setValue(5);
    _distanceSpinBox->setEnabled(false);
    _inputText->setText("");
    _latlonLabel->setText("");
    _typeComboBox->setCurrentIndex(0);
}

void SearchWidget::onClickSearchButton(bool checked)
{
    (void)checked;

    // SEARCH: Exact place or nearby
    PossibleSearchCase searchCase;

    // Search Parameters
    SearchParams mySearchResult;

    if (_exactButton->isChecked())
    {
        searchCase = SEARCH_EXACT_PLACE;
        mySearchResult.placeType = _placesTypes.mapToEnum.value(_typeComboBox->currentText(), UNKNOWN_TYPE);
        mySearchResult.enteredText = _inputText->text().toStdString();
        if (_isMapClicked)
        {
            mySearchResult.isMapClicked = true;
            mySearchResult.latitude = _lat;
            mySearchResult.longitude = _lon;
        }
        emit searchResult(searchCase, mySearchResult);
    }
    else if (_nearbyButton->isChecked())
    {
        searchCase = SEARCH_NEARBY;
        if (_isMapClicked)
        {
            mySearchResult.isMapClicked = true;
            mySearchResult.latitude = _lat;
            mySearchResult.longitude = _lon;
            mySearchResult.radiousDistance = _distanceSpinBox->value();
            mySearchResult.placeType = _placesTypes.mapToEnum.value(_typeComboBox->currentText(), UNKNOWN_TYPE);
            mySearchResult.enteredText = _inputText->text().toStdString();
            emit searchResult(searchCase, mySearchResult);
        }
        else
        {
            QMessageBox::warning(this, "Search Error", "Select a point in the map to make the nearby place search!");
        }
    }
    else
    {
        std::cerr << "Error: cannot detect type of search" << std::endl;
        assert(0);
    }
}

void SearchWidget::onClickClearButton(bool checked)
{
    (void)checked;
    emit clearPressed();

    SearchParams mySearchResult;
    emit searchResult(CLEAR_RESULTS, mySearchResult);
    clear();
}

void SearchWidget::onClickHowToGo(bool checked)
{
    (void)checked;
    emit howToGoRequested();
}

void SearchWidget::onToggledExactButton(bool checked)
{
    (void)checked;
    _distanceSpinBox->setEnabled(false);
}

void SearchWidget::onToggledNearbyButton(bool checked)
{
    (void)checked;
    _distanceSpinBox->setEnabled(true);
}

void SearchWidget::clear()
{
    // Reset search parameters
    _isMapClicked = false;
    _exactButton->setChecked(true);
    _distanceSpinBox->setValue(5);
    _distanceSpinBox->setEnabled(false);
    _inputText->setText("");
    _latlonLabel->setText("");
    _typeComboBox->setCurrentIndex(0);
}
