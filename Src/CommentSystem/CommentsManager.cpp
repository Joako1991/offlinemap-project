/// Qt includes
#include <QHBoxLayout>
#include <QPushButton>

/// Custom includes
#include "CommentSystem/CommentsManager.hpp"

CommentsManager::CommentsManager(const User &user, QWidget *parent) :
    QWidget(parent),
    _commentsDialog(nullptr),
    _newCommentDialog(nullptr)
{
    initializeWidget(user);
}

CommentsManager::~CommentsManager()
{
    delete _commentsDialog;
    delete _newCommentDialog;
}

void CommentsManager::initializeWidget(const User &user)
{
    QHBoxLayout *layout = new QHBoxLayout(this);
    QPushButton *newComment = new QPushButton(tr("New comment"), this);
    layout->addWidget(newComment);
    QPushButton *seeComments = new QPushButton(tr("See comments"), this);
    layout->addWidget(seeComments);

    _commentsDialog = new DialogShowComments();
    _newCommentDialog = new DialogNewComment(user);

    connect(seeComments,
        &QPushButton::clicked,
        _commentsDialog,
        &DialogShowComments::showDialog);

    connect(newComment,
        &QPushButton::clicked,
        _newCommentDialog,
        &DialogNewComment::showDialog);

    setLayout(layout);
}

void CommentsManager::onNewPlace(const PlaceObj& pl)
{
    if(_commentsDialog && _newCommentDialog)
    {
        _commentsDialog->setPlaceInformation(pl);
        _newCommentDialog->setPlaceInformation(pl);
    }
}

void CommentsManager::onReset()
{
    if(_commentsDialog && _newCommentDialog)
    {
        _commentsDialog->reset();
        _newCommentDialog->reset();
    }
}