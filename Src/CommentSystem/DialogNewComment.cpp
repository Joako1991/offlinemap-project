/// Qt includes
#include <QDate>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QMessageBox>
#include <QPushButton>

/// Custom includes
#include "CommentSystem/DialogNewComment.hpp"
#include <DBEngine/DBEngine.hpp>

DialogNewComment::DialogNewComment(const User& user, QDialog *parent) :
    QDialog(parent),
    _localUser(user),
    _placeName(nullptr),
    _userLabel(nullptr),
    _dayOfComment(nullptr),
    _editComment(nullptr),
    _editReputation(nullptr)
{
    assert(user.isValid());
    initializeDialog();
    setModal(true);
}

DialogNewComment::~DialogNewComment()
{
    delete _placeName;
    delete _userLabel;
    delete _dayOfComment;
    delete _editComment;
    delete _editReputation;
}

void DialogNewComment::setPlaceInformation(const PlaceObj& pl)
{
    /// We only update the place if it is valid
    if(pl.isValidPlace() && _placeName)
    {
        _localPlace = pl;
        _placeName->setText("Comments for " + pl.getPlaceName());
        setWindowTitle(tr("Add a comment to ") + pl.getPlaceName());
    }
}

void DialogNewComment::setUserInformation(const User& newUser)
{
    /// We only update the user if it is valid
    if(newUser.isValid())
    {
        _localUser = newUser;
    }
}

void DialogNewComment::showDialog()
{
    if(_localPlace.isValidPlace())
    {
        QVector<PlaceObj> placesWithThisName = DBEngine::instance()->getPlacesBasedName(_localPlace.getPlaceName().toStdString());
        if(placesWithThisName.size() == 1)
        {
            _dayOfComment->setText(tr("Date: ") + QDate::currentDate().toString("dd/MM/yyyy"));
            _userLabel->setText(tr("User: ") + _localUser.getUserName());
            show();
        }
        else
        {
            QMessageBox::warning(this,
                "ERROR",
                "The place " + _localPlace.getPlaceName() + " is not in the database.\n" +
                "In order to add a comment about it, please insert the place first");
        }

    }
    else
    {
        QMessageBox::warning(this,
            "ERROR",
            "In order to add a new comment you need to select a place first");
    }
}

void DialogNewComment::initializeDialog()
{
    QFormLayout *layout = new QFormLayout(this);
    int rowCounter = 0;

    /// Place Name
    _placeName = new QLabel(tr(""), this);
    _placeName->setAlignment(Qt::AlignCenter);
    layout->addRow(_placeName);

    /// Comment section
    QLabel *commentLabel = new QLabel(tr("Comment"), this);
    _editComment = new QTextEdit(this);
    _editComment->setPlaceholderText("What do you think about this place?");
    _editComment->setStyleSheet("QCustomLineEdit{color: gray;}");
    layout->addRow(commentLabel, _editComment);
    rowCounter++;

    /// Reputation
    QLabel *reputationLabel = new QLabel(tr("How many stars do you give?"), this);
    _editReputation = new QDoubleSpinBox(this);
    _editReputation->setMinimum(0.0);
    _editReputation->setMaximum(5.0);
    layout->addRow(reputationLabel, _editReputation);
    rowCounter++;

    /// User information
    _userLabel = new QLabel(tr(""), this);
    layout->addRow(_userLabel);

    /// Date
    _dayOfComment = new QLabel(tr(""), this);
    layout->addRow(_dayOfComment);

    /// We create a Ok and close button and we connect it to the close method
    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Close);
    buttons->button(QDialogButtonBox::Ok)->setText(tr("Submit"));
    buttons->button(QDialogButtonBox::Close)->setText(tr("Close"));
    layout->addRow(buttons);

    // connects slots
    connect(buttons->button(QDialogButtonBox::Ok),
            &QPushButton::clicked,
            this,
            &DialogNewComment::onNewCommentSubmitted);

    connect(buttons->button(QDialogButtonBox::Close),
            &QPushButton::clicked,
            this,
            &QDialog::close);

    setLayout(layout);
}

void DialogNewComment::onNewCommentSubmitted(bool checked)
{
    (void)checked;
    bool isValidComment = true;

    /// We asses if the set place is valid. We don't need to check the user because
    /// it is checked on construction
    assert(_localPlace.isValidPlace());
    if(_editComment->toPlainText().isEmpty())
    {
        isValidComment = false;
        QMessageBox::critical(this,
            "ERROR",
            "Comment cannot be empty");
    }

    if(isValidComment)
    {
        CommentObj myComment(
            _editComment->toPlainText().toStdString(),
            _localPlace.getPlaceName().toStdString(),
            _localUser.getUserName().toStdString(),
            _editReputation->value(),
            QDate::currentDate());

        if(DBEngine::instance()->insertNewComment(myComment))
        {
            QMessageBox::information(this,
                "New comment",
                "Comment added successfully to the database");
            close();
        }
        else
        {
            QMessageBox::critical(this,
                "ERROR",
                "Comment cannot be added to the database");
        }
    }
}

void DialogNewComment::reset()
{
    _localPlace.clear();
}