/// Qt includes
#include <QVBoxLayout>

/// Custom includes
#include "CommentSystem/CommentWidget.hpp"

CommentWidget::CommentWidget(CommentObj cm, QWidget *parent) :
    QWidget(parent),
    _localComment(cm),
    _user(nullptr),
    _comment(nullptr),
    _reputation(nullptr),
    _day(nullptr)
{
    assert(cm.isCommentValid());
    initializeWidget();
}

CommentWidget::~CommentWidget()
{
    delete _user;
    delete _comment;
    delete _reputation;
    delete _day;
}

void CommentWidget::setCommentInformation(const CommentObj& cm)
{
    if(cm.isCommentValid())
    {
        _localComment = cm;
        updateLabels();
    }
}

void CommentWidget::updateLabels()
{
    _user->setText("Comment made by: " +
        QString::fromStdString(_localComment.getUsername()));
    _comment->setText(QString::fromStdString(_localComment.getComment()));
    _reputation->setText("Reputation given: " +
        QString::number(_localComment.getReputation(), 'f', 1));
    _day->setText("Comment made on: " +
        _localComment.getCommentDate().toString("dd/MM/yyyy"));
}

void CommentWidget::initializeWidget()
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    _comment = new QLabel(tr(""), this);
    _comment->setWordWrap(true);
    layout->addWidget(_comment);
    _reputation = new QLabel(tr(""), this);
    layout->addWidget(_reputation);
    _user = new QLabel(tr(""), this);
    _user->setWordWrap(true);
    layout->addWidget(_user);
    _day = new QLabel(tr(""), this);
    layout->addWidget(_day);

    updateLabels();
    setLayout(layout);
}
