/// Qt includes
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QPushButton>

/// STL includes
#include <vector>

/// Custom includes
#include <CommentObj.hpp>
#include <CommentSystem/CommentWidget.hpp>
#include "CommentSystem/DialogShowComments.hpp"
#include <DBEngine/DBEngine.hpp>

DialogShowComments::DialogShowComments(QDialog *parent) :
    QDialog(parent),
    _listOfComments(nullptr),
    _placeName(nullptr)
{
    initializeDialog();
    setModal(true);
}

DialogShowComments::~DialogShowComments()
{
    delete _listOfComments;
    delete _placeName;
}

void DialogShowComments::initializeDialog()
{
    QVBoxLayout *layout = new QVBoxLayout(this);

    /// Place Label
    _placeName = new QLabel(tr(""), this);
    /// We allign the label to be in the center of the widget
    _placeName->setAlignment(Qt::AlignCenter);
    layout->addWidget(_placeName);

    /// List of comments
    _listOfComments = new QListWidget(this);
    layout->addWidget(_listOfComments);

    /// We create a close button and we connect it to the close method
    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Close);
    buttons->button(QDialogButtonBox::Close)->setText(tr("Close"));
    layout->addWidget(buttons);

    // connects slots
    connect(buttons->button(QDialogButtonBox::Close),
            &QPushButton::clicked,
            this,
            &QDialog::close);

    setLayout(layout);
    setMinimumHeight(400);
    setMinimumWidth(400);
}

void DialogShowComments::showDialog()
{
    if(_localPlace.isValidPlace())
    {
        fillListWithComments();
        show();
    }
    else
    {
        QMessageBox::warning(this,
            "ERROR",
            "In order to see the comments you need to select a place first");
    }
}

void DialogShowComments::setPlaceInformation(const PlaceObj& pl)
{
    /// We only update the place if it is valid
    if(pl.isValidPlace())
    {
        _localPlace = pl;
        _placeName->setText("Comments for " + pl.getPlaceName());
        setWindowTitle(tr("Comments about ") + _localPlace.getPlaceName());
    }
}

void DialogShowComments::fillListWithComments()
{
    assert(_listOfComments);
    /// We clear the actual list and we fill it with the comments retrieved from the DB
    _listOfComments->clear();
    std::vector<CommentObj> comments;

    bool ok_cm = DBEngine::instance()->getPlaceComments(
        _localPlace.getPlaceName().toStdString(),
        comments);

    if(ok_cm)
    {
        for(auto &cm : comments)
        {
            QListWidgetItem *item = new QListWidgetItem(_listOfComments);
            CommentWidget *myComment = new CommentWidget(cm, this);
            item->setSizeHint(myComment->sizeHint());
            _listOfComments->addItem(item);
            _listOfComments->setItemWidget(item, myComment);
        }
    }
}

void DialogShowComments::reset()
{
    _localPlace.clear();
}