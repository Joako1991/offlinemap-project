/// Qt includes
#include <QDialogButtonBox>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QPushButton>

/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include "ErasePlaceWidget/DialogEraseAPlace.hpp"
#include <PlaceObj.hpp>

DialogEraseAPlace::DialogEraseAPlace(QDialog *parent) :
    QDialog(parent),
    _listOfPlaces(nullptr),
    _layout(nullptr)
{
    initializeDialog();
    setWindowTitle(tr("Erase a place"));
    setModal(true);
}

DialogEraseAPlace::~DialogEraseAPlace()
{
    delete _listOfPlaces;
    delete _layout;
}

void DialogEraseAPlace::initializeDialog()
{
    _listOfPlaces = new QListWidget(this);
    _layout = new QVBoxLayout(this);
    _layout->addWidget(_listOfPlaces);

    /// We create a cancel button and we connect it to the close method
    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Cancel);
    buttons->button(QDialogButtonBox::Cancel)->setText(tr("Cancel"));
    _layout->addWidget(buttons);

    // connects slots
    connect(buttons->button(QDialogButtonBox::Cancel),
            &QPushButton::clicked,
            this,
            &QDialog::close);
    setLayout(_layout);
    setMinimumHeight(600);
}

void DialogEraseAPlace::fillListWithPlaces()
{
    assert(_listOfPlaces);
    _listOfPlaces->clear();
    QVector<PlaceObj> placesInDatabase = DBEngine::instance()->getListOfPlaces();
    for(auto &pl : placesInDatabase)
    {
        QListWidgetItem *item = new QListWidgetItem(_listOfPlaces);
        PlaceWidget *myPlaceWidget = new PlaceWidget(pl, this);
        item->setSizeHint(myPlaceWidget->sizeHint());
        _listOfPlaces->addItem(item);
        _listOfPlaces->setItemWidget(item, myPlaceWidget);
        connect(myPlaceWidget,
            &PlaceWidget::widgetTapped,
            this,
            &DialogEraseAPlace::onPlaceClicked);
    }
}

void DialogEraseAPlace::onPlaceClicked(PlaceObj placeToErase)
{
    /// This message box is inspired in the example give at
    /// https://doc.qt.io/qt-5/qmessagebox.html
    QMessageBox msgBox;
    msgBox.setWindowTitle(tr("Confirmation"));
    msgBox.setText("You are about to erase definitely an entry from the database.");
    msgBox.setInformativeText("Are you sure you want to erase the entry of " + placeToErase.getPlaceName() + "?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();
    switch (ret) {
    case QMessageBox::Yes:
        if(DBEngine::instance()->eraseAPlaceFromDatabase(placeToErase))
        {
            fillListWithPlaces();
        }
        else
        {
            QMessageBox::warning(this, "Error", "Entry could not be erased");
        }
        break;
    case QMessageBox::Cancel:
        // Go back to the erase window
        break;
    default:
        // should never be reached
        break;
    }
}

void DialogEraseAPlace::showWindow(bool checked)
{
    (void)checked;
    fillListWithPlaces();
    show();
    exec();
}
