/// Qt includes
#include <QPushButton>

/// Custom includes
#include "ErasePlaceWidget/ErasePlaceWidget.hpp"

ErasePlaceWidget::ErasePlaceWidget(QWidget *parent) :
    QWidget(parent),
    _layout(nullptr),
    _dialog(nullptr)
{
    initializeWidget();
}

ErasePlaceWidget::~ErasePlaceWidget()
{
    delete _layout;
    delete _dialog;
}

void ErasePlaceWidget::initializeWidget()
{
    _layout = new QHBoxLayout(this);
    _layout->setMargin(0);
    QPushButton *eraseAPlace = new QPushButton("Erase a place", this);
    _layout->addWidget(eraseAPlace);
    setLayout(_layout);

    _dialog = new DialogEraseAPlace();

    connect(eraseAPlace,
        &QPushButton::clicked,
        _dialog,
        &DialogEraseAPlace::showWindow);
}
