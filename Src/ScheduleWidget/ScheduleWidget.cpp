/// Qt includes
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>

/// Custom includes
#include <DBEngine/DBEngine.hpp>
#include "ScheduleWidget/ScheduleWidget.hpp"

ScheduleWidget::ScheduleWidget(QWidget *parent) :
    QWidget(parent),
    _dialog(nullptr)
{
    initializeWidget();
}

ScheduleWidget::~ScheduleWidget()
{
    delete _dialog;
}

void ScheduleWidget::initializeWidget()
{
    QHBoxLayout *layout = new QHBoxLayout(this);

    _dialog = new DialogShowSchedule();

    QPushButton *showSched = new QPushButton(tr("Show place schedule"), this);

    connect(showSched,
        &QPushButton::clicked,
        this,
        &ScheduleWidget::onClickShowSchedule);

    layout->addWidget(showSched);
    setLayout(layout);
}

void ScheduleWidget::setPlace(const PlaceObj& pl)
{
    if(pl.isValidPlace() && pl.getPlaceHours().size() == 7)
    {
        _localPlace = pl;
    }
}

void ScheduleWidget::onReset()
{
    _localPlace.clear();
}

void ScheduleWidget::onClickShowSchedule(bool checked)
{
    (void)checked;
    assert(_dialog);
    if(_localPlace.isValidPlace() && _localPlace.getPlaceHours().size() == 7)
    {
        QVector<PlaceObj> placesWithThisName = DBEngine::instance()->getPlacesBasedName(_localPlace.getPlaceName().toStdString());
        if(placesWithThisName.size() == 1)
        {
            _dialog->showDialog(placesWithThisName[0]);
        }
        else
        {
            QMessageBox::warning(this,
                "ERROR",
                "We do not know the schedule for " + _localPlace.getPlaceName());
        }
    }
    else
    {
        QMessageBox::warning(this,
            "ERROR",
            "Please first select a place to see its schedule");
    }
}
