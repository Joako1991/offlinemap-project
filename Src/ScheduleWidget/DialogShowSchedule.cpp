/// Qt includes
#include <QDialogButtonBox>
#include <QPushButton>
#include <QVBoxLayout>

/// Custom includes
#include "ScheduleWidget/DialogShowSchedule.hpp"

DialogShowSchedule::DialogShowSchedule(QDialog *parent) :
    QDialog(parent),
    _mondaySchedLabel(nullptr),
    _tuesdaySchedLabel(nullptr),
    _wednesdaySchedLabel(nullptr),
    _thrusdaySchedLabel(nullptr),
    _fridaySchedLabel(nullptr),
    _saturdaySchedLabel(nullptr),
    _sundaySchedLabel(nullptr)
{
    initializeDialog();
    setModal(true);
}

DialogShowSchedule::~DialogShowSchedule()
{
    delete _mondaySchedLabel;
    delete _tuesdaySchedLabel;
    delete _wednesdaySchedLabel;
    delete _thrusdaySchedLabel;
    delete _fridaySchedLabel;
    delete _saturdaySchedLabel;
    delete _sundaySchedLabel;
}

void DialogShowSchedule::initializeDialog()
{
    QVBoxLayout *layout = new QVBoxLayout(this);

    _mondaySchedLabel = new QLabel(tr(""), this);
    _mondaySchedLabel->setAlignment(Qt::AlignCenter);
    layout->addWidget(_mondaySchedLabel);

    _tuesdaySchedLabel = new QLabel(tr(""), this);
    _tuesdaySchedLabel->setAlignment(Qt::AlignCenter);
    layout->addWidget(_tuesdaySchedLabel);

    _wednesdaySchedLabel = new QLabel(tr(""), this);
    _wednesdaySchedLabel->setAlignment(Qt::AlignCenter);
    layout->addWidget(_wednesdaySchedLabel);

    _thrusdaySchedLabel = new QLabel(tr(""), this);
    _thrusdaySchedLabel->setAlignment(Qt::AlignCenter);
    layout->addWidget(_thrusdaySchedLabel);

    _fridaySchedLabel = new QLabel(tr(""), this);
    _fridaySchedLabel->setAlignment(Qt::AlignCenter);
    layout->addWidget(_fridaySchedLabel);

    _saturdaySchedLabel = new QLabel(tr(""), this);
    _saturdaySchedLabel->setAlignment(Qt::AlignCenter);
    layout->addWidget(_saturdaySchedLabel);

    _sundaySchedLabel = new QLabel(tr(""), this);
    _sundaySchedLabel->setAlignment(Qt::AlignCenter);
    layout->addWidget(_sundaySchedLabel);

    QDialogButtonBox *buttons = new QDialogButtonBox(this);
    buttons->addButton(QDialogButtonBox::Close);

    // connects slots
    connect(buttons->button(QDialogButtonBox::Close),
            &QPushButton::clicked,
            this,
            &QDialog::close);

    layout->addWidget(buttons);

    setMinimumWidth(400);
    setLayout(layout);
}

void DialogShowSchedule::showDialog(const PlaceObj& pl)
{
    setWindowTitle("Schedule for " + pl.getPlaceName());
    if(pl.isValidPlace() && pl.getPlaceHours().size() == 7)
    {
        QVector<DaySchedule> sched = pl.getPlaceHours();
        _mondaySchedLabel->setText(sched[0].getDaySchedule().c_str());
        _tuesdaySchedLabel->setText(sched[1].getDaySchedule().c_str());
        _wednesdaySchedLabel->setText(sched[2].getDaySchedule().c_str());
        _thrusdaySchedLabel->setText(sched[3].getDaySchedule().c_str());
        _fridaySchedLabel->setText(sched[4].getDaySchedule().c_str());
        _saturdaySchedLabel->setText(sched[5].getDaySchedule().c_str());
        _sundaySchedLabel->setText(sched[6].getDaySchedule().c_str());
    }
    else
    {
        _mondaySchedLabel->setText("Monday: Closed all day");
        _tuesdaySchedLabel->setText("Tuesday: Closed all day");
        _wednesdaySchedLabel->setText("Wednesday: Closed all day");
        _thrusdaySchedLabel->setText("Thrusday: Closed all day");
        _fridaySchedLabel->setText("Friday: Closed all day");
        _saturdaySchedLabel->setText("Saturday: Closed all day");
        _sundaySchedLabel->setText("Sunday: Closed all day");
    }
    show();
}