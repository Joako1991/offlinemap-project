// Qt includes
#include <QString>

// STL includes
#include <cassert>
#include <iostream>
#include <sstream>

// Custom includes
#include "TimeSchedule.hpp"

Time24Hours::Time24Hours(int HH, int mm) : _hours(HH), _minutes(mm)
{
    if(_hours < 0 || _hours > 23) throw "Trying to initialize with invalid hours value";
    if(_minutes < 0 || _minutes > 59) throw "Trying to initialize with invalid minutes value";
}

Time24Hours::Time24Hours(const Time24Hours& time) :
    _hours(time._hours),
    _minutes(time._minutes)
{
    if(_hours < 0 || _hours > 23) throw "Trying to initialize with invalid hours value";
    if(_minutes < 0 || _minutes > 59) throw "Trying to initialize with invalid minutes value";
}

Time24Hours& Time24Hours::operator=(const Time24Hours& t)
{
    if(t._hours < 0 || t._hours > 23) throw "Trying to initialize with invalid hours value";
    if(t._minutes < 0 || t._minutes > 59) throw "Trying to initialize with invalid minutes value";
    _hours = t._hours;
    _minutes = t._minutes;
    return *this;
}

void Time24Hours::setHours(int newVal)
{
    if(newVal < 0 || newVal > 23) throw "Trying to set with invalid hours value";
    _hours = newVal;
}

void Time24Hours::setMinutes(int newVal)
{
    if(newVal < 0 || newVal > 59) throw "Trying to set with invalid minutes value";
    _minutes = newVal;
}

std::string Time24Hours::convertToString(int val, int dig) const
{
    std::string str;
    std::stringstream ss;
    ss.fill('0');
    ss.width(dig);
    ss << std::right  << val;
    return ss.str();
}

DaysOfTheWeek DaySchedule::extractDay(std::string str)
{
    /// We pass the day of the week to lower case, to make easier the clasification
    std::string day_str = QString::fromStdString(str).toLower().toStdString();
    if(!day_str.compare("monday"))
    {
        return MONDAY;
    }
    else if(!day_str.compare("tuesday"))
    {
        return TUESDAY;
    }
    else if(!day_str.compare("wednesday"))
    {
        return WEDNESDAY;
    }
    else if(!day_str.compare("thrusday"))
    {
        return THRUSDAY;
    }
    else if(!day_str.compare("friday"))
    {
        return FRIDAY;
    }
    else if(!day_str.compare("saturday"))
    {
        return SATURDAY;
    }
    else
    {
        return SUNDAY;
    }
}

void DaySchedule::extractTimes(std::string str, Time24Hours& openTime, Time24Hours& closeTime)
{
    int sep = str.find("-");
    std::string startTime = str.substr(0, sep);
    std::string endTime = str.substr(sep+1);

    int pos = startTime.find(":");
    openTime.setHours(atoi(startTime.substr(0, pos).c_str()));
    openTime.setMinutes(atoi(startTime.substr(pos+1).c_str()));

    pos = endTime.find(":");
    closeTime.setHours(atoi(endTime.substr(0, pos).c_str()));
    closeTime.setMinutes(atoi(endTime.substr(pos+1).c_str()));
}

DaySchedule::DaySchedule(std::string sched)
{
    if(!sched.empty())
    {
        /// Implementation inspired in http://www.cplusplus.com/reference/string/string/substr/
        int pos = sched.find(":");
        _day = extractDay(sched.substr(0, pos));

        std::string hours_str = QString::fromStdString(sched.substr(pos+1)).toLower().toStdString();
        if(hours_str.find("/") != std::string::npos)
        {
            /// It closes at midday because we have two schedules separated by slash '/'
            _closesMidDay = true;
            _closeAllDay = false;
            Time24Hours morningOpenTime;
            Time24Hours morningCloseTime;
            Time24Hours afternoonOpenTime;
            Time24Hours afternoonCloseTime;

            pos = hours_str.find("/");
            extractTimes(hours_str.substr(0, pos), morningOpenTime, morningCloseTime);
            extractTimes(hours_str.substr(pos+1), afternoonOpenTime, afternoonCloseTime);
            setScheduleWithCloseAtMidday(morningOpenTime,
                morningCloseTime,
                afternoonOpenTime,
                afternoonCloseTime);
        }
        else if(hours_str.find("closed") != std::string::npos)
        {
            /// Closed all day
            _closeAllDay = true;
            _closesMidDay = false;
            _morningStartTime = Time24Hours(0, 0);
            _afternoonEndTime = Time24Hours(0, 0);
            _morningEndTime = Time24Hours(0, 0);
            _afternoonStartTime = Time24Hours(0, 0);
        }
        else
        {
            Time24Hours openTime;
            Time24Hours closeTime;
            extractTimes(hours_str, openTime, closeTime);
            setScheduleWithoutClosureTime(openTime, closeTime);
        }
    }
    else
    {
        std::cerr << "ERROR!! Trying to build with an empty string" << std::endl;
        assert(0);
    }
}

void DaySchedule::setScheduleWithoutClosureTime(Time24Hours startTime, Time24Hours endTime)
{
    _closesMidDay = false;
    _closeAllDay = false;
    _morningStartTime = startTime;
    _morningEndTime = Time24Hours(0, 0);
    _afternoonStartTime = Time24Hours(0, 0);
    _afternoonEndTime = endTime;
}

void DaySchedule::setScheduleWithCloseAtMidday(Time24Hours morningStartTime,
        Time24Hours morningEndTime,
        Time24Hours afternoonStartTime,
        Time24Hours afternoonEndTime)
{
    _closesMidDay = true;
    _closeAllDay = false;
    _morningStartTime = morningStartTime;
    _morningEndTime = morningEndTime;
    _afternoonStartTime = afternoonStartTime;
    _afternoonEndTime = afternoonEndTime;
}

std::string DaySchedule::getDaySchedule() const
{
    std::string str_day;
    std::string hours;
    switch(_day)
    {
        case MONDAY:
            str_day = "Monday";
            break;
        case TUESDAY:
            str_day = "Tuesday";
            break;
        case WEDNESDAY:
            str_day = "Wednesday";
            break;
        case THRUSDAY:
            str_day = "Thrusday";
            break;
        case FRIDAY:
            str_day = "Friday";
            break;
        case SATURDAY:
            str_day = "Saturday";
            break;
        case SUNDAY:
            str_day = "Sunday";
            break;
         default:
            str_day = "Unknown Day";
            break;
    }
    if(_closeAllDay)
    {
        hours = "Closed";
    }
    else
    {
        if(_closesMidDay)
        {
            hours = _morningStartTime.getTime()
                + " - "
                + _morningEndTime.getTime()
                + " / "
                + _afternoonStartTime.getTime()
                + " - "
                + _afternoonEndTime.getTime();
        }
        else
        {
            hours = _morningStartTime.getTime()
                + " - "
                + _afternoonEndTime.getTime();
        }
    }
    return str_day + ": " + hours;
}
