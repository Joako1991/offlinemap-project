#ifndef __DIALOG_SHOW_SCHEDULE_HPP__
#define __DIALOG_SHOW_SCHEDULE_HPP__

/// Qt includes
#include <QDialog>
#include <QLabel>

/// Custom includes
#include <PlaceObj.hpp>

/// @brief This class holds the information of the schedule of a place.
/// The information shown is the one provided by the place in showDialog.
/// If the place is not valid, then the schedule shown is going to be everyday
/// closed.
class DialogShowSchedule : public QDialog
{
    Q_OBJECT
public:
    /// @brief Constructor
    DialogShowSchedule(QDialog *parent = nullptr);
    ~DialogShowSchedule();

    /**
     * @brief Show the dialog with the schedule information
     *
     * @param pl: Place information from which we are going to show the schedule.
     *   If the place is invalid, then an everyday closed schedule is shown.
    */
    void showDialog(const PlaceObj& pl);

private:
    /// @brief Initialize dialog GUI
    void initializeDialog();

    QLabel *_mondaySchedLabel;
    QLabel *_tuesdaySchedLabel;
    QLabel *_wednesdaySchedLabel;
    QLabel *_thrusdaySchedLabel;
    QLabel *_fridaySchedLabel;
    QLabel *_saturdaySchedLabel;
    QLabel *_sundaySchedLabel;
};

#endif // __DIALOG_SHOW_SCHEDULE_HPP__
