#ifndef __SCHEDULE_WIDGET_HPP__
#define __SCHEDULE_WIDGET_HPP__

/// Qt includes
#include <QWidget>

/// Custom includes
#include <ScheduleWidget/DialogShowSchedule.hpp>
#include <PlaceObj.hpp>

/// @brief This class provides the schedule showing feature.
/// It includes a button that is connected to a dialog that will show the
/// place schedule as a dialog with labels and a close button.
/// It will do some checks in the place provided. If the place is not valid,
/// or its name is not in the database, then we show a pop up about the error,
/// and no schedule is shown.
class ScheduleWidget : public QWidget
{
    Q_OBJECT
public:
    /// @brief Constructor
    ScheduleWidget(QWidget *parent = nullptr);
    ~ScheduleWidget();

    /**
     * @brief Update the locally stored place. If the place is not valid,
     *   then this function does anything.
     *
     * @param pl: New place information to be set.
    */
    void setPlace(const PlaceObj& pl);

public slots:
    /// @brief Action to be taken when a reset signal is emitted
    void onReset();

protected slots:
    /**
     * @brief Action to be taken whenever we press the button Show Schedule.
     *   It will check if the stored place is valid and if it is in the database.
     *   If any of these conditions are not met, then a pop up window will appear
     *   explaining the situation
    */
    void onClickShowSchedule(bool checked);

private:
    /// @brief Initialize the widget GUI
    void initializeWidget();

    PlaceObj _localPlace;
    DialogShowSchedule *_dialog;
};

#endif // __SCHEDULE_WIDGET_HPP__