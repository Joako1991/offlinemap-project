#ifndef __DB_QUERIES_HPP__
#define __DB_QUERIES_HPP__

// STL includes
#include <string>

// Custom includes
#include <CommentObj.hpp>
#include <PlaceObj.hpp>
#include <User.hpp>

enum TypeOfQuery
{
    UNKNOWN = 0,
    CREATE_TABLE,
    INSERT,
    UPDATE,
    ERASE,
    COUNT,
    AVERAGE,
    FILTER_PLACES,
    FILTER_USERS,
    FILTER_COMMENTS,
};

class SQLQueries
{
public:
    /// Places queries
    static std::string createTableOfPlaces();
    static std::string getAllPlaces();
    static std::string insertPlace(PlaceObj place);
    static std::string updateAPlace(PlaceObj place);
    static std::string eraseAPlace(const PlaceObj& pl);
    static std::string erasePlacesTable();
    static std::string getAmountOfPlaces();
    static std::string filterPlacesByType(TypeOfPlace type);
    static std::string filterPlacesByName(std::string name);
    static std::string filterPlacesByAddress(std::string addr);
    static std::string filterPlacesByTypeAndName(TypeOfPlace type, std::string name);
    static PlaceObj convertQueryResultToPlace(int, char**, char**);

    /// Users queries
    static std::string createTableOfUsers();
    static std::string eraseUsersTable();
    static std::string getAmountOfUsers();
    static std::string getAllUsers();
    static std::string getUserInfo(std::string username, std::string password);
    static std::string getUserInfoBasedUserName(std::string username);
    static std::string insertUser(User user);
    static std::string updateUserPassword(User user);
    static User convertQueryResultToUser(int, char**, char**);

    /// Comments queries
    static std::string createTableOfComments();
    static std::string getAmountOfComments();
    static std::string insertComment(CommentObj comment);
    static std::string getPlaceComments(std::string placeName);
    static std::string getGlobalPlaceReputation(std::string placeName);
    static CommentObj convertQueryResultToComment(int, char**, char**);

private:
    /// Generic internal functions
    static std::string eraseTable(std::string tableName);
    static std::string getAllElements(std::string id_key, std::string tableName);
    static std::string getAmountOfElements(std::string id_key, std::string tableName);
    SQLQueries() = default;
    ~SQLQueries() = default;
    Q_DISABLE_COPY(SQLQueries)
};

#endif // __DB_QUERIES_HPP__