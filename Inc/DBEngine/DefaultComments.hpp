#ifndef __DEFAULT_COMMENTS_HPP__
#define __DEFAULT_COMMENTS_HPP__

/// Custom includes
#include <CommentObj.hpp>

/**
 * @brief This class is just a hardcoded version of the comments we can
 * have about the default places. It contains just a few comments made by
 * the admin of the database, with a date, reputation, a comment and the user
 * that made them
*/
class DefaultComments
{
public:
    DefaultComments() {}
    ~DefaultComments() = default;
    /**
     * @brief Load the comments in the internal vector. This function should be called
     * before calling getComments in order to get valid data. If not, the vector on that
     * function would be empty.
     *
     * If it is called more than one time, it will restore the vector of comments
     * to its original state.
    */
    void loadComments();

    /**
     * @brief Get a vector of comments. These are the default values hardcoded in code.
    */
    QVector<CommentObj> getComments() const {return _comments;}

private:
    QVector<CommentObj> _comments;
};

#endif // __DEFAULT_COMMENTS_HPP__
