#ifndef __DEFAULT_PLACES_HPP__
#define __DEFAULT_PLACES_HPP__

// Qt includes
#include <QVector>

// Custom includes
#include <PlaceObj.hpp>

/**
 * @brief This class is just a hardcoded version of the places we can
 * show in the map. It contains positions in the map, addresses, name of the place
 * and its type and schedules.
*/
class DefaultPlaces
{
public:
    DefaultPlaces() {}
    ~DefaultPlaces() = default;
    /**
     * @brief Load the places in the internal vector. This function should be called
     * before calling getPlaces in order to get valid data. If not, the vector on that
     * function would be empty.
     *
     * If it is called more than one time, it will restore the vector of places
     * to its original state.
    */
    void loadPlaces();

    /**
     * @brief Get a vector of places. These are the default values hardcoded in code.
    */
    QVector<PlaceObj> getPlaces() const {return _places;}

private:
    QVector<PlaceObj> _places;
};

#endif // __DEFAULT_PLACES_HPP__
