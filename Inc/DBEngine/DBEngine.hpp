#ifndef __DB_ENGINE_HPP__
#define __DB_ENGINE_HPP__

// Qt includes
#include <QObject>

// SQLite includes
#include <sqlite3.h>

// Custom includes
#include <CommentObj.hpp>
#include <DBEngine/DBQueries.hpp>
#include <PlaceObj.hpp>

class User;

/**
 * @brief Singleton class to wrap the functionality of the database
 * according to our needs. If not database file found, default values be loaded.
*/
class DBEngine : public QObject
{
    Q_OBJECT
public:
    /// \brief: Get the instance of this class
    static DBEngine* instance();

    /**
     * @brief Load the database file. If the file cannot be open or is not found
     * in the specified path, default values would be loaded. This database will load
     * information regarding places, comments and users.
     *
     * This function should be called before any other query. If not, no data will be
     * retrieved when calling the getters.
     *
     * @param db_filepath: Absolute or relative path to the database base to be loaded.
     *
     * @returns True if the database could be loaded, or false otherwise
    */
    bool loadDatabase(QString db_filepath);

    /**
     * @brief Get a vector with all the places in the database.
     *
     * @returns Vector of the places.
    */
    QVector<PlaceObj> getListOfPlaces();

    /**
     * @brief Function to be called each time a new SQLite exec result is ready.
     * This function is aimed to be used internally, but due to
     * SQLite needs a callback to be provided, and static function should be able to
     * access to this object, and the only way to do it is this one being public.
     *
     * @param cols: Amount of columns from the query result
     * @param colName: Array with the name of the columns
     * @param colVal: Array with the values for each column
    */
    void resultsOfQuery(int cols, char** colName, char** colVal);

    /**
     * @brief Check if the places table in the database is empty or not.
     *
     * @returns true if it is empty, false otherwise
    */
    bool isPlacesDatabaseEmpty();

    /**
     * @brief Filter the places based on type.
     *
     * @param type: type of place
     *
     * @returns Vector of places filtered by type.
    */
    QVector<PlaceObj> getPlacesBasedType(TypeOfPlace type);

    /**
     * @brief Filter the places based on name.
     *
     * @param name: name of place
     *
     * @returns Vector of places filtered by name.
    */
    QVector<PlaceObj> getPlacesBasedName(std::string name);

    /**
     * @brief Filter the places based on address.
     *
     * @param addr: address of place
     *
     * @returns Vector of places filtered by address.
    */
    QVector<PlaceObj> getPlacesBasedAddress(std::string addr);

    /**
     * @brief Filter the places based on type and name. If the type
     *   is UNKNOWN, ALL_TYPES or AMOUNT_OF_TYPES, then we only filter
     *   by name
     *
     * @param type: type of place
     * @param name: name of place
     *
     * @returns Vector of places filtered by type and name.
    */
    QVector<PlaceObj> getPlacesBasedTypeAndName(TypeOfPlace type, std::string name);

    /**
     * @brief Check if the users table in the database is empty or not.
     *
     * @returns true if it is empty, false otherwise
    */
    bool isUsersDatabaseEmpty();

    /**
     * @brief Get the information of all the users that matches with the
     *   provided username and password.
     *
     * @param username: String of the username.
     * @param password: String of the password.
     *
     * @returns Vector of all the users that has the same password and username
     *   than the ones provided
    */
    std::vector<User> getUserInfoValues(std::string username, std::string password);

    /**
     * @brief Get the information of all the users that matches with the
     *   provided username only.
     *
     * @param username: String of the username.
     *
     * @returns Vector of all the users that has the same username
     *   than the one provided
    */
    std::vector<User> getUserInfoValuesBasedUserName(std::string username);

    /**
     * @brief Insert a new user into the database
     *
     * @param newUser: User class object containing all the information about
     *   the new user.
     *
     * @returns Boolean. True if the user has been inserted. False otherwise
     *   (An user cannot be inserted if the username or the email are already
     *   in another database entry for instance)
    */
    bool insertUserInfo(const User& newUser);

    /**
     * @brief Update the user password of the username provided.
     *
     * @param user: User class object containing at least the username and the password
     *   that we want to update in the database.
     *
     * @returns Boolean. True if the user has been updated. False otherwise.
    */
    bool updateUserPassword(const User &user);

    /**
     * @brief Add a new place to the database.
     *
     * @param newPlace: New place to be inserted into the database, with all
     *   the fields containing valid information
     *
     * @returns: Boolean. True if the place was inserted
    */
    bool insertNewPlace(const PlaceObj& newPlace);

    /**
     * @brief Erase a given place from the database. In order to do this,
     *   you need to provide a place with a valid ID. This ID should be the
     *   corresponding ID set in the DB for the place we want to erase.
     *   In order to get a right value, the proper manner to use this function
     *   is to make a query to get the place we want to erase, and that will
     *   include the right ID, so we can erase it after the query.
     *
     * @param pl: Place we want to erase. It has to have a valid ID in order
     *   to erase the proper entry from the DB.
     *
     * @returns Boolean. True if the query was executed without any problem
    */
    bool eraseAPlaceFromDatabase(const PlaceObj& pl);

    /**
     * @brief Insert a comment in the table of comments in the database
     *
     * @param newComment: Valid CommentObj instance with the new comment to be inserted
     *
     * @returns: Boolean. True if the comment has been successfully inserted in the database
    */
    bool insertNewComment(const CommentObj& newComment);

    /**
     * @brief Get the average reputation of a place. It will compute
     *   the average of the Reputation column of the given place.
     *
     * @param placeName: String with the name of the place from which we want
     *   to compute the reputation.
     *
     * @param reputation (output): Computed reputation
     *
     * @returns Boolean. False if the placeName does not exists in the database or the
     *   query for this placeName cannot be executed
    */
    bool getPlaceAverageReputation(std::string placeName, double &reputation);

    /**
     * @brief Query to the database for all the comments about certain place
     *
     * @param placeName. String with the place name we want to know about.
     * @param comments (output): vector with the retrieved comments.
     *
     * @returns Boolean: True if the place exists in the database and the query
     *   could have been accomplish successfully
    */
    bool getPlaceComments(std::string placeName, std::vector<CommentObj> &comments);

    /**
     * @brief Check if the comments database has any entry
     *
     * @return True if the comments table is empty
    */
    bool isCommentsDatabaseEmpty();

private:
    /// We disable the copy of objects and we define contructors
    /// and destructors to be private
    DBEngine();
    ~DBEngine();
    Q_DISABLE_COPY(DBEngine)

    /// @brief Load default places hardcoded in the code, and write them into the database.
    void loadDefaultDatabase();

    /// @brief Load default admin user hardcoded in the code, and write it in the database
    void loadUsersDefaultDatabase();

    /// @brief Load default places comments hardcoded in the code, and write it in the database
    void loadDefaultComments();

    /// @brief Close the database
    void closeDatabase();

    /**
     * @brief Wrapping function to make the SQL query through the exec function.
     * We have different type of queries, and some of them require a callback function
     * and some of them don't.
     * This function will block until the query is finished and the results served
     *
     * @param type: Type of query
     * @param query: String of an SQL statement to be executed by the database
     * @returns Boolean. True if no error happened when executing the SQLite query
    */
    bool evaluateSQLStatement(TypeOfQuery type, std::string query);

    /**
     * @brief This function is useful to know if we want to insert a comment to a place
     *   that does not exist in the database.
     *
     * @param placeName: String with the name of the place we want to check
     *
     * @returns Boolean: True if the place exists in the database
    */
    bool checkIfPlaceExists(std::string placeName);

    QString _filepath;
    QVector<PlaceObj> _lastPlacesQuery;
    std::vector<User> _lastUserQuery;
    std::vector<CommentObj> _lastCommentsQuery;
    int _lastCountQueryValue;
    double _lastAverageValue;
    static DBEngine* _instance;

    sqlite3* _db;
    bool _isOpened;
    TypeOfQuery _currentQueryType;
};

#endif // __DB_ENGINE_HPP__