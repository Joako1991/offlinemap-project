#ifndef ROUTE_HPP
#define ROUTE_HPP

#include <QObject>

/// @brief Class to represent a point in the map. It includes Latitude and
/// Longitude information on it as double numbers. It is a QObject class, then
/// declared as Metatype because we are going to use this type of data in QML.
/// This is the requirement in order to be able to access to the fields
class PointInMap : public QObject
{
    Q_GADGET
    Q_PROPERTY(double lat READ getLat)
    Q_PROPERTY(double lng READ getLong)

public:
    /// @brief Default constructor. It initializes the latitude and longitude in zero
    PointInMap(QObject *parent = nullptr) : QObject(parent), _lat(0), _lng(0) {}

    /*
     * @brief Copy constructor. It initializes the latitude and longitude with
     *  the analogous values from the provided point
    */
    PointInMap(const PointInMap& p, QObject *parent = nullptr) :
        QObject(parent),
        _lat(p.getLat()),
        _lng(p.getLong()) {}

    /**
     * @brief Contructor based on latitude and longitude provided
    */
    PointInMap(double lat, double lng, QObject *parent = nullptr) :
        QObject(parent),
        _lat(lat),
        _lng(lng) {}

    ~PointInMap() = default;

    /// Mutators
    double getLat() const {return _lat;}
    double getLong() const {return _lng;}

    /**
     * @brief Copy operator. It will copy the latitude and longitude of a provided point
    */
    PointInMap& operator=(const PointInMap& p) { _lat =p.getLat(); _lng = p.getLong(); return *this;}

private:
    double _lat;
    double _lng;
};

Q_DECLARE_METATYPE(PointInMap);

class Route
{
public:
    /**
     * @brief Constructor
     *
     * @param start_lat: Latitude of the starting point of the Route
     * @param start_lng: Longitude of the starting point of the Route
     * @param end_lat: Latitude of the target point of the Route
     * @param end_lng: Longitude of the target point of the Route
    */
    Route(double start_lat, double start_lng, double end_lat, double end_lng) :
        _start(start_lat, start_lng),
        _end(end_lat, end_lng) {}

    /**
     * @brief Get the starting point of the route
     *
     * @returns PointInMap instance with the latitude and longitude of the starting point
    */
    PointInMap start() const{return _start;}

    /**
     * @brief Get the target point of the route
     *
     * @returns PointInMap instance with the latitude and longitude of the target point
    */
    PointInMap end() const{return _end;}

private:
    PointInMap _start;
    PointInMap _end;
};

#endif // ROUTE_H
