#ifndef __DIALOG_NEW_COMMENT_HPP__
#define __DIALOG_NEW_COMMENT_HPP__

/// Qt includes
#include <QDialog>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QTextEdit>

/// Custom includes
#include <PlaceObj.hpp>
#include <User.hpp>

/// @brief Dialog suitable to insert a new comment into the database.
/// The comment should be provided by a valid user, and about a valid place.
/// That is really important. If any of those conditions are not met, an assertion
/// will be thrown.
/// The comment dialog has 2 editable fields: Reputation and comment. Date, place
/// and user are not editable fields. The date is taken from the current date of
/// the machine
class DialogNewComment : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param user: Valid user that is going to do the comments
    */
    DialogNewComment(const User& user, QDialog *parent = nullptr);
    ~DialogNewComment();

    /**
     * @brief Set a new user information
     *
     * @param newUser: New user to be set. It should be a valid user. If not,
     *   the set won't have any effect.
    */
    void setUserInformation(const User& newUser);

    /**
     * @brief Update the place information.
     *
     * @param pl: New place to be stored in the class. It should be a valid place,
     *   if not, this set won't have any effect.
    */
    void setPlaceInformation(const PlaceObj& pl);

    /**
     * @brief Show this dialog. It only will be shown if a valid place has
     *   been set before hand. If not valid place is available, then a pop
     *   up window will appear explaining the situation
    */
    void showDialog();

    /// @brief Clear stored user and place information
    void reset();

protected slots:
    /**
     * @brief Slot that is going to be called whenever the Submit button is pressed.
     *   It will read the entered fields, and the place and user information, generate
     *   a comment instance, and store it into the database.
    */
    void onNewCommentSubmitted(bool checked);

private:
    /// @brief Initialize the GUI of this dialog
    void initializeDialog();

    PlaceObj _localPlace;
    User _localUser;
    QLabel *_placeName;
    QLabel *_userLabel;
    QLabel *_dayOfComment;
    QTextEdit *_editComment;
    QDoubleSpinBox *_editReputation;
};

#endif // __DIALOG_NEW_COMMENT_HPP__