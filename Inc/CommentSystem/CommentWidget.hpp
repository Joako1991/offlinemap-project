#ifndef __COMMENT_WIDGET_HPP__
#define __COMMENT_WIDGET_HPP__

/// Qt includes
#include <QLabel>
#include <QWidget>

/// Custom includes
#include <CommentObj.hpp>

/// @brief This class contains labels that will show the information
/// stored in the CommentObj contained in this class.
/// This widget will show the user that commented, the comment itself,
/// the reputation given and the day the comment has been done
class CommentWidget : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param cm: CommentObj instance with a valid comment. If the comment
     *  is invalid, and assertion is going to be thrown.
    */
    CommentWidget(CommentObj cm, QWidget *parent = nullptr);
    ~CommentWidget();

    /**
     * @brief Set a new CommentObj instance. The comment is going to be updated
     *    only if it is valid. If the comment is valid, the labels are going
     *    to be updated too.
     *
     * @param cm: New comment object to be set
    */
    void setCommentInformation(const CommentObj& cm);

private:
    /**
     * @brief Update the label values. It will only read the values in the
     *   local comment variable and update the labels with those values.
     *   Useful when we set the comment information with a new variable
    */
    void updateLabels();

    /// @brief Initialize the widget GUI
    void initializeWidget();

    CommentObj _localComment;
    QLabel *_user;
    QLabel *_comment;
    QLabel *_reputation;
    QLabel *_day;
};

#endif // __COMMENT_WIDGET_HPP__