#ifndef __DIALOG_SHOW_COMMENTS_HPP__
#define __DIALOG_SHOW_COMMENTS_HPP__

/// Qt includes
#include <QDialog>
#include <QLabel>
#include <QListWidget>

/// Custom includes
#include <PlaceObj.hpp>

/// @brief Show a dialog with the comments about a given place.
/// It will set up a QDialog with a label that tells you which place
/// are we showing the comments, and a list of the comments as they
/// come from the database. It stores of a place, so we can ensure it is
/// valid, and that we have all the information we might need in order to find
/// the right place in the database.
class DialogShowComments : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
    */
    DialogShowComments(QDialog *parent = nullptr);
    ~DialogShowComments();

    /**
     * @brief Receive a new place and update the local copy of the place we have.
     *   This information is used to show the label with the place from which we
     *   are showing the comments, and to look into the database the place.
    */
    void setPlaceInformation(const PlaceObj& pl);

    /**
     * @brief Show the dialog with the comments about the stored place.
     *   If the place set is not valid (place never set or it has invalid information),
     *   a pop up will be shown about this situation and the window won't be shown.
    */
    void showDialog();

    /// @brief Clear stored place information
    void reset();

private:
    /**
     * @brief Initialize the widget GUI
    */
    void initializeDialog();

    /**
     * @brief Retrieve the comments from the database about the place, and
     *   fill the internal widget list with their information.
    */
    void fillListWithComments();

    PlaceObj _localPlace;
    QListWidget *_listOfComments;
    QLabel *_placeName;
};

#endif // __DIALOG_SHOW_COMMENTS_HPP__