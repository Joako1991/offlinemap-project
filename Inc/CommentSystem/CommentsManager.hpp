#ifndef __COMMENTS_MANAGER_WIDGET_HPP__
#define __COMMENTS_MANAGER_WIDGET_HPP__

/// Qt includes
#include <QWidget>

/// Custom includes
#include <CommentSystem/DialogNewComment.hpp>
#include <CommentSystem/DialogShowComments.hpp>
#include <PlaceObj.hpp>

/// @brief Class that will manager all the tasks related to the Comments System
/// It contains two buttons, one to add a new comment and another one to see
/// the comments about a place.
class CommentsManager : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param user: Valid user that is going to do the comments
    */
    CommentsManager(const User &user, QWidget *parent = nullptr);
    ~CommentsManager();

public slots:
    /**
     * @brief Slot that will update the Place stored in the show comments class.
     *   If this method is never called, then the list of comments cannot be
     *   seen.
     *
     * @param pl: New place to update the internal variable of show comments
    */
    void onNewPlace(const PlaceObj& pl);

    /// @brief Clear internal information about users and places
    void onReset();

private:
    /**
     * @brief Initialize widget GUI
     *
     * @param user: Valid user that is going to do the comments.
     *   It is used for the dialog of new comments
    */
    void initializeWidget(const User &user);

    /// @brief Action to be taken whenever the Show comments button is pressed
    void onShowComments();

    /// @brief Action executed when the user presses the New comments button
    void onNewComment();

    DialogShowComments *_commentsDialog;
    DialogNewComment *_newCommentDialog;
};

#endif // __COMMENTS_MANAGER_WIDGET_HPP__
