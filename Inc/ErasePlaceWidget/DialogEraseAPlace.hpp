#ifndef __DIALOG_ERASE_A_PLACE_HPP__
#define __DIALOG_ERASE_A_PLACE_HPP__

/// Qt includes
#include <QDialog>
#include <QVBoxLayout>
#include <QListWidget>

/// Custom includes
#include <PlaceWidget.hpp>

/// @brief Class created to show a QDialog that will make
/// easy to erase a place. It includes a list of all the places
/// present in the database through a QListWidget.
/// It also contains a QMessageWindow that will pop up asking
/// for a confirmation to the user that we want to erase certain place
class DialogEraseAPlace : public QDialog
{
    Q_OBJECT
public:
    /// @brief Constructor
    explicit DialogEraseAPlace(QDialog *parent = nullptr);
    ~DialogEraseAPlace();

public slots:
    /**
     * @brief Slot that will clear all the elements in the list
     *  of places in the database, which could be out-of-date, and
     *  then it adds the places into the list querying the database
     *  for that.
    */
    void fillListWithPlaces();

    /**
     * @brief Configure the GUI and connect the proper signals and
     *  slots with the QListWidget and its items, and the QMessageBox
    */
    void initializeDialog();

    /**
     * @brief Show the QDialog main window.
    */
    void showWindow(bool checked);

    /**
     * @brief Slot called whenever we click an item from the list of
     *  places.
     *
     * @param placeToErase: Information of the place clicked
    */
    void onPlaceClicked(PlaceObj placeToErase);

private:
    QListWidget *_listOfPlaces;
    QVBoxLayout *_layout;
};
#endif // __DIALOG_ERASE_A_PLACE_HPP__
