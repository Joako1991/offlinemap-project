#ifndef __ERASE_PLACE_WIDGET_HPP__
#define __ERASE_PLACE_WIDGET_HPP__

/// Qt layout
#include <QHBoxLayout>

/// Custom includes
#include <ErasePlaceWidget/DialogEraseAPlace.hpp>

/// @brief Class that includes the erase places system.
/// It includes a button and a custom QDialog. When the button
/// is pressed, a QDialog will appear with all the places included
/// in the database. The procedure to erase a place is to clic any place
/// confirm its deletion clicking in a pop up window made for that.
class ErasePlaceWidget : public QWidget
{
    Q_OBJECT
public:
    /// @brief Constructor
    explicit ErasePlaceWidget(QWidget *paren = nullptr);
    ~ErasePlaceWidget();

private:
    /**
     * @brief Initialize the GUI of this widget and it will also
     *  connect the proper signals of the Push button and the Erase dialog
    */
    void initializeWidget();

    QHBoxLayout *_layout;
    DialogEraseAPlace *_dialog;
};

#endif // __ERASE_PLACE_WIDGET_HPP__
