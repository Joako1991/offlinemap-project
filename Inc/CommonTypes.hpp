#ifndef __COMMON_TYPES_HPP__
#define __COMMON_TYPES_HPP__

/// Qt includes
#include <QMap>

enum TypeOfPlace
{
    UNKNOWN_TYPE = 0,
    RESTAURANT_TYPE,
    FASTFOOD_TYPE,
    CAFE_TYPE,
    PHARMACIE_TYPE,
    SCHOOL_TYPE,
    UNIVERSITY_TYPE,
    SUPERMARKET_TYPE,
    BAKERY_TYPE,
    HOTEL_TYPE,
    RESIDENCE_TYPE,
    PARK_TYPE,
    CULTURAL_TYPE,
    HAIRDRESSER_TYPE,
    FLOWER_TYPE,
    CLOTHES_TYPE,
    POST_TYPE,
    ALL_TYPES,
    AMOUNT_OF_TYPES
};

enum MeansOfTransport
{
    ON_FOOT = 0,
    BY_BIKE,
    BY_CAR,
};

struct MeansOfTransportMap
{
    MeansOfTransportMap()
    {
        mapToEnum[ON_FOOT] = "foot";
        mapToEnum[BY_BIKE] = "bicycle";
        mapToEnum[BY_CAR] = "car";
    }
    QMap<MeansOfTransport, QString> mapToEnum;
};

struct SearchParams
{
    SearchParams() :
        isMapClicked(false),
        radiousDistance(0),
        latitude(0),
        longitude(0),
        placeType(UNKNOWN_TYPE),
        enteredText("")
    {}
    bool isMapClicked;
    int radiousDistance;
    double latitude;
    double longitude;
    TypeOfPlace placeType;
    std::string enteredText;
};

struct PlaceTypeMap
{
    PlaceTypeMap()
    {
        mapToEnum["Restaurant"] = RESTAURANT_TYPE;
        mapToEnum["Fast food"] = FASTFOOD_TYPE;
        mapToEnum["Cafe"] = CAFE_TYPE;
        mapToEnum["Pharmacie"] = PHARMACIE_TYPE;
        mapToEnum["School"] = SCHOOL_TYPE;
        mapToEnum["University"] = UNIVERSITY_TYPE;
        mapToEnum["Supermarket"] = SUPERMARKET_TYPE;
        mapToEnum["Bakery"] = BAKERY_TYPE;
        mapToEnum["Hotel"] = HOTEL_TYPE;
        mapToEnum["Residence"] = RESIDENCE_TYPE;
        mapToEnum["Park"] = PARK_TYPE;
        mapToEnum["Cultural place"] = CULTURAL_TYPE;
        mapToEnum["Hairdresser"] = HAIRDRESSER_TYPE;
        mapToEnum["Flower shop"] = FLOWER_TYPE;
        mapToEnum["Clothes shop"] = CLOTHES_TYPE;
        mapToEnum["Post Office"] = POST_TYPE;
        mapToEnum["All Types"] = ALL_TYPES;
    }
    QMap<QString, TypeOfPlace> mapToEnum;
};

enum PossibleSearchCase {
  CLEAR_RESULTS = 0,
  SHOW_ALL_PLACES,
  SEARCH_EXACT_PLACE,
  SEARCH_NEARBY,
};

enum LoginStates
{
    MAIN_WINDOW = 0,
    LOGIN_WINDOW,
    FORGET_PASSWORD_WINDOW,
    RESET_PASSWORD,
    SIGNUP_WINDOW,
    LOGGING_SUCCESS,
    EXIT,
    NUM_OF_WINDOWS,
};

enum TypeOfSchedule
{
    CLOSED_ALL_DAY = 0,
    CLOSES_AT_MIDDAY,
    ONLY_ONE_SCHEDULE,
};

enum DaysOfTheWeek
{
    MONDAY = 0,
    TUESDAY,
    WEDNESDAY,
    THRUSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY,
    AMOUNT_OF_DAYS,
};

#endif // __COMMON_TYPES_HPP__
