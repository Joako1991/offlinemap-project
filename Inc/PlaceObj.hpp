#ifndef __PLACE_HPP__
#define __PLACE_HPP__

/// Qt includes
#include <QString>
#include <QVector>

/// Custom includes
#include <TimeSchedule.hpp>
#include <CommonTypes.hpp>

class PlaceObj
{
public:
    PlaceObj();

    /**
     * @brief Constructor
     *
     * @param addr: String with the address of the place
     * @param name: String with the name of the place.
     * @param lat: Double number: Latitude of the location of the place
     * @param lng: Double number: Longitude of the location of the place
     * @param type: Type of place (given by one of the values of the enum TypeOfPlace)
    */
    PlaceObj(int id, QString addr, QString name, double lat, double lgn, TypeOfPlace type);

    /**
     * @brief Get the ID place. This identifier should be unique for each place.
     *
     * @returns Integer. ID of the place
    */
    int getPlaceId() const {return _placeId;}

    /// Mutators
    QString getAddress() const {return _address;}
    QString getPlaceName() const {return _placeName;}
    double getLat() const {return _latitude;}
    double getLong() const {return _longitude;}
    TypeOfPlace getPlaceType() const {return _placeType;}
    QVector<DaySchedule> getPlaceHours() const {return _hoursOpened;}

    /**
     * @brief Add one day schedule. Several calls to this function with the same day
     * won't generate several inputs to the array. For each day, only the newest schedule
     * is kept.
     *
     * @param sched: Day schedule to be added (or updated)
    */
    void addDayToSchedule(DaySchedule sched) {_hoursOpened[sched.getDay()] = sched;}

    /// @brief Clear the stored place information
    void clear();

    /**
     * @brief Check if the stored place information has a valid Latitude
     *  and Longitude
     *
     * @return Boolean. True if the latitude and longitude are not zero
    */
    bool isValidPlace() const {return ((_longitude != 0) && (_latitude != 0));}

private:
    int _placeId;
    QString _address;
    QString _placeName;
    double _latitude;
    double _longitude;
    TypeOfPlace _placeType;
    QVector<DaySchedule> _hoursOpened;
};

#endif // __PLACES_HPP__