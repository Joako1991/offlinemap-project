#ifndef PLACEWIDGET_HPP
#define PLACEWIDGET_HPP

/// Qt includes
#include <QLabel>
#include <QMouseEvent>

/// Custom includes
#include <PlaceObj.hpp>

/// @brief Class to show all the information regarding a place.
/// It will receive a place in the contructor (that later on can
/// be changed with a setter) and the information stored in that
/// object will be shown using labels in an organized widget.
/// It includes a signal that will be emitted once the QWidget
/// is clicked, and with that signal, we will have the place information
/// available
class PlaceWidget : public QWidget
{
    Q_OBJECT
public:
    PlaceWidget(PlaceObj, QWidget *parent = nullptr);
    ~PlaceWidget();

    /**
     * @brief set all variable of place in place widget.
     * @param Place: Place object
    */
    void setPlace(PlaceObj);

    /**
     * @brief Get the stored place in this widget
     *
     * @returns Place class object with the latest place set
     *   into this class
    */
    PlaceObj getPlaceInfo() const { return _place;}

signals:
    /**
     * @brief Signal emitted anytime this widget is clicked
     *
     * @param myPlace: Place information stored in this class
    */
    void widgetTapped(PlaceObj myPlace);

protected:
    /**
     * @brief Overrided slot triggered anytime we click into this widget
    */
    void mousePressEvent(QMouseEvent *event) override;

private:
    /**
     * @brief initialize Place widget and set the QGridLayout values.
    */
    void initPlaceWidget();

    PlaceObj _place;
    QLabel *_placeName;
    QLabel *_placeadd;
    QLabel *_placeType;
    QLabel *_placelat;
    QLabel *_placelon;
    QLabel *_placeReputation;
};
#endif // PLACEWIDGET_HPP
