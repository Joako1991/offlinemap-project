#ifndef __COMMENT_OBJ_HPP__
#define __COMMENT_OBJ_HPP__

/// Qt includes
#include <QDate>

/// STL includes
#include <string>

/// @brief Class that will store all the information needed to conform a comment
/// about a place. It considers the reputation, the comment itself, the date when
/// the comment has been done, the place which we are doing a comment about, and
/// the user that made that comment.
class CommentObj
{
public:
    /**
     * @brief Constructor
     *
     * @param comment: String with the comment about the place
     * @param placeName: String with the place we want to write about
     * @param user: String with the username that made that comment
     * @param reputation: Assigned reputation to this comment. It should be a float
     *   number between 0 and 5
     * @param when: QDate of when the comment has been done
    */
    CommentObj(std::string comment, std::string placeName, std::string user, float reputation, QDate when) :
        _comment(comment),
        _place(placeName),
        _username(user),
        _reputation(reputation),
        _commentDay(when) {}

    /// Mutators
    std::string getUsername() const {return _username;}
    std::string getComment() const {return _comment;}
    float getReputation() const {return _reputation;}
    std::string getPlaceName() const {return _place;}
    QDate getCommentDate() const {return _commentDay;}

    void setUsername(std::string user) {_username = user;}
    void setComment(std::string comment) {_comment = comment;}
    void setReputation(float rep) {_reputation = rep;}
    void setPlaceName(std::string newName) {_place = newName;}

    /**
     * @brief Check if the stored comment is valid.
     *
     * @returns Boolean. True only if the strings are not empty, the reputation
     *   is between 0 and 5, and the date is valid
    */
    bool isCommentValid() const {return
        !_comment.empty() &&
        !_place.empty() &&
        !_username.empty() &&
        _reputation >= 0.0 && _reputation <= 5.0 &&
        _commentDay.isValid();}

private:
    std::string _comment;
    std::string _place;
    std::string _username;
    float _reputation;
    QDate _commentDay;
};

#endif // __COMMENT_OBJ_HPP__
