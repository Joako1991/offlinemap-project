#ifndef __DAY_SCHEDULE_GROUP_HPP__
#define __DAY_SCHEDULE_GROUP_HPP__

/// Qt includes
#include <QGridLayout>
#include <QLabel>
#include <QTimeEdit>
#include <QWidget>

/// Custom includes
#include <CommonTypes.hpp>
#include <TimeSchedule.hpp>

/// @brief This class creates a widget that includes all the elements
/// needed to define the schedule of a given day. It includes 3 radio buttons
/// to define the type of schedule we want to define, and depending of what
/// we choose, it will show different QTimeEdit elements to introduce the
/// time value.
/// Then we have a function that will retrieve all the information given by the user,
/// in order to get finally a DaySchedule class instance.
class DayScheduleGroup : public QWidget
{
public:
    /**
     * @brief Constructor
     *
     * @param day: Day of the week that this class is going to represent
    */
    explicit DayScheduleGroup(DaysOfTheWeek day, QWidget *parent = nullptr);
    ~DayScheduleGroup();

    /**
     * @brief Check if the data entered by the user is valid or not.
     *   An schedule is valid if:
     *      -) If it is closed all day
     *      -) If it does not close at midday, then the closing time should
     *         be greater than the starting time
     *      -) If it closes at midday, then the closing time at the morning
     *         should be greater than the opening time, the time at which the
     *         place closes at the afternoon should be bigger than the one the
     *         place opens in the afternoon, and the time the place opens in the
     *         afternoon should be bigger than the value that the place closes in
     *         the morning.
     *
     * @returns Boolean. True if the actual schedule is valid
    */
    bool isValid();

    /**
     * @brief Get the equivalent day schedule to the input given by the user
     *
     * @returns Already initialized DaySchedule, filled with the information
     *  entered by the user
    */
    DaySchedule getSchedule();

protected slots:
    /**
     * @brief Slot executed whenever the user selects the option that
     *  the place do not close at midday.
    */
    void onShowMorningTime(bool checked);

    /**
     * @brief Slot executed whenever the user selects that the place
     *  closes at the midday
    */
    void onShowMorningAndAfternoonTime(bool checked);

    /**
     * @brief Slot executed whenever the user selects that the place
     *  is closed all day
    */
    void onHideEverything(bool checked);

private:
    /// @brief Initialization function to set up all the widget GUI
    void initializeGroup();

    /**
     * @brief Creates a new line to enter an schedule with opening
     *  and closure time.
     *
     * @param layout: Layout where to attach the new line.
     * @param row: Which row of the Layout we are going to add the line.
     * @param start: QTimeEdit pointer that is going to serve as the opening time.
     * @param end: QTimeEdit pointer that is going to serve as the closing time.
     * @param label: Label object that will serve to identify the line.
     *  In this case, if it is morning or afternoon. The text is not set in this function.
    */
    void addHoursLine(QGridLayout *layout,
        int row,
        QTimeEdit *&start,
        QTimeEdit *&end,
        QLabel *&label);

    int _rowCounter;
    QTimeEdit *_morningStartTime;
    QTimeEdit *_morningEndTime;
    QTimeEdit *_afternoonStartTime;
    QTimeEdit *_afternoonEndTime;
    QGridLayout *_layout;
    QLabel *_morningLabel;
    QLabel *_afternoonLabel;

    TypeOfSchedule _schedType;
    DaysOfTheWeek _day;
};

#endif // __DAY_SCHEDULE_GROUP_HPP__
