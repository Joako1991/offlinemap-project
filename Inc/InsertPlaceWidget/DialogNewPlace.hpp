#ifndef __DIALOG_NEW_PLACE_HPP__
#define __DIALOG_NEW_PLACE_HPP__

/// Qt includes
#include <QComboBox>
#include <QDialog>
#include <QGridLayout>
#include <QGroupBox>
#include <QLineEdit>
#include <QEventLoop>

/// Custom includes
#include <CommonTypes.hpp>
#include "InsertPlaceWidget/DayScheduleGroup.hpp"

/// @brief Dialog that will insert a new place into the database.
/// It contains all the fields that are required to define a place,
/// like address, name, latitude and longitude. It also contains the
/// possibility to get the lat and long clicking from the map, and
/// it includes two other buttons. One for cancel the operation,
/// and the other one to validate and submit the changes if no
/// error happens. If any field is incomplete or it has invalid
/// data, then a pop up will appear saying what is happening.
/// This Dialog is set to modal, so once it appears, all the other
/// widgets cannot be accessed
class DialogNewPlace : public QDialog
{
    Q_OBJECT
public:
    /// Constructor
    explicit DialogNewPlace(QDialog *parent = nullptr);
    ~DialogNewPlace();

public slots:
    /// @brief Slot that should be called in order to start the window.
    void showWindow(bool checked);

    /**
     * @brief Slot that should be connected to the map, in order
     *   to receive the lat and long of a point when the user clicks
     *   over the map. This function will work when the user is entering
     *   the place data and he clicks the button that it want to chose the
     *   location from a map position.
     *
     * @param lat: Latitude from the point clicked
     * @param lng: Longitude from the point clicked
    */
    void onMapTapped(double lat, double lng);

protected slots:
    /**
     * @brief: Slot that is going to be called once the user clicks the
     *   button submit. In this slot, a process of data validation happens.
     *   If everything is ok, then the actual values in the different
     *   fields are read and written into the database. If there is any
     *   error, then a pop up window will appear, with the information
     *   about the error
    */
    void onClickSubmitButton(bool checked);

    /**
     * @brief Slot that is going to be called whenever the user presses
     *   the cancel button. This will close the modal and return to the MainWindow
    */
    void onClickCancelButton(bool checked);

    /**
     * @brief Slot that is going to be executed whenever the user clicks
     *   on the button Select point from the map. It will hide the modal window,
     *   allow the click on the map, and as soon as a click happens, then the modal
     *   will read those values and copy them into the right fields for the place.
    */
    void onClickSelectFromMap(bool checked);

private:

    /// @brief Set up the GUI of the modal dialog
    void initializeDialog();

    /**
     * @brief Create a simple field in the modal dialog, based on a label
     *   and a QLineEdit.
     *
     * @param layout: Layout where to attach the new field.
     * @param title: Label that identifies the field we are going to create
     * @param placeHolder: Letters that are going to appear in the line edit
     *   place whenever the QLineEdit is empty
     * @param row: Which row of the Layout we are going to add the field.
     *
     * @returns: Point to the QLineEdit created for this field.
    */
    QLineEdit *createField(QGridLayout *layout, QString title, QString placeHolder, int row);

    /**
     * @brief Create a field of places types in a shape of a list, and not a
     *   field that the user can write information. We provide a set of options and
     *   the user has to choose between them.
     *
     * @param layout: Layout where to attach the new field.
     * @param title: Label that identifies the field we are going to create
     * @param row: Which row of the Layout we are going to add the field.
    */
    void createTypeOption(QGridLayout *layout, QString title, int row);

    /**
     * @brief Create one field for one day of the week in order to set up its
     *   opening and closure schedule.
     *
     * @param layout: Layout where to attach the new field.
     * @param title: Label that identifies the field we are going to create.
     *   In this case, the day of the week
     * @param row: Which row of the Layout we are going to add the field.
     *
     * @returns A point to the DayScheduleGroup that has been created
    */
    DayScheduleGroup* createDayScheduleField(QGridLayout *layout,
        QString title,
        DaysOfTheWeek day,
        int row);

    int _rowCounter;
    QLineEdit *_editAddress;
    QLineEdit *_editPlaceName;
    QLineEdit *_editLatitude;
    QLineEdit *_editLongitude;
    QComboBox *_typeOptionList;
    QEventLoop _loop;

    DayScheduleGroup *_mondaySched;
    DayScheduleGroup *_tuesdaySched;
    DayScheduleGroup *_wednesdaySched;
    DayScheduleGroup *_thrusdaySched;
    DayScheduleGroup *_fridaySched;
    DayScheduleGroup *_saturdaySched;
    DayScheduleGroup *_sundaySched;
};

#endif // __DIALOG_NEW_PLACE_HPP__
