#ifndef __INSERT_PLACE_WIDGET_HPP__
#define __INSERT_PLACE_WIDGET_HPP__

/// Qt includes
#include <QWidget>
#include <QHBoxLayout>
#include "InsertPlaceWidget/DialogNewPlace.hpp"

class InsertPlaceWidget : public QWidget
{
    Q_OBJECT
public:
    InsertPlaceWidget(QWidget *parent = nullptr);
    ~InsertPlaceWidget();

public slots:
    void mapInformationReceived(double lat, double lng);

private:
    void initializeWidget();

    QHBoxLayout *_layout;
    DialogNewPlace *_dialog;
};

#endif  // __INSERT_PLACE_WIDGET_HPP__