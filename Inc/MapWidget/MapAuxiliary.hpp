#ifndef __MAP_AUXILIARY_HPP__
#define __MAP_AUXILIARY_HPP__

class MapAuxiliary
{
public:
    static double getDistanceBetweenPointsInMeters(double, double, double, double);
    static double getDistanceBetweenPointsInKm(double, double, double, double);
    static double degreesToRadians(double);
};

#endif // __MAP_AUXILIARY_HPP__
