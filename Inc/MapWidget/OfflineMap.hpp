/*
  Map class based on the example OSMScout2 given by the library OSMScout

  OSMScout - a Qt backend for libosmscout and libosmscout-map
  Copyright (C) 2010  Tim Teulings

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __MAP_WIDGET_HPP__
#define __MAP_WIDGET_HPP__

// Qt includes
#include <QLabel>
#include <QQuickView>
#include <QVBoxLayout>
#include <QWidget>

// STL includes
#include <vector>

// Custom includes
#include <PlaceObj.hpp>
#include <Route.hpp>

/// Proportion of the MainWindow that the map will take
#define MAX_WIDTH_FACTOR  0.5
#define MIN_WIDTH_FACTOR  0.5
#define MAX_HEIGHT_FACTOR 1.0

/// @brief: This is a wrapping class that will interface with the offline
/// map. We need this interface because the map is loaded by the library
/// OSMSCout as a QML object, and with this class, we just load the map.
/// TODO: Future implementations will also include interactions with
/// the user through the mouse clicks and mouse wheel, as well as
/// provide global position and be able to set up a point from interest
/// given a map position only.
class OfflineMap : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(int currentMarkId READ getCurrentMarkId WRITE setMarkId NOTIFY currentMarkIdChanged)
    Q_PROPERTY(int currentRouteId READ getCurrentRouteId  WRITE setRouteId NOTIFY currentRouteIdChanged)
    Q_PROPERTY(int height READ getWindowHeight NOTIFY sizeChanged)
    Q_PROPERTY(int width  READ getWindowWidth  NOTIFY sizeChanged)

public:
    explicit OfflineMap(QWidget *parent = nullptr);
    ~OfflineMap();

    /**
     * @brief Check if the map has been initialized
     *
     * @returns: True if the map has been initialized, false otherwise
    */
    bool is_initialized() const { return _initialized; }

    /**
     * @brief Get the QML window with the loaded map
     *
     * @returns: Pointer to the QML window
    */
    QQuickView *get_window() { return _window; }

    /**
     * @brief Get the widget pointer with the loaded map. This widget
     * is just obtained from the QML window directly using the static
     * convertion QWidget::createWindowContainer.
     *
     * @returns Pointer to the QWidget that includes the map
    */
    QWidget *get_widget() { return _widget; }

    /**
     * @brief Get the current mark ID used to plot a mark in the map
     *
     * @returns Integer with the current mark ID.
    */
    int getCurrentMarkId() const {return _currentMarkId;}

     /**
     * @brief Get the route mark ID used to plot a path in the map
     *
     * @returns Integer with the route mark ID.
    */
    int getCurrentRouteId() const {return _currentRouteId;}

    /**
     * @brief Set the mark ID that we want to modify later on within the map.
     * This funcion should be used whenever we want to mark several places.
     * Each place marked has its own ID so if we don't modify this value, then whenever
     * we want to select a new place, it will erase the previous shown place and show the
     * new one.
     *
     * @param newVal: New ID to be set.
    */
    void setMarkId(int newVal);

    /**
     * @brief Set the route ID that we want to modify later on.
     * This funcion should be used whenever we want to add more than one stop.
     * Each route has its own ID so if we don't modify this value, then whenever
     * we want to add a new route, it will erase the previous shown route and show the
     * new one only. As a result, without modifying the ID, we are erasing old computed
     * routes and plotting new ones
     *
     * @param nVal: New ID to be set.
    */
    void setRouteId(int nVal);

    /**
     * @brief Erase all the computed routes and set the route ID to 0
    */
    void resetRoutes();

    /**
     * @brief Erase all the marks in the map and set the current mark ID to 0
    */
    void resetMarks();

    /**
     * @brief Inform to QML that we want to mark a place on the map at a given
     * position. Before calling this function you need to set the ID we want our mark
     * to have. If not, when you mark a new one, it will erase the previous one that has
     * the same ID
     *
     * @param lat: Latitude of the point we want to mark
     * @param lon: Longitude of the point we want to mark.
    */
    void markAPlace(const PlaceObj& pl);

    /// @brief Remove the mark of the place with the ID provided
    void removeMarkedPlace(int id);

    /**
     * @brief Get the actual height of the map widget
    */
    int getWindowHeight() const {return _height;}

    /**
     * @brief Get the actual width of the map widget
    */
    int getWindowWidth() const {return _width;}

signals:
    /**
     * @brief Signal emitted whenever the user touched the QML map.
     * It will provide the Latitude and Longitude of the point touched
    */
    void mapTapped(double, double);

    /**
     * @brief Signal emitted whenever the variable current marks Id has changed.
     * This is useful to inform to the QML engine the the value of this
     * variable has changed, so then it can update it too.
    */
    void currentMarkIdChanged();

    /**
     * @brief Signal used to pass information to QML regarding a place we want to mark
     *
     * @param lat: Latitude of the point we want to mark
     * @param lon: Longitude of the point we want to mark.
     * @param showAnimation: If true, the map will animate and show the place in the center
     *   of the map. If not, the map won't move
    */
    void newPlace(double lat, double lon, bool showAnimation);

    /**
     * @brief Signal to inform QML to re-center the map into the provided latitude
     *   and longitude
    */
    void mapRecenter(double lat, double lon);

    /**
     * @brief Signal emitted to QML to remove the given mark ID from the map
    */
    void removeId(int id);

    /**
     * @brief Signal emitted to QML to remove all the positional marks ID from the map
    */
    void removeAllMarkIds();

    /**
     * @brief Signal emitted whenever the variable current Route Id has changed.
     * This is useful to inform to the QML engine the the value of this
     * variable has changed, so then it can update it too.
    */
    void currentRouteIdChanged();

    /**
     * @brief Signal used to pass information to QML for computing the path between two points.
    */
    void computePath(PointInMap start, PointInMap end, QString mean);

    /**
     * @brief Signal emitted to QML to remove all the computed routes shown in the map
    */
    void removeAllRouteIds();

    /**
     * @brief Signal to inform QML that the window size has changed
    */
    void sizeChanged(void);

public slots:
    /// @brief Remove all the additional items from the map (routes and marks)
    void clearMapInformation();

    /**
     * @brief Slot to be called whenever we want to compute the path between points.
     *
     * @param list: List of places, in order we want to go to. The list should have at least 2
     *   elements
     *
     * @param meanTransport: Mean of transport chosen by the user.
    */
    void onComputeRoute(std::vector<PlaceObj> &list, QString meanTransport);

protected:
    /**
     * @brief Slot from QWidget. It is called once the widget has been resized
    */
    void resizeEvent(QResizeEvent *event) override;

protected slots:
    /**
     * @brief Function executed whenever the user clicks on the map. Then we
     * can know the latitude and the longitude of the desired point, and then
     * maybe use it as a starting point to plan a path.
     *
     * This function will also emit the mapTapped signal.
     *
     * @params lat: Latitude of the point touched
     * @params lon: Longitude of the point touched
    */
    void onMapClicked(double lat, double lon);

    /**
     * @brief Function called by QML whenever a requested path computation is finished
     *
     * @param time_min: Amount of seconds required to complete the path by the choosen mean
     * @param length_km: Length of the computed path.
    */
    void onRouteComputationFinished(double time_sec, double length_km);

private:
    /// @brief Inner class that will store configuration information.
    /// Using this class will allow to easily add settings to the system
    /// without modifying the function declarations (we always transmit
    /// an Arguments object)
    class Arguments
    {
    public:
        bool help;
        QString databaseDirectory;
        QString style;
        QString iconDirectory;

        Arguments() : help(false),
                      databaseDirectory(""),
                      style(""),
                      iconDirectory("") {}
    };

    /**
     * @brief Initialize the map. This function will configure the QML environment,
     * variables, and start the OSMScout engine to render the map. We also create the
     * Map window, the widget and we set up some size policies to have a nice map shown
     * to the user
    */
    void initializeMap();

    /**
     * @brief Convert the logging level string into the equivalent integer logging
     * level. This will define the OSMScout library logging level. By default,
     * the logging level is WARNING
     *
     * @param 1: String that tells the debug level. The options can be:
     *      DEBUG - INFO - WARNING - ERROR
     * If the string provided does not match with any of the defined logging levels
     * it returns the equivalent level to WARNING.
     *
     * @returns Integer that corresponds to the logging levels specified
    */
    int LogEnv(const QString &);

    /**
     * @brief Provide the theme to show the map.
     *
     * @param 1: Unused. We need to include them because the QML engine has this prototype
     * @param 2: Unused. We need to include them because the QML engine has this prototype
     *
     * @returns: Pointer to QObject with the theme that is going to be use to render the map
    */
    static QObject *ThemeProvider(QQmlEngine *, QJSEngine *);

    /**
     * @brief Load the class Arguments variable that will be use to load the map.
     * Between the arguments we have the Map database dir, the location of the icons to use
     * within the library, and the map style (railway, standard, etc).
    */
    void loadMapArguments();

    /**
     * @brief Function that includes all the possible calls to QObject::connect from Qt
     * framework.
    */
    void makeSignalsConnections();

    /**
     * @brief It will contain only the instructions to configure the format
     * of the Map Widget (maximum and minimum sizes, layouts, etc).
    */
    void setUpWidgetFormat();

    /**
     * @brief Initialize the OSMScout library in order to be able to load the
     * map database and render it.
    */
    bool initOSMScoutEnginer();

    /**
     * @brief Determine if a given ID is within the IDs we are showing to the user
     * in the map.
     *
     * @param id: ID to contrast with the IDs already shown to the user
     * @param seenIds: List of all the IDs that we have seen up to now,
     *  since the last reset call
     *
     * @returns: True is the given ID is already in the map, and false otherwise
    */
    bool isAnOldId(int id, std::vector<int> seenIds);

    Arguments _args;
    bool _initialized;
    QQuickView* _window;
    QWidget* _widget;
    QVBoxLayout* _layout;

    int _currentMarkId;
    std::vector<int> _marksIds;

    int _currentRouteId;
    std::vector<int> _routeIds;
    std::vector<Route> _vectorOfRoutes;

    int _height;
    int _width;
    QString _choosenMean;

    double _total_distance_km;
    double _total_time_sec;
    QLabel *_timeLengthLabel;
};
#endif // __MAP_WIDGET_HPP__
