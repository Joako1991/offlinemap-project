import QtQuick 2.2

import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Window 2.0

import QtPositioning 5.2

import net.sf.libosmscout.map 1.0

Item {
    id: mainWindow
    objectName: "main"
    visible: true
    width: offlineMap.width
    height: offlineMap.height

    Settings {
        id: settings
        //mapDPI: 50
    }

    AppSettings{
        id: appSettings
    }

    /// Map widget
    GridLayout {
        id: content
        anchors.fill: parent

        Map {
            id: map
            objectName: "MapWindow"
            Layout.fillWidth: true
            Layout.fillHeight: true
            focus: true
            // renderyingType can be "plane" or "tiled", but tiled
            // needs online sources, so we don' use it
            renderingType: "plane"

            // Custom definitions
            signal mapClicked(double lat, double lon)
            signal routeComputationFinished(double time_min, double length_km);

            function getFreeRect() {
                return Qt.rect(Theme.horizSpace,
                               Theme.vertSpace,
                               map.width-2*Theme.horizSpace,
                               map.height-2*Theme.vertSpace)
            }

            function setupInitialPosition(){
                if (map.databaseLoaded){
                  if (map.isInDatabaseBoundingBox(appSettings.mapView.lat, appSettings.mapView.lon)){
                    map.view = appSettings.mapView;
                    console.log("restore last position: " + appSettings.mapView.lat + " " + appSettings.mapView.lon);
                  }else{
                    console.log("position " + appSettings.mapView.lat + " " + appSettings.mapView.lon + " is outside database, recenter");
                    map.recenter();
                  }
                }else{
                  map.view = appSettings.mapView;
                }
            }

            PositionSource {
                id: positionSource
                active: true

                onValidChanged: {
                    console.log("Positioning is " + valid)
                    console.log("Last error " + sourceError)

                    for (var m in supportedPositioningMethods) {
                        console.log("Method " + m)
                    }
                }

                onPositionChanged: {
                    map.locationChanged(
                        position.latitudeValid && position.longitudeValid,
                        position.coordinate.latitude, position.coordinate.longitude,
                        position.horizontalAccuracyValid, position.horizontalAccuracy);
                }
            }

            onTap: {
                map.focus=true;
                map.mapClicked(lat, lon)
                map.removePositionMark(offlineMap.currentMarkId);
                map.addPositionMark(offlineMap.currentMarkId, lat, lon);
                console.log("Latitude: " + lat + " Longitude: " + lon);
            }

            onLongTap: {
                console.log("long tap: " + screenX + "x" + screenY + " @ " + lat + " " + lon);
                map.focus=true;
            }

            onViewChanged: {
                appSettings.mapView = map.view;
            }

            Component.onCompleted: {
                setupInitialPosition();
            }

            RoutingListModel {
                id: route

                onComputingChanged: {
                    if (route.count>0){
                        map.addOverlayObject(offlineMap.currentRouteId, route.routeWay);
                        map.routeComputationFinished(route.duration, route.length/1000.0);
                    }
                }
            }

            Connections {
                target: offlineMap

                onNewPlace: {
                    /// Put a mark at certain latitude and longitude point.
                    /// We have a parameter called showAnimation that will move
                    /// the map and centered in the given point if it is true
                    map.removePositionMark(offlineMap.currentMarkId);
                    map.addPositionMark(offlineMap.currentMarkId, lat, lon);
                    if(showAnimation)
                    {
                        map.showCoordinates(lat, lon);
                    }
                }

                onMapRecenter: {
                    map.showCoordinates(lat, lon);
                }

                onRemoveId: {
                    /// Remove an specific mark in the map. The mark is removed based on the provided ID
                    map.removePositionMark(id);
                }

                onRemoveAllMarkIds: {
                    /// Remove all the marks in the map. The mark is removed based on the provided ID
                    map.removeAllPositionMarks();
                }

                onComputePath: {
                    /// This function is called whenever the computePath signal is emitted.
                    /// This slot will request a path computation
                    var from=route.locationEntryFromPosition(start.lat, start.lng);
                    var to=route.locationEntryFromPosition(end.lat, end.lng);
                    route.setStartAndTarget(to, from, mean);
                }

                onRemoveAllRouteIds: {
                    /// In our case, the overlay objects are the routes, so this function
                    /// will basically remove all the routes marked in the map
                    map.removeAllOverlayObjects();
                }
            }

            onDatabaseLoaded: {
                setupInitialPosition();
            }

            /// Zoom in and zoom out buttons
            property var b_width: 30;
            property var b_height: 30;
            property var b_x_offset: 5;
            property var b_y_offset: 5;
            property var b_spacing: 5;

            Button {
                id: zoomIn;
                visible: true;
                x: map.b_x_offset;
                y: map.b_y_offset;
                parent: map;
                anchors.top: map.top;
                width: map.b_width;
                height: map.b_height;
                text: qsTr("+");
                onClicked: {
                    map.zoomIn(2.0);
                }
            }

            Button {
                /// This button is alligned with the zoomIn button
                id: zoomOut;
                visible: true;
                parent: map;
                anchors.topMargin: map.b_spacing;
                anchors.top: zoomIn.bottom;
                anchors.left: zoomIn.left;
                width: map.b_width;
                height: map.b_height;
                text: qsTr("-");
                onClicked: {
                    map.zoomOut(2.0);
                }
            }

            Keys.onPressed: {
                if (event.key === Qt.Key_Plus) {
                    map.zoomIn(2.0)
                    event.accepted = true
                }
                else if (event.key === Qt.Key_Minus) {
                    map.zoomOut(2.0)
                    event.accepted = true
                }
                else if (event.key === Qt.Key_Up) {
                    map.up()
                    event.accepted = true
                }
                else if (event.key === Qt.Key_Down) {
                    map.down()
                    event.accepted = true
                }
                else if (event.key === Qt.Key_Left) {
                    if (event.modifiers & Qt.ShiftModifier) {
                        map.rotateLeft();
                    }
                    else {
                        map.left();
                    }
                    event.accepted = true
                }
                else if (event.key === Qt.Key_Right) {
                    if (event.modifiers & Qt.ShiftModifier) {
                        map.rotateRight();
                    }
                    else {
                        map.right();
                    }
                    event.accepted = true
                }
                else if (event.modifiers===Qt.ControlModifier &&
                         event.key === Qt.Key_D) {
                    map.toggleDaylight();
                }
                else if (event.modifiers===Qt.ControlModifier &&
                         event.key === Qt.Key_R) {
                    map.reloadStyle();
                }
            }
        }
    }
}
