#ifndef __DIALOG_RESET_PASS_HPP__
#define __DIALOG_RESET_PASS_HPP__

/// Qt includes
#include <QDialog>
#include <QDialogButtonBox>
#include <QEventLoop>
#include <QLineEdit>

/// Custom includes
#include <CommonTypes.hpp>
#include <User.hpp>

/// Class implementation inspired in the example given at https://wiki.qt.io/User_Login_Dialog
/// @brief This class is a window that is shown to the user whenver it wants to
/// reset its password, because the user forgot it. This window has 2 fields,
/// which are new password and confirm password. Both of them should match, and
/// they should not be empty in order to submit and write the new password in the
/// database.
class DialogResetPass : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param back_state: This is the state is going to be returned by
     *   the showWindow function whenever the user presses the back button.
     * @param submit_state: This is the state is going to be returned by
     *   the showWindow function whenever the user presses the Submit button.
    */
    explicit DialogResetPass(LoginStates back_state, LoginStates submit_state, QWidget *parent = nullptr);

    ~DialogResetPass();

    /**
     * @brief Show this dialog window and return the state corresponding
     *  to the user selection
     *
     * @returns LoginState of the next thing to do, based on the user selection
    */
    LoginStates showWindow(const User user_to_update);

protected slots:
    /// @brief Slot that is going to be called whenever the user presses the button Submit
    void onClickSubmitButton();

    /// @brief Slot that is going to be called whenever the user presses the button Back
    void onClickBackButton();

private:
    /// @brief Set up all dialog components and initialize them.
    void setUpGUI();

    QLineEdit *_editNewPassword;
    QLineEdit *_editConfNewPassword;
    QDialogButtonBox *_buttons;

    const LoginStates _back_state;
    const LoginStates _submit_state;

    QEventLoop _loop;

    LoginStates _ret_state;
    User _user_data;
};

#endif // __DIALOG_RESET_PASS_HPP__
