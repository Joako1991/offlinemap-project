#ifndef __DIALOG_INFO_VALIDATION_HPP__
#define __DIALOG_INFO_VALIDATION_HPP__

/// Qt includes
#include <QDialog>
#include <QDialogButtonBox>
#include <QEventLoop>
#include <QLineEdit>
#include <QDateEdit>

/// Custom includes
#include <CommonTypes.hpp>
#include <User.hpp>

/// Class implementation inspired in the example given at https://wiki.qt.io/User_Login_Dialog
/// @brief Class that is going to show a window with several fields that
/// has to be filled by the user before resetting the password.
/// These fields are used to contrast with the data retrieved from the database
/// for the given username. If the username does not match, or the totality of
/// the data does not match with the retrieved user data, then a message is pop-up
/// and the system will not continue.
/// All the fields should be provided. Any empty field is not accepted as an answer
/// From this window, the user can return to the previous window, or move forward
/// to the next one if the data provided is correct
class DialogInfoValidation : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param back_state: Value that is going to be returned if the user
     *   clicks the button Back
     * @param ok_state: Value that is going to be returned if the user
     *   clicks the button Submit, in order to check if the entered data is valid or not
    */
    explicit DialogInfoValidation(LoginStates back_state, LoginStates ok_state, QWidget *parent = nullptr);

    ~DialogInfoValidation();

    /**
     * @brief Show this dialog window and return the state corresponding
     *  to the user selection
     *
     * @param output: (out) User found in the database after the user entered the right data
     *
     * @returns LoginState of the next thing to do, based on the user selection
    */
    LoginStates showWindow(User &output);

protected slots:
    /// @brief Slot that is going to be called whenever the user presses the button Submit
    void onClickSubmitButton();

    /// @brief Slot that is going to be called whenever the user presses the button Back
    void onClickBackButton();

    /**
     * @brief Reject method overrided from the class QDialog. This method
     *  is called whenever the user clicks the X button in the window
    */
    void reject() override;

private:
    /// @brief Set up all dialog components and initialize them.
    void setUpGUI();

    QLineEdit *_editUsername;
    QLineEdit *_editEmail;
    QDateEdit *_editbirthday;

    QDialogButtonBox *_buttons;
    QEventLoop _loop;

    const LoginStates _back_state;
    const LoginStates _ok_state;

    LoginStates _ret_state;
    User _entered_user;
};
#endif // __DIALOG_INFO_VALIDATION_HPP__
