#ifndef __LOGIN_MANAGER_HPP__
#define __LOGIN_MANAGER_HPP__

/// Custom includes
#include <CommonTypes.hpp>

class User;

/// @brief Logging manager class. It orchestrates the windows related to the
/// logging system. This means, this is a super class that has all the possible
/// login system windows on it, and it is going to move between them based on what
/// the previous window said it is going to be the next one. This is the state of
/// the loging manager
class LoginManager
{
public:
    /// Default constructor
    LoginManager() : _state(MAIN_WINDOW) {}

    /**
     * @brief Start the logging system. This will block until the user system tells
     *  EXIT or a valid user has been entered.
     *
     * @param user: (output) If the logging succeeded, the this variable will
     *  contain a valid user in order to use in the main window.
     *
     * @returns: Boolean. True if the user information is valid, false if the user
     *  wants to exit from the program.
     */
    bool startLoginManager(User &user);

private:
    LoginStates _state;
};

#endif // __LOGIN_MANAGER_HPP__