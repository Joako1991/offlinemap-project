#ifndef __DIALOG_SIGN_UP_HPP__
#define __DIALOG_SIGN_UP_HPP__
/// Qt includes
#include <QDateEdit>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QEventLoop>

/// Custom includes
#include <CommonTypes.hpp>

/// Class implementation inspired in the example given at https://wiki.qt.io/User_Login_Dialog
/// @brief Window to sign up a new user and write it in the database.
/// This window will ask for certain data from the user, like an username, a password,
/// an email and the birthday in order to create the account. It will check the
/// consistency of the data too (do not repeat username or email and that the password
/// and its confirmation  matches). If any of the checks fails, then a pop-up
/// window will appear saying what is wrong. If everything is ok, a pop-up
/// will appear saying that everything is ok, and it will write the new user
/// into the database
class DialogSignUp : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param back_state: Value that is going to be returned if the user
     *   clicks the button Back
     * @param submit_state: Value that is going to be returned if the user
     *   clicks the button Submit and the entered data has been correctly
     *   written in the database
    */
    explicit DialogSignUp(LoginStates back_state, LoginStates submit_state, QWidget *parent = nullptr);

    ~DialogSignUp();

    /**
     * @brief Show this dialog window and return the state corresponding
     *  to the user selection
    */
    LoginStates showWindow();

protected slots:
    /// @brief Slot that is going to be called after the user clicked the Back button
    void onClickBackButton();

    /// @brief Slot that is going to be called after the user clicked the Submit button
    void onClickSignUpButton();

    /**
     * @brief Reject method overrided from the class QDialog. This method
     *  is called whenever the user clicks the X button in the window
    */
    void reject() override;

private:
    /// @brief Set up all dialog components and initialize them.
    void setUpGUI();

    QLineEdit *_editUsername;
    QLineEdit *_editEmail;
    QDateEdit *_editBirthday;
    QLineEdit *_editPassword;
    QLineEdit *_editConfPassword;
    QDialogButtonBox *_buttons;

    QEventLoop _loop;

    const LoginStates _back_state;
    const LoginStates _submit_state;

    LoginStates _ret_state;
};

#endif // __DIALOG_SIGN_UP_HPP__
