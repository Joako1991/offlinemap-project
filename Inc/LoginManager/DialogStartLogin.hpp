#ifndef __DIALOG_START_LOGIN_HPP__
#define __DIALOG_START_LOGIN_HPP__

// Qt includes
#include <QDialog>
#include <QDialogButtonBox>
#include <QEventLoop>

/// Custom includes
#include <CommonTypes.hpp>

/// Class implementation inspired in the example given at https://wiki.qt.io/User_Login_Dialog
/// @brief QDialog that shows the first window related to the logging system
class DialogStartLogin : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param exit_state: Value that is going to be returned if the user
     *   clicks the button Exit
     * @param login_state: Value that is going to be returned if the user
     *   clicks the button Login
     * @param signup_state: Value that is going to be returned if the user
     *   clicks the button Sign Up
    */
    explicit DialogStartLogin(LoginStates exit_state, LoginStates login_state, LoginStates signup_state, QWidget *parent = nullptr);

    ~DialogStartLogin();

    /**
     * @brief Show this dialog window and return the state corresponding
     *  to the user selection
     *
     * @returns LoginState of the next thing to do, based on the user selection
    */
    LoginStates showWindow();

protected slots:
    /// @brief Slot to be called when the user clicks the Exit button
    void onExitClicked();

    /// @brief Slot to be called when the user clicks the Login button
    void onLoginClicked();

    /// @brief Slot to be called when the user clicks the Sign up button
    void onSignUpClicked();

    /**
     * @brief Reject method overrided from the class QDialog. This method
     *  is called whenever the user clicks the X button in the window
    */
    void reject() override;

private:
    /// @brief Set up all dialog components and initialize them.
    void setUpGUI();

    QDialogButtonBox *_buttons;
    QEventLoop _loop;

    const LoginStates _exit_state;
    const LoginStates _login_state;
    const LoginStates _signup_state;

    LoginStates _ret_state;
};

#endif // __DIALOG_START_LOGIN_HPP__
