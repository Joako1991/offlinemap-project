#ifndef __DIALOG_LOGIN_HPP__
#define __DIALOG_LOGIN_HPP__

/// Qt includes
#include <QDialog>
#include <QDialogButtonBox>
#include <QEventLoop>
#include <QLineEdit>

/// Custom includes
#include <CommonTypes.hpp>
#include <User.hpp>

/// Class implementation inspired in the example given at https://wiki.qt.io/User_Login_Dialog
/// @brief Window to log into the system. This window
/// includes two edit boxes where the user can put its
/// information (username and password) and then click
/// on the different buttons to either, retrieve forgotten
/// password, cancel the login procedure, or try to
/// validate the information to get into the map program
class DialogLogin : public QDialog
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     *
     * @param back_state: Value that is going to be returned if the user
     *   clicks the button Back
     * @param login_success: Value that is going to be returned if the user
     *   clicks the button Login and the login succeeded.
     * @param forget_pass: Value that is going to be returned if the user
     *   clicks the button Forget Password
    */
    explicit DialogLogin(LoginStates back_state, LoginStates login_success, LoginStates forget_pass, QWidget *parent = nullptr);

    ~DialogLogin();

    /**
     * @brief Show this dialog window and return the state corresponding
     *  to the user selection
     *
     * @param out(output): User that is going to be returned. It has valid data
     *   ONLY if the logging result is successful.
     * @returns LoginState of the next thing to do, based on the user selection
    */
    LoginStates showWindow(User &out);

protected slots:
    void onClickedLogin();
    void onClickedBack();
    void onClickedForgetPassword();

    /**
     * @brief Reject method overrided from the class QDialog. This method
     *  is called whenever the user clicks the X button in the window
    */
    void reject() override;

private:
    /// @brief Set up all dialog components and initialize them.
    void setUpGUI();

    QLineEdit *_editUsername;
    QLineEdit *_editPassword;

    QDialogButtonBox *_buttons;
    QEventLoop _loop;

    const LoginStates _back_state;
    const LoginStates _login_success;
    const LoginStates _forget_pass;

    LoginStates _ret_state;
    User _retrievedUser;
};

#endif // __DIALOG_LOGIN_HPP__
