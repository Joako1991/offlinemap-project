#ifndef SEARCHMANAGER_HPP
#define SEARCHMANAGER_HPP

/// Qt includes
#include <QVBoxLayout>
#include <QWidget>

/// Custom includes
#include <SearchEngine/HowToGoWidget.hpp>
#include <SearchEngine/SearchWidget.hpp>
#include <SearchEngine/SearchEngine.hpp>
#include <SearchEngine/SearchResult.hpp>

/// @brief This class aim is to group all the search related widgets.
/// The SearchWidget is in charge of allowing the user to input
/// some data to filter the places we are interested about. This information
/// is going to be passed to the other widgets that are going to be in charge of
/// looking the right data and showing it to the user.
/// TODO: Add more documentation when other widgets are added
class SearchManager : public QWidget
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
    */
    SearchManager(QWidget *parent = nullptr);
    ~SearchManager();

signals:
    /**
     * @brief Signal emitted with the latitude and longitude when the user clicks
     *   on a place listed in the result list
     *
     * @param Lat: latitude of place.
     * @param Lon: longitude of place.
    */
    void markPlaceRequest(const PlaceObj& pl);

    /**
     * @brief Signal emitted whenever a stop modify button has been pressed.
     *   It will transmit the actual ID of the stop that identifies it uniquely
    */
    void stopIdChanged(int id);

    /**
     * @brief Signal emitted when any reset or clear button in the search widgets
     *   is pressed
    */
    void clearRequested();

    /**
     * @brief Signal emitted when either the how to go or go by steps button is pressed
    */
    void computePathRequested(std::vector<PlaceObj> &list, QString meanTransport);

    /**
     * @brief Signal emitted when a mark want to be erased
    */
    void removeMarkRequested(int id);

public slots:
    /**
     * @brief Slot that is called when the user finished the query.
     *   This will send the information to a StopWidget, if it is waiting
     *   for such information
     *
     * @param pl: Place information retrieved from user's input
    */
    void onSearchFinished(const PlaceObj &pl);

    /**
     * @brief Slot called when the ListOfWidgets wants to cancel the
     *   modification request. This will clear the flag of modification request
    */
    void onCancelRequest();

    /**
     * @brief Slot that receives the clicked placed by the user on the placeResult list.
     * @param placeToErase: Place class instance containing information of the selected place by
     *   the user from the result list
    */
    void onPlaceClicked(const PlaceObj& pl);

    /**
     * @brief Slot that should be called whenever a new mark has been added in the map
     *
     * @param lat: Latitude of the mark added in the map
     * @param lng: Longitude of the mark added in the map
    */
    void onMarkAdded(double lat, double lng);

protected slots:
    /**
     * @brief Slot called whenever an user presses a Modify button from the list of stops
     *
     * @param id: Stop widget id that requested the modification
    */
    void onPlaceModificationRequested(int id);

    /**
     * @brief Slot called by the internal widgets whenever a reset or clear is requested
    */
    void onClear();

    /**
     * @brief Slot called by the internal widgets whenever an already marked
     *   place wants to be removed.
     *
     * @param id: ID of the mark to be removed
    */
    void onRemoveMark(int id);

    /**
     * @brief Slot called by the internal widgets whenever a path want to be computed
     *
     * @param list: List of places we want to stop on each corner of the path.
     *   The order of the vector is the order in which we want to stop on each of them
     *
     * @param meanTransport: String with a value from the MeansOfTransportMap struct,
     *   that tells how we want to move between stops. It is supposed that we move by the
     *   same vehicle through all the places
    */
    void onComputePath(std::vector<PlaceObj> &list, QString meanTransport);

    /**
     * @brief Slot to be executed whenever the user clicks the button How to go, placed
     *   in the search widget
    */
    void onClickedHowToGo();

private:
    /**
     * @brief Initialize the SearchManager GUI widgets as well
     *  as signals connections
    */
    void initSearchManagerWidget();

    QVBoxLayout *_layout;
    SearchEngine *_searchEng;
    SearchResult *_searchres;
    SearchWidget *_searchWidget;
    HowToGoWidget *_howToGo;
    QTabWidget *_tabWidget;
    int _searchTabIndex;
    int _howToGoTabIndex;
    bool _modificationPending;
    PlaceObj _lastPlaceClicked;
};

#endif // SEARCHMANAGER_HPP
