#ifndef SEARCHENGINE_HPP
#define SEARCHENGINE_HPP

/// Qt includes
#include <QObject>
#include <QVector>

/// Custom includes
#include <PlaceObj.hpp>
#include <CommonTypes.hpp>

class SearchEngine : public QObject
{
    Q_OBJECT

public:
    SearchEngine() {}
    ~SearchEngine() = default;
    /**
     * @brief filter places based on the distance .
     * @param double: It corresponds to the latitude (if nearby search). If not is set to -1.
     * @param double: It corresponds to the longitude (if nearby search). If not is set to -1.
     * @param int: Nearby radius (0 if exact place is desired).
     * @param QVector<Place>: Input vector of place to be filtered based on distance.
     * @return QVector<Place>: Vector of places already filtered based on distance criterion
    */
    QVector<PlaceObj> getPlacesBasedOnDistance(double, double, int, QVector<PlaceObj>);

    /**
     * @brief Filter places based on the search params provided and the type of search.
     *  In all the cases, it will first filter based on the type of place the user entered,
     *  and the entered text. After that, if the user enters a latitude and longitude
     *  (i.e. clicked the map) and the type of search is for an exact place, then it filters
     *   using a fixed radious, to look around of the selected point if there is any place already
     *   entered, that matches with the entered data. If not, only a place intialized with the latitude
     *   and longitude values is provided.
     *
     * @param PossibleSearchCase: Type of search to be performed
     * @param SearchParams: Struct with the information taken from the user in order to produce
     *   a search result.
     *
     * @return QVector<PlaceObj>: Vector of places already filtered based on user entered data
    */
    QVector<PlaceObj> getPlacesBasedFiltering(PossibleSearchCase, SearchParams);

signals:
    /**
     * @brief Signal emitted when the search process has been finished.
     * @param QVector<PlaceObj>: final vector of places .
     * @param bool: Boolean. If true, that means the results cames
     *   due to a clear signal
    */
    void newSearchResult(std::vector<PlaceObj>, bool);

public slots:
    /**
     * @brief Slot that receives the results of the search widget.
     *
     * @param PossibleSearchCase: PossibleSearchCase enum that tells the type of search the user
     *  wants to perform.
     *
     * @param SearchParams: SearchParams struct that includes all the info needed to perform the
     *  search.
     *   -) radiousDistance: only valid when PossibleSearchCase = SEARCH_NEARBY. It indicates the
     *          nearby radius.
     *   -) latitude: valid when PossibleSearchCase is not RESET. When SEARCH_NEARBY, it is the
     *          latitude of the center of the nearby radius. When SEARCH_EXACT_PLACE, it is the
     *          latitude of the position of the exact place.
     *   -) longitude: valid when PossibleSearchCase is not RESET. When SEARCH_NEARBY, it is the
     *          longitude of the center of the nearby radius. When SEARCH_EXACT_PLACE, it is the
     *          longitude of the position of the exact place.
     *   -) placeType: valid when PossibleSearchCase is not RESET. It corresponds to the TypeOfPlace
     *          enum that tells the type of place the user wants to search.
     *   -) enteredText: valid when PossibleSearchCase is not RESET. User entered text corresponding
     *          to place to search.
    */
    void onSearchRequest(PossibleSearchCase, SearchParams);
};
#endif // SEARCHENGINE_HPP
