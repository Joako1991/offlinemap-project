#ifndef __STOPWIDGET_HPP__
#define __STOPWIDGET_HPP__

// QT includes
#include <QLabel>
#include <QPushButton>

// Custom includes
#include <PlaceObj.hpp>

/// @brief Wrapper class for an Stop widget. This class is the one
/// that stores the information about one target point we want
/// to be considered when computing a route. It has no editable field,
/// just labels that are going to be modified based on the information
/// provided through the slots
class StopWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor
     *
     * @param title: String that is going to be placed as the title of the stop.
     *   For instance, the title can be TO, FROM or STOP 1.
     * @param includeRemove: Boolean flag. If set to true, a remove button
     *   will be added to the constructed widget.
    */
    StopWidget(QString title, bool includeRemove, QWidget *parent = nullptr);
    ~StopWidget();

    /**
     * @brief Reset the widget information. It will clear the stored information about
     *   the place and set default values to the corresponding labels
    */
    void reset();

    /**
     * @brief Change the title
     *
     * @param title: New string that will be used to modify the actual
     *   title of the widget
    */
    void setTitle(QString title);

    /// @brief Get the place information stored in this widget
    PlaceObj getPlaceInfo() const {return _storedPlace;}

signals:
    /**
     * @brief Signal emitted when the modify button is pressed. This means the
     *   user want to change the selected place stored in this widget.
     *
     * @param thisWidget: 'this' pointer value so the connected slot can know which
     *   stop wants to change its value
    */
    void modifyPlace(StopWidget *thisWidget);

    /**
     * @brief Signal emitted when the Remove button is pressed. This means the
     *   user want to remove this stop widget.
     *
     * @param thisWidget: 'this' pointer value so the connected slot can know which
     *   stop wants to be removed
    */
    void removePlace(StopWidget *thisWidget);

public slots:
    /**
     * @brief Slot that should be called after the user has selected the new
     *   information to be set in this widget.
     *
     * @param newPlace: Place object with the newer information about the stop
    */
    void onPlaceInfoReceived(PlaceObj newPlace);

    /**
     * @brief Slot called whenever we click on the button Modify. It will emit
     *   the signal modifyPlace, which includes the widget 'this' pointer
    */
    void onClickModifyButton();

    /**
     * @brief Slot called whenever we click on the button Remove. It will emit
     *   the signal removePlace, which includes the widget 'this' pointer
    */
    void onClickRemoveButton(bool checked);

private:
    /// @brief Function to set up the GUI pointers and initial values
    void initWidget();

    QLabel *_titleLabel;
    QLabel *_placeLabel;
    QLabel *_latlonLabel;
    QLabel *_addressLabel;
    QPushButton *_modifyButton;
    QPushButton *_removeButton;
    PlaceObj _storedPlace;
    QString _title;
    bool _includeRemove;
    int _gridCounter;
};

#endif // __STOPHWIDGET_HPP__