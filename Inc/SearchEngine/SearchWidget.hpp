#ifndef __SEARCHWIDGET_HPP__
#define __SEARCHWIDGET_HPP__

// Custom includes
#include <CommonTypes.hpp>

// QT includes
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QRadioButton>
#include <QSpinBox>
#include <QWidget>

/// @brief This is a class to create the search engine widget.
class SearchWidget : public QWidget
{
    Q_OBJECT

public:
    SearchWidget(QWidget *parent = nullptr);
    ~SearchWidget();

signals:
    /**
     * @brief Signal that emits the results of a search process.
     * @param PossibleSearchCase: PossibleSearchCase enum that tells the type of search the user
     *  wants to perform.
     * @param SearchParams: SearchParams struct that includes all the info needed to perform the
     *  search.
     *   -) radiousDistance: only valid when PossibleSearchCase = SEARCH_NEARBY. It indicates the
     *          nearby radius.
     *   -) latitude: valid when PossibleSearchCase is not RESET. When SEARCH_NEARBY, it is the
     *          latitude of the center of the nearby radius. When SEARCH_EXACT_PLACE, it is the
     *          latitude of the position of the exact place.
     *   -) longitude: valid when PossibleSearchCase is not RESET. When SEARCH_NEARBY, it is the
     *          longitude of the center of the nearby radius. When SEARCH_EXACT_PLACE, it is the
     *          longitude of the position of the exact place.
     *   -) placeType: valid when PossibleSearchCase is not RESET. It corresponds to the TypeOfPlace
     *          enum that tells the type of place the user wants to search.
     *   -) enteredText: valid when PossibleSearchCase is not RESET. User entered text corresponding
     *          to place to search.
    */
    void searchResult(PossibleSearchCase, SearchParams);

    /// @brief Signal emitted when the clear button is pressed
    void clearPressed();

    /// @brief Signal emitted when the user clicks the button How To Go
    void howToGoRequested();

public slots:
    /**
     * @brief This slot is called all the times the map is tapped.
     * It is connected to the signal mapTapped of OfflineMap.
     * It changes the label _latlonlabel to show the latitude and longitude values.
     * @param double: Latitude.
     * @param double: Longitude.
    */
    void onClickMap(double, double);

    /// @brief Clear all the fields in the widget
    void clear();

protected slots:
    /**
     * @brief This slot is called when the Show all places button is clicked.
     * It emits the searchResult signal and resets the search parameters
     * to default values.
    */
    void onClickShowAllPlaces(bool);

    /**
     * @brief This slot is called when the search button is clicked.
     * It emits the searchResult signal.
    */
    void onClickSearchButton(bool);

    /// @brief Slot called whenever the user presses the clear result button
    void onClickClearButton(bool checked);

    /**
     * @brief This slot is called when the Exact Place button is clicked.
     * It disables the nearby Radius spinbox.
    */
    void onToggledExactButton(bool);

    /**
     * @brief This slot is called when the Nearby button is clicked.
     * It enabes the nearby Radius spinbox (the user is supposed to select
     * on the map the place where the nearby search is going to be performed).
    */
    void onToggledNearbyButton(bool);

    /**
     * @brief Slot to be executed whenever the user clicks the button How to go
    */
    void onClickHowToGo(bool checked);

private:
    /**
     * @brief Initialize the search widget. This function creates and structures
     * all the necessary widgets for the search tab.
    */
    void initWidget();

    /// @brief This function initializes all the GUI objects for the Search group box
    void initSearchGroupBox();

    bool _isMapClicked;
    double _lat;
    double _lon;
    QLabel *_latlonLabel;
    QGroupBox *_searchGroupBox;
    QComboBox *_typeComboBox;
    QLineEdit *_inputText;
    QRadioButton *_exactButton;
    QRadioButton *_nearbyButton;
    QSpinBox *_distanceSpinBox;
    PlaceTypeMap _placesTypes;
};

#endif // __SEARCHWIDGET_HPP__
