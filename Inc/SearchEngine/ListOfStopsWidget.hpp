#ifndef __LIST_OF_STOPS_WIDGET_HPP__
#define __LIST_OF_STOPS_WIDGET_HPP__

// QT includes
#include <QMap>
#include <QPushButton>
#include <QString>
#include <QVBoxLayout>
#include <QVector>
#include <QWidget>

/// STL includes
#include <vector>

// Custom includes
#include <CommonTypes.hpp>
#include <SearchEngine/StopWidget.hpp>

/// @brief This class provide a list of stop that we want to go
/// when planning a path. This List is composed by a TO and FROM main
/// stops (that cannot be erased) and then we can add more stops calling
/// the proper functions. It also includes a reset mode, where we erase all
/// the additional stops, and only the main ones are kept
class ListOfStopsWidget : public QWidget
{
    Q_OBJECT
public:
    /// @brief Constructor
    ListOfStopsWidget(QWidget *parent = nullptr);
    ~ListOfStopsWidget();

    /**
     * @brief Update the place information in the last stop that received the
     *   request of modification from the user.
     *
     * @param pl: Place information to be updated and stored in the corresponding
     *   stop widget
    */
    void updateStopUnderModification(const PlaceObj& pl);

    /*
     * @brief Update the place information in the target point
     *
     * @param pl: Place information to be updated and stored in the stop widget
     *   corresponding to the target point(where TO go)
    */
    void updateTargetPoint(const PlaceObj& pl);

public slots:
    /**
     * @brief Slot that is going to be called when we want to
     *   modify certain stop information
     *
     * @param stopToModify: 'this' pointer of the widget on which we asked
     *   to change
    */
    void onModifyStop(StopWidget *stopToModify);

    /**
     * @brief Slot that is going to be called when we want to
     *   remove certain stop information. This slot can only be called by
     *   the stops that are not the main ones (To and from stops).
     *   It will remove the widget information from the vector of stops, and
     *   then update the QWidget list of stops
     *
     * @param stopToModify: 'this' pointer of the widget on which we asked
     *   to erase
    */
    void onRemoveStop(StopWidget *stopWidget);

    /**
     * @brief Slot that is going to be called everytime we click the button
     *   "Add stop". It will add an StopWidget into the list of stops.
    */
    void onAddNewStop();

    /**
     * @brief Slot that is going to be called everytime the user selects
     * a mean of transport (radio button).
     * @param transport: Mean of transport selected.
    */
    void onUpdateMeanOfTransport(MeansOfTransport transport);

    /**
     * @brief Reset the list of stops. After this function is executed,
     *   only FROM and TO stop are going to be shown
    */
    void onReset();

    /**
     * @brief Prepare a vector of the places stored in the stop widgets.
     *   This function will show a pop up if any place has no latitude and
     *   no longitude set, and then it will return. If all the latitudes
     *   and longitudes are valid, then a computePathRequest signal will be
     *   emitted.
    */
    void generateRouteVector();

    /**
     * @brief Prepare a vector of 2 places only from the ones stored in the stop widgets.
     *   This function will show a pop up if any place has no latitude and no longitude set.
     *   If all the latitudes and longitudes are valid, then a computePathRequest signal
     *   will be emitted only with 2 points, corresponding to the step we want to see.
    */
    void generateNextRouteStepVector();

signals:
    /**
     * @brief signal emitted whenever any stop widget received the signal of
     *   modification from the user
     *
     * @param id: Stop widget id
    */
    void modificationRequested(int id);

    /// @brief Signal emitted when the modification request is cancelled
    void cancelModificationRequest();

    /**
     * @brief Signal that will be called when the user wants to know how
     *   to go to certain place.
     *
     * @param list: List of places stored in the stop widgets by the user.
     * @param meanTransport: Mean of transport chosen by the user.
    */
    void computePathRequest(std::vector<PlaceObj> &list, QString meanTransport);

    /// @brief Signal emitted when we want to remove a place and that place has been marked in the map
    void removeStopMark(int id);

private:
    /// @brief Initialize the Widget GUI
    void initWidget();

    /**
     * @brief Create a proper Stop with a given Title, and with the Remove
     *   button, if it corresponds. It will also make the connections for each
     *   button of the widget.
     *
     * @param title: String with the title we want to set into the stop.
     *   If it is a normal stop (not FROM nor TO), then the title should not
     *   include the stop number. The Stop index is going to be appended to the
     *   title in this function.
     *
     * @param includeRemove: Boolean. If true, then the new stop will include the
     *   Remove button (Normal stop).
    */
    StopWidget* createStop(QString title, bool includeRemove);

    /**
     * @brief Remove all the elements from the layout, but we do not delete
     *   the pointers
     *
     * @param layout: Layout to be cleared
    */
    void clearLayout(QLayout *layout);

    /**
     * @brief Clear and re-fill the stops list layout. This function should be called
     *   after any modification in the stop's list
    */
    void updateGridOfStops();

    /**
     * @brief Check if all the places stored in the StopWidgets are valid.
     *   If there is any information missing, then a pop up window will be shown.
     *
     * @returns Boolean. True if all the stop widgets has valid information
    */
    bool checkUserEntries();

    /**
     * @brief Generate a vector with all the places from the StopWidget list.
     *   This function does not do any check about the information entered.
     *   If you want to ensure that this data is totally valid, then you need
     *   to check with the checkUserEntries function
     *
     * @returns Vector of places retrieved from the StopWidgets
    */
    std::vector<PlaceObj> getListOfPlaces();

    QVBoxLayout *_layout;
    QVector<StopWidget *> _stops;
    StopWidget *_to;
    StopWidget *_from;
    StopWidget *_stopUnderModification;
    QMap<StopWidget*, int> _idsMap;
    int _maximumCurrentId;
    int _currentStep;
    QString _meanOfTransport;
};

#endif // __LIST_OF_STOPS_WIDGET_HPP__