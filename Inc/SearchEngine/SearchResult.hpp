#ifndef SEARCHRESULT_HPP
#define SEARCHRESULT_HPP

/// QT includes
#include <QLabel>
#include <QListWidget>
#include <QVBoxLayout>

/// STL
#include <iostream>

/// Custom includes
#include <PlaceObj.hpp>
#include "PlaceWidget.hpp"

/// @brief Class that shows the results of a search.
/// It uses a QListWidget in order to show the results.
/// Each result is of the type PlaceWidget.
class SearchResult : public QWidget
{
    Q_OBJECT

public:
    /// @brief Constructor
    SearchResult(QWidget *parent = nullptr);
    ~SearchResult();

    /// @brief Remove all the results in the list
    void clearList();

signals:

    /**
     * @brief Signal emitted when user clicked a place in the placeResult list.
     *
     * @param placeToSelect: Place class instance with the information of the
     *   place selected by the user.
    */
    void placeClicked(PlaceObj placeToSelect);

public slots:

    /**
     * @brief Slot that receives a vector of places as the result
     *   from the search process.
     * @param places: List of places based in the search parameters given
     *   by the user.
     * @param areWeClearing: Boolean. If true, that means the results cames
     *   due to a clear signal
    */
    void onSearchFinalResult(std::vector<PlaceObj> places, bool areWeClearing);

    /**
     * @brief Slot that receives the place from the result list when the user
     *   clicks on the placeResult list.
     * @param placeToErase: place based on user clicked in placeResult lsit.
    */
    void onPlaceResultClicked(PlaceObj placeToErase);


private:
    QVector<PlaceWidget*> _searchResult;
    QListWidget *_resultListWidget;
    QVBoxLayout *_layout;
    QLabel *_emptyResultText;
};
#endif // SEARCHRESULT_HPP
