#ifndef __HOW_TO_GO_WIDGET_HPP__
#define __HOW_TO_GO_WIDGET_HPP__

/// Qt includes
#include <QPushButton>
#include <QRadioButton>
#include <QVBoxLayout>
#include <QWidget>

/// Custom includes
#include <SearchEngine/ListOfStopsWidget.hpp>

/// @brief This class includes includes the buttons and the
/// list of stops that are going to be included in the how to search
/// tab. This way, the buttons reset and Add stop stays fixed, and the
/// scrollable list moves only with the Stops
class HowToGoWidget : public QWidget
{
    Q_OBJECT
public:
    /// @brief Constructor
    HowToGoWidget(QWidget *parent = nullptr);
    ~HowToGoWidget();

    ListOfStopsWidget* getListOfStops() {return _listOfStops;}

signals:
    /// @brief Signal emitted whenever the Reset button is pressed
    void reset();

protected slots:
    /**
     * @brief This slot is called when On Foot button is clicked.
     * It calls the function onUpdateMeanOfTransport of ListOfStopsWidget class
     * with the parameter ON_FOOT.
    */
    void onChangeToFoot(bool);

    /**
     * @brief This slot is called when By bike button is clicked.
     * It calls the function onUpdateMeanOfTransport of ListOfStopsWidget class
     * with the parameter BY_BIKE.
    */
    void onChangeToBike(bool);

    /**
     * @brief This slot is called when By car button is clicked.
     * It calls the function onUpdateMeanOfTransport of ListOfStopsWidget class
     * with the parameter BY_CAR.
    */
    void onChangeToCar(bool);

    /**
     * @brief Slot executed whenever the Reset button is pressed
    */
    void onReset();

private:
    /// @brief Init widget GUI
    void initWidget();

    QVBoxLayout *_layout;
    ListOfStopsWidget *_listOfStops;

    QPushButton *_showNextStepButton;
    QRadioButton *_footButton;
};
#endif // __HOW_TO_GO_WIDGET_HPP__
