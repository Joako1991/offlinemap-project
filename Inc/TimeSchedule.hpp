#ifndef __TIME_SCHEDULE_HPP__
#define __TIME_SCHEDULE_HPP__

/// STL includes
#include <string>

/// Custom types
#include <CommonTypes.hpp>

/**
 * @brief Class to easily get hour format given two integers. The hour is expressed in
 * 24 hs format
*/
class Time24Hours
{
public:
    Time24Hours() : _hours(0), _minutes(0) {}
    Time24Hours(int HH, int mm);
    Time24Hours(const Time24Hours& time);
    ~Time24Hours() = default;

    /**
     * @brief Get an string with the time formatted in 2 digits for
     * hour and 2 digits for minutes. The format is always in 24 hours format
    */
    std::string getTime() const {return convertToString(_hours,2) + ":" + convertToString(_minutes, 2);}

    /// Mutators
    void setHours(int newVal);
    void setMinutes(int newVal);
    int getHours() {return _hours;}
    int getMinutes() {return _minutes;}

    /// Operators
    Time24Hours& operator=(const Time24Hours&);

private:
    /**
     * @brief Function to convert an integer to an string, with a fixed amount of digits
     *
     * @param val: Integer to be converted into string
     * @param dig: Amount of digits that the final string will have
    */
    std::string convertToString(int val, int dig) const;
    int _hours;
    int _minutes;
};

/**
 * @brief Class to wrap the schedule generation. In order to have one
 * day schedule, we need to know which day of the week we are talking about,
 * and the hours when this place opens and close, and if they are close all day,
 * or if they close in the middle of the day.
*/
class DaySchedule
{
public:
    DaySchedule(DaysOfTheWeek day) : _day(day), _closeAllDay(true) {}

    /**
     * @brief Constructor that will initialize all the parameters in the
     * class using an string. The format of this string should be:
     *      <DAY_OF_THE_WEEK>: HH:mm - HH:mm
     * if it has only one schedule a day, or
     *      <DAY_OF_THE_WEEK>: HH:mm - HH:mm
     *          HH:mm - HH:mm
     * if the place closes at midday, or
     *      <DAY_OF_THE_WEEK>: Closed
    */
    DaySchedule(std::string sched);
    ~DaySchedule() = default;

    /**
     * @brief Set the hours when a place is open and when is closed. This funcion
     * when called will set up the class as an schedule that does not close at midday,
     * or it has only one range of hours open.
     *
     * @param startTime: Time when the place opens
     * @param endTime: Time when the place closes
    */
    void setScheduleWithoutClosureTime(Time24Hours startTime, Time24Hours endTime);

    /**
     * @brief Set the hours when a place is open and when is closed during the morning and
     * during afternoon. This funcion when called, will set up the class as an schedule
     * that closes at midday, or it has two ranges of hours open.
     *
     * @param morningStartTime: Time when the place opens in the morning.
     * @param morningEndTime: Time when the place closes in the morning.
     * @param afternoonStartTime: Time when the place opens in the afternoon.
     * @param afternoonEndTime: Time when the place closes in the afternoon.
    */
    void setScheduleWithCloseAtMidday(Time24Hours morningStartTime,
        Time24Hours morningEndTime,
        Time24Hours afternoonStartTime,
        Time24Hours afternoonEndTime);

    /**
     * @brief Set up the schedule to be closed for this day
    */
    void placeClosed() {_closeAllDay = true;};

    /**
     * @brief Get the day of this schedule
     *
     * @returns DaysOfTheWeek enum value corresponding to the day stored in the class
    */
    DaysOfTheWeek getDay() const {return _day;}

    /**
     * @brief Change the day at which this schedule refers to
     *
     * @param newVal: New enum value corresponding to the day we want to set
    */
    void setDay(DaysOfTheWeek newVal) { _day = newVal;}

    /**
     * @brief Get a formatted string with the day and the hours when the
     * place opens and closes.
     *
     * @return String with the schedule of the day. The format given is:
     *      DAY: HH:mm - HH:mm
     *      HH:mm - HH:mm
     *
     * The second line might not appear, if the place has only one range of hours
     * when it is open. If the store is closed that day, the string will be:
     *      DAY: Closed
    */
    std::string getDaySchedule() const;

private:

    /**
     * @brief Extract the day of the week from an schedule string.
     * The format of this string should be:
     *      <DAY_OF_THE_WEEK>: HH:mm - HH:mm
     * if it has only one schedule a day, or
     *      <DAY_OF_THE_WEEK>: HH:mm - HH:mm / HH:mm - HH:mm
     * if the place closes at midday, or
     *      <DAY_OF_THE_WEEK>: Closed
     *
     * @param str: String with the schedule from which extract the day.
     * @returns Enum value DaysOfTheWeek of the corresponding day found the given string
    */
    DaysOfTheWeek extractDay(std::string str);

    /**
     * @brief Extract the schedule of the day from an schedule string.
     * The format of this string should be:
     *          HH:mm - HH:mm
     * if the place opens during the day, or
     *          Closed
     *
     * @param str: String with the schedule.
     * @param: OUTPUT Time when the place opens
     * @param: OUTPUT Time when the place closes
    */
    void extractTimes(std::string str, Time24Hours& openTime, Time24Hours& closeTime);

    Time24Hours _morningStartTime;
    Time24Hours _morningEndTime;
    Time24Hours _afternoonStartTime;
    Time24Hours _afternoonEndTime;
    DaysOfTheWeek _day;
    bool _closesMidDay;
    bool _closeAllDay;
};

#endif // __TIME_SCHEDULE_HPP__
