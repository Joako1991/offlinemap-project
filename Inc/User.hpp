#ifndef USER_HPP
#define USER_HPP

/// Qt includes
#include <QDate>
#include <QString>

class User
{
public:
    /// Default constructor
    User() :
        _userName(""),
        _email(""),
        _birthday(),
        _password("")
    {}

    /**
     * @brief Constructor
     *
     * @param userName: QString with the user name.
     * @param email: QString with the email of the user.
     * @param birthday: QDate with the birthday of the user
     * @param password: QString with the password of the user
    */
    User(QString userName, QString email, QDate birthday, QString password)  :
        _userName(userName),
        _email(email),
        _birthday(birthday),
        _password(password) {}

    /// Mutators
    void setPassword(const QString &newPass) { _password = newPass; }
    QString getUserName() const { return _userName; }
    QString getEmail() const { return _email; }
    QDate getBirthDay() const { return _birthday; }
    QString getPassword() const { return _password; }

    /**
      * @brief Clear all the parameters of the user
    */
    void clear();

    /**
     * @brief Check if the information contained in this class
     *  is a valid user
     *
     * @returns Boolean true if neither if the fields username,
     *  email and password are empty
    */
    bool isValid() const { return !_userName.isEmpty() && !_email.isEmpty() && !_password.isEmpty(); }

    /**
     * @brief Check if a user wants to be dupplicated. We consider
     *  a dupplication when either the username or the email is the same
     *
     * @param user: User to compare with
     *
     * @returns true if the user is a dupplication. False otherwise.
    */
    bool isDupplicated(const User &user) const {
        return (_userName == user.getUserName() ||
            _email == user.getEmail());
    }

    /**
     * @brief Equal operator. Check if two users are exactly the same.
     *  In order to know that the user is exactly the same, we only check if
     *  the usernames, the emails and the birthdays are the same. We don't care
     *  about the password. This is an useful function when we bring information
     *  from the database, and we want to compare the information with what the user
     *  entered.
     *
     * @param user: User to compare with
     *
     *  @returns true if they are equal. False otherwise.
    */
    bool operator==(const User &user) const {
        return (_userName == user.getUserName() &&
            _email == user.getEmail() &&
            _birthday == user.getBirthDay());
    }

private:
    QString _userName;
    QString _email;
    QDate _birthday;
    QString _password;
};

#endif // USER_HPP
