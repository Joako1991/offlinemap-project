TEMPLATE = app

QT_CONFIG -= no-pkg-config

CONFIG += qt warn_on debug link_pkgconfig thread c++11 silent

QT += core gui widgets qml quick

qtHaveModule(svg) {
 QT += svg
}

qtHaveModule(positioning) {
 QT += positioning
}

unix|win32: LIBS += -L$$PWD/../build/libosmscout-client-qt/ -losmscout_client_qt

INCLUDEPATH += $$PWD/../build/libosmscout-client-qt
DEPENDPATH += $$PWD/../build/libosmscout-client-qt

unix|win32: LIBS += -L$$PWD/../build/libosmscout-map-qt/ -losmscout_map_qt

INCLUDEPATH += $$PWD/../build/libosmscout-map-qt
DEPENDPATH += $$PWD/../build/libosmscout-map-qt

INCLUDEPATH += ../libosmscout-gpx/include \
               ../libosmscout/include \
               ../libosmscout-client-qt/include \
               ../libosmscout-map/include \
               ../libosmscout-map-qt/include \
               ../build/libosmscout/include \
               ../build/libosmscout-gpx/include \
               ../build/libosmscout-client-qt/include \
               ../build/libosmscout-map/include \
               ../build/libosmscout-map-qt/include \

!macx {
  gcc:QMAKE_CXXFLAGS += -fopenmp
}

release: DESTDIR = release
debug:   DESTDIR = debug

OBJECTS_DIR = $$DESTDIR/
MOC_DIR = $$DESTDIR/
RCC_DIR = $$DESTDIR/
UI_DIR = $$DESTDIR/

SOURCES = src/OSMScout.cpp \
          src/Theme.cpp \
          src/AppSettings.cpp \
          src/PositionSimulator.cpp

HEADERS = src/Theme.h \
          src/AppSettings.h \
          src/PositionSimulator.h

DISTFILES += \
    qml/custom/MapButton.qml \
    qml/main.qml \
    qml/custom/LineEdit.qml \
    qml/custom/DialogActionButton.qml \
    qml/custom/LocationSearch.qml \
    qml/custom/ScrollIndicator.qml \
    qml/custom/MapDialog.qml \
    qml/AboutDialog.qml \
    qml/SearchDialog.qml \
    qml/MapDownloadDialog.qml \
    pics/DeleteText.svg \
    pics/Minus.svg \
    pics/Plus.svg \
    pics/Search.svg \
    pics/Download.svg

RESOURCES += \
    res.qrc

ANDROID_EXTRA_LIBS = ../libosmscout/src/.libs/libosmscout.so \
                     ../libosmscout-map/src/.libs/libosmscoutmap.so \
                     ../libosmscout-map-qt/src/.libs/libosmscoutmapqt.so

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

unix|win32: LIBS += -L$$PWD/../build/libosmscout/ -losmscout

INCLUDEPATH += $$PWD/../build/libosmscout
DEPENDPATH += $$PWD/../build/libosmscout

unix|win32: LIBS += -L$$PWD/../build/libosmscout-gpx/ -losmscout_gpx

INCLUDEPATH += $$PWD/../build/libosmscout-gpx
DEPENDPATH += $$PWD/../build/libosmscout-gpx
