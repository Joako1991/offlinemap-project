QT += core gui qml quick quickwidgets
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


qtHaveModule(svg) {
 QT += svg
}

qtHaveModule(positioning) {
 QT += positioning
}

CONFIG += qt warn_on debug link_pkgconfig thread c++11 silent

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ./Externals/libosmscout/libosmscout-gpx/include \
               ./Externals/libosmscout/libosmscout/include \
               ./Externals/libosmscout/libosmscout-client-qt/include \
               ./Externals/libosmscout/libosmscout-map/include \
               ./Externals/libosmscout/libosmscout-map-qt/include \
               ./Externals/libosmscout/build/libosmscout/include \
               ./Externals/libosmscout/build/libosmscout-gpx/include \
               ./Externals/libosmscout/build/libosmscout-client-qt/include \
               ./Externals/libosmscout/build/libosmscout-map/include \
               ./Externals/libosmscout/build/libosmscout-map-qt/include \
               ./Inc \
               ./Databases

!macx {
  gcc:QMAKE_CXXFLAGS += -fopenmp
}

release: DESTDIR = release
debug:   DESTDIR = debug

OBJECTS_DIR = $$DESTDIR/
MOC_DIR = $$DESTDIR/
RCC_DIR = $$DESTDIR/
UI_DIR = $$DESTDIR/

SOURCES = Src/LoginManager/LoginManager.cpp \
          Src/LoginManager/DialogStartLogin.cpp \
          Src/LoginManager/DialogLogin.cpp \
          Src/LoginManager/DialogInfoValidation.cpp \
          Src/LoginManager/DialogResetPass.cpp \
          Src/LoginManager/DialogSignUp.cpp \
          Src/InsertPlaceWidget/InsertPlaceWidget.cpp \
          Src/InsertPlaceWidget/DialogNewPlace.cpp \
          Src/InsertPlaceWidget/DayScheduleGroup.cpp \
          Src/ErasePlaceWidget/ErasePlaceWidget.cpp \
          Src/ErasePlaceWidget/DialogEraseAPlace.cpp \
          Src/MapWidget/Theme.cpp \
          Src/MapWidget/AppSettings.cpp \
          Src/MapWidget/OfflineMap.cpp \
          Src/MapWidget/MapAuxiliary.cpp \
          Src/TimeSchedule.cpp \
          Src/PlaceObj.cpp \
          Src/PlaceWidget.cpp \
          Src/User.cpp \
          Src/CommentSystem/CommentWidget.cpp \
          Src/CommentSystem/CommentsManager.cpp \
          Src/CommentSystem/DialogShowComments.cpp \
          Src/CommentSystem/DialogNewComment.cpp \
          Src/DBEngine/DBEngine.cpp \
          Src/DBEngine/DBQueries.cpp \
          Src/DBEngine/DefaultPlaces.cpp \
          Src/DBEngine/DefaultComments.cpp \
          Src/SearchEngine/SearchWidget.cpp \
          Src/SearchEngine/SearchResult.cpp \
          Src/SearchEngine/SearchManager.cpp \
          Src/SearchEngine/SearchEngine.cpp \
          Src/SearchEngine/StopWidget.cpp \
          Src/SearchEngine/ListOfStopsWidget.cpp \
          Src/SearchEngine/HowToGoWidget.cpp \
          Src/ScheduleWidget/DialogShowSchedule.cpp \
          Src/ScheduleWidget/ScheduleWidget.cpp \
          mainwindow.cpp \
          main.cpp

HEADERS = Inc/LoginManager/LoginManager.hpp \
          Inc/LoginManager/DialogStartLogin.hpp \
          Inc/LoginManager/DialogLogin.hpp \
          Inc/LoginManager/DialogInfoValidation.hpp \
          Inc/LoginManager/DialogResetPass.hpp \
          Inc/LoginManager/DialogSignUp.hpp \
          Inc/InsertPlaceWidget/InsertPlaceWidget.hpp \
          Inc/InsertPlaceWidget/DialogNewPlace.hpp \
          Inc/InsertPlaceWidget/DayScheduleGroup.hpp \
          Inc/ErasePlaceWidget/ErasePlaceWidget.hpp \
          Inc/ErasePlaceWidget/DialogEraseAPlace.hpp \
          Inc/MapWidget/Theme.h \
          Inc/MapWidget/AppSettings.h \
          Inc/MapWidget/OfflineMap.hpp \
          Inc/MapWidget/MapAuxiliary.hpp \
          Inc/CommentObj.hpp \
          Inc/TimeSchedule.hpp \
          Inc/PlaceObj.hpp \
          Inc/PlaceWidget.hpp \
          Inc/Route.hpp \
          Inc/User.hpp \
          Inc/CommonTypes.hpp \
          Inc/CommentSystem/CommentWidget.hpp \
          Inc/CommentSystem/CommentsManager.hpp \
          Inc/CommentSystem/DialogShowComments.hpp \
          Inc/CommentSystem/DialogNewComment.hpp \
          Inc/DBEngine/DBEngine.hpp \
          Inc/DBEngine/DBQueries.hpp \
          Inc/DBEngine/DefaultPlaces.hpp \
          Inc/DBEngine/DefaultComments.hpp \
          Inc/SearchEngine/SearchWidget.hpp \
          Inc/SearchEngine/SearchResult.hpp \
          Inc/SearchEngine/SearchManager.hpp \
          Inc/SearchEngine/SearchEngine.hpp \
          Inc/SearchEngine/StopWidget.hpp \
          Inc/SearchEngine/ListOfStopsWidget.hpp \
          Inc/SearchEngine/HowToGoWidget.hpp \
          Inc/ScheduleWidget/DialogShowSchedule.hpp \
          Inc/ScheduleWidget/ScheduleWidget.hpp \
          mainwindow.h

FORMS += \
    mainwindow.ui

DISTFILES += \
    Inc/MapWidget/qml/main.qml

RESOURCES += \
    Inc/MapWidget/res.qrc

## We define a variable that will be available in the CPP program. This way
## we do not depend where the binary is installed.
DEFINES += \
    PROJECT_PATH=\\\"$$PWD/\\\"

unix|win32: LIBS += -L$$PWD/Externals/libosmscout/build/libosmscout/ -losmscout
unix|win32: LIBS += -L$$PWD/Externals/libosmscout/build/libosmscout-gpx/ -losmscout_gpx
unix|win32: LIBS += -L$$PWD/Externals/libosmscout/build/libosmscout-client-qt/ -losmscout_client_qt
unix|win32: LIBS += -L$$PWD/Externals/libosmscout/build/libosmscout-map-qt/ -losmscout_map_qt
unix|win32: LIBS += -lsqlite3


INCLUDEPATH += $$PWD/Externals/libosmscout/build/libosmscout
INCLUDEPATH += $$PWD/Externals/libosmscout/build/libosmscout-gpx
INCLUDEPATH += $$PWD/Externals/libosmscout/build/libosmscout-map-qt
INCLUDEPATH += $$PWD/Externals/libosmscout/build/libosmscout-client-qt

DEPENDPATH += $$PWD/Externals/libosmscout/build/libosmscout
DEPENDPATH += $$PWD/Externals/libosmscout/build/libosmscout-gpx
DEPENDPATH += $$PWD/Externals/libosmscout/build/libosmscout-map-qt
DEPENDPATH += $$PWD/Externals/libosmscout/build/libosmscout-client-qt
